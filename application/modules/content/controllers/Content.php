<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if (!$this->session->userdata('authLogin')) {
            return redirect('/login');
        }
    }

    public function menu($idMenu=''){
        $getAll = explode('.',$idMenu);
        if (isset($getAll[0])) {
            $menu = $getAll[0];
            $subMenu = $getAll[1];

            $getContent = $this->datahandler->selectData('*','sub_menu','id='.$subMenu.' and id_menu='.$menu);
            if (empty($getContent)) {
                return $this->hlmhelper->throwError();
            }
            $funcName = $getContent[0]->route_submenu;
            try {
                return $this->$funcName('get',$menu.'.'.$subMenu);
            } catch (\Exception $exception) {
                return $this->hlmhelper->throwError();
            }
        }else{
           return $this->hlmhelper->throwError();
        }    
    }

    public function news($type='get',$requestMenu='',$data='')
        {
            $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',"sb.content = 'content/news' AND id_user =".$this->session->userdata('id'));
            $priviledge = $getPriviledge[0]->access_grant;
            $priviledge = explode(',',$priviledge);
            if (empty($priviledge[0])) {
                return $this->hlmhelper->throwError();
            }
            $nextProcess = false;
            $data = array('requestMenu'=>$requestMenu);
            $subMeniId = explode('.', $requestMenu);
            $getSubmenuName = $this->datahandler->selectData('submenu_name','sub_menu','id='.$subMeniId[1]);
            $data['title'] = $getSubmenuName[0]->submenu_name;
            if (strripos($type,'.')!=false) {
                $getData = explode('.', $type);
                $mode = $getData[0];
                $idParam = $getData[1];
                $nextProcess = true;
                $data['idParam'] = $idParam;

                if ($mode=='save') {
                    $subMode = $idParam;
                }
            }else{
                $mode = $type;
                $nextProcess = true;
            }
            $data['status'] = array('0'=>'Incative','1'=>'Active');
            if ($nextProcess) {
                switch ($mode) {
                    case 'get':
                        return $this->hlmhelper->genView('content/news',$data);
                        break;
                    case 'save':
                        switch ($subMode) {
                            case 'new':
                                //for new data insert
                                if (in_array(2, $priviledge)) {
                                    //for new data insert
                                    $allData = $this->input->post();
                                    $dataInsert = array();
                                    foreach ($allData as $key => $value) {
                                        if($key!="idParam" && $key!="image"){
                                            $dataInsert[$key] = $value;
                                        }
                                    }

                                    //image process
                                    $config['upload_path']          = './data_uploads/news';
                                    $config['allowed_types']        = 'pdf|doc|docx|jpeg|jpg|';
                                    $config['max_size']             = 1000000;

                                    if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                                        $fileName = $_FILES['image']['name'];
                                        $ext = explode('.', $fileName);
                                        $ext = $ext[1];
                                        $newFileName = $config['file_name'] = date('Ymdhis').".".$ext;
                                        $this->load->library('upload', $config);
                                        if ( ! $this->upload->do_upload('image'))
                                        {
                                            $error = array('error' => $this->upload->display_errors());
                                            $this->session->set_flashdata('error',$error['error']);
                                            
                                            return redirect('/content/menu/'.$requestMenu);
                                        }
                                        else
                                        {   
                                            $dataInsert['image'] = trim($newFileName);
                                        }
                                    }
                                    //end image process

                                    //echo "<pre>";print_r($dataInsert);die();
                                    $this->datahandler->insertData('news',$dataInsert);
                                    
                                    $this->session->set_flashdata('success','Data berhasil ditambah');
                                    return redirect('content/menu/'.$requestMenu);
                                    
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, $priviledge)) {
                                    //for new data insert
                                    $allData = $this->input->post();
                                    $getOldData = $this->datahandler->selectData('*','news','id='.$allData['idParam']);
                                    $dataInsert = array();
                                    foreach ($allData as $key => $value) {
                                        if($key!="idParam" && $key!="image"){
                                            $dataInsert[$key] = $value;
                                        }
                                    }

                                    //image process
                                    $config['upload_path']          = './data_uploads/news';
                                    $config['allowed_types']        = 'pdf|doc|docx|jpeg|jpg|';
                                    $config['max_size']             = 1000000;

                                    if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                                        $fileName = $_FILES['image']['name'];
                                        $ext = explode('.', $fileName);
                                        $ext = $ext[1];
                                        $newFileName = $config['file_name'] = date('Ymdhis').".".$ext;
                                        $this->load->library('upload', $config);
                                        if ( ! $this->upload->do_upload('image'))
                                        {
                                            $error = array('error' => $this->upload->display_errors());
                                            $this->session->set_flashdata('error',$error['error']);
                                            
                                            return redirect('/content/menu/'.$requestMenu);
                                        }
                                        else
                                        {   
                                            if (file_exists('./data_uploads/news/'.$getOldData[0]->image)) {
                                                unlink('./data_uploads/news/'.$getOldData[0]->image);
                                            }
                                            $dataInsert['image'] = trim($newFileName);
                                        }
                                    }
                                    //end image process

                                    $this->datahandler->updateData('news',$dataInsert,'id='.$allData['idParam']);
                                    
                                    $this->session->set_flashdata('success','Data berhasil diedit');
                                    return redirect('content/menu/'.$requestMenu);
                                    
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'ajax':
                        $columns = array( 
                        'id',
                        'title',
                        'description',
                        'concat("<img height=\'100px\' width=\'auto\'  src=\''.base_url().'data_uploads/news/",image,"\'>") as image'
                        );
                        $nama_tbl = "news";
                        $custom_where = "";
                        $hide_column = "";
                        $custom_order = "";
                        $custom_if =array();
                        $custom_group = "";
                        $method_link = array();
                        $icon_img = array();
                        if (in_array(4, $priviledge)) {
                            $method_link['edit'] = "content/news/viewEdit";
                            $icon_img['edit'] = "fa fa-pencil";
                        }
                        if (in_array(4, $priviledge)) {
                            $method_link['delete'] = "content/news/delete";
                            $icon_img['delete'] = "fa fa-trash";
                        }

                        $custom_field = array();
                        $custom_thick = "";

                        $hide_column="id";
                        $tbl_id = "id";

                        $custom_link = array();

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;
                    case 'viewNew':
                        $data['statEdit'] = false;
                        return $this->hlmhelper->genView('content/viewEditNews',$data);
                        break;
                    case 'viewEdit':
                        $data['dataEdit'] = $this->datahandler->selectData('*','news','id = '.$idParam);
                        $data['statEdit'] = true;
                        return $this->hlmhelper->genView('content/viewEditNews',$data);
                        break;
                    case 'delete':
                        if (in_array(4, $priviledge)) {
                            $this->db->where('id',$idParam)->delete('news');
                            $this->session->set_flashdata('success','Data berhasil di hapus');
                            return redirect('content/menu/'.$requestMenu);
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return $this->hlmhelper->throwError();
            }
            
        }

    public function slider($type='get',$requestMenu='',$data='')
        {
            $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',"sb.content = 'content/slider' AND id_user =".$this->session->userdata('id'));
            $priviledge = $getPriviledge[0]->access_grant;
            $priviledge = explode(',',$priviledge);
            if (empty($priviledge[0])) {
                return $this->hlmhelper->throwError();
            }
            $nextProcess = false;
            $data = array('requestMenu'=>$requestMenu);
            $subMeniId = explode('.', $requestMenu);
            $getSubmenuName = $this->datahandler->selectData('submenu_name','sub_menu','id='.$subMeniId[1]);
            $data['title'] = $getSubmenuName[0]->submenu_name;
            if (strripos($type,'.')!=false) {
                $getData = explode('.', $type);
                $mode = $getData[0];
                $idParam = $getData[1];
                $nextProcess = true;
                $data['idParam'] = $idParam;

                if ($mode=='save') {
                    $subMode = $idParam;
                }
            }else{
                $mode = $type;
                $nextProcess = true;
            }
            $data['status'] = array('0'=>'Incative','1'=>'Active');
            if ($nextProcess) {
                switch ($mode) {
                    case 'get':
                        return $this->hlmhelper->genView('content/slider',$data);
                        break;
                    case 'save':
                        switch ($subMode) {
                            case 'new':
                                //for new data insert
                                if (in_array(2, $priviledge)) {
                                    //for new data insert
                                    $this->datahandler->insertData('news',$dataInsert);
                                    
                                    $this->session->set_flashdata('success','Data berhasil ditambah');
                                    return redirect('content/menu/'.$requestMenu);
                                    
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, $priviledge)) {
                                    //for new data insert
                                    //end image process

                                    $this->datahandler->updateData('news',$dataInsert,'id='.$allData['idParam']);
                                    
                                    $this->session->set_flashdata('success','Data berhasil diedit');
                                    return redirect('content/menu/'.$requestMenu);
                                    
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'ajax':
                        $columns = array( 
                        'v.id',
                        'v.voucher_id',
                        'v.name',
                        'v.price',
                        'v.point',
                        'v.expired_date',
                        'v.bonus_point',
                        'vc.name as voucher_category',
                        'v.stock',
                        'v.sold',
                        'concat("<img height=\'auto\' width=\'100px\'  src=\''.base_url().'data_uploads/voucher/",v.merchant_id,"/",v.image,"\'>") as image'
                        );
                        $nama_tbl = " voucher as v join voucher_category as vc on v.voucher_category = vc.id ";
                        $hide_column = "";
                        $custom_order = "";
                        $custom_if =array();
                        $custom_group = "";
                        $method_link = array();
                        $icon_img = array();

                        $custom_where = " stat_slider = 1 ";

                        if (in_array(4, $priviledge)) {
                            $method_link['delete'] = "content/slider/delete";
                            $icon_img['delete'] = "fa fa-close";
                        }

                        $custom_field = array();
                        $custom_thick = "";

                        $hide_column="v.id";
                        $tbl_id = "v.id";

                        $custom_link = array();

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;  
                    case 'ajaxVoucher':
                        $columns = array( 
                        'v.id',
                        'v.voucher_id',
                        'v.name',
                        'v.price',
                        'v.point',
                        'v.expired_date',
                        'v.bonus_point',
                        'vc.name as voucher_category',
                        'v.stock',
                        'v.sold',
                        'concat("<img height=\'auto\' width=\'100px\'  src=\''.base_url().'data_uploads/voucher/",v.merchant_id,"/",v.image,"\'>") as image'
                        );
                        $nama_tbl = " voucher as v join voucher_category as vc on v.voucher_category = vc.id ";
                        $hide_column = "";
                        $custom_order = "";
                        $custom_if =array();
                        $custom_group = "";
                        $method_link = array();
                        $icon_img = array();

                        $custom_where = " stat_slider = 0 ";

                        if (in_array(4, $priviledge)) {
                            $method_link['add'] = "content/slider/add";
                            $icon_img['add'] = "fa fa-check-square";
                        }

                        $custom_field = array();
                        $custom_thick = "";

                        $hide_column="v.id";
                        $tbl_id = "v.id";

                        $custom_link = array();

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;  
                    case 'delete':
                        if (in_array(2, $priviledge)) {
                            $this->db->where('id',$idParam)->update('voucher',array('stat_slider'=>0));
                            $this->session->set_flashdata('success','Slider berhasil di hapus');
                            return redirect('content/menu/'.$requestMenu);
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    case 'add':
                        if (in_array(2, $priviledge)) {
                            $this->db->where('id',$idParam)->update('voucher',array('stat_slider'=>1));
                            $this->session->set_flashdata('success','Slider Berhasil Ditambah');
                            return redirect('content/menu/'.$requestMenu);
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return $this->hlmhelper->throwError();
            }
            
        }

        //[new function append]
}