<?php echo $this->session->userdata('id_group'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $title; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Dashboard</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Detail</h3>
            </div>
            <?php if ($statEdit==true) { ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>product/voucher/save.edit/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php }else{ ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>product/voucher/save.new/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php } ?>

            <?php $readonly = ""; 
                  $disabled = "";
            if ($statEdit==true) { $readonly = "readonly"; $disabled="disabled"; ?>
              <input name='idParam' type="hidden" value="<?php echo $idParam; ?>">
            <?php  } ?>

              <div class="box-body">
              <?php if($statEdit==true){ ?>
                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Voucher ID</label>
                  <div class="col-sm-10">
                          <input type="text" name="voucher_id" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->voucher_id; } ?>" <?php echo $readonly ?> required>
                  </div>
                </div>
              <?php } ?>

              <?php if($this->session->userdata('id_group')==1 || $this->session->userdata('id_group')==9 || $this->session->userdata('id_group')==7){ ?>
                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Company</label>
                  <div class="col-sm-10">
                    <select name="company_id" id="company_id" class="form-control" <?php echo ($this->session->userdata('id_group')==9? 'disabled' : ""); ?> >
                      <?php
                      $companylogin = false;
                      if ($this->session->userdata('id_group')==1 || $this->session->userdata('id_group')==7 ) {
                        echo '<option value="" selected>Please select</option>';
                      }else{
                        $companylogin = true;
                      }
                        $selected = "";
                        foreach ($company as $key => $value) {
                          if ($statEdit==true || $companylogin) {
                            if ($statEdit) {
                              if ($value->company_id==$dataEdit[0]->company_id) {
                                $selected= "selected";
                              }
                            }else{
                              if ($value->company_id==$this->session->userdata('company_id')) {
                                $selected= "selected";
                              }
                            }
                          }
                      ?>
                        <option value="<?php echo $value->company_id; ?>" <?php echo $selected ?>><?php echo $value->name; ?></option>
                      <?php $selected = ""; } ?>
                    </select>
                  </div>
                </div>
              <?php } ?>
              <?php if($this->session->userdata('id_group')==1  || $this->session->userdata('id_group')==9 || $this->session->userdata('id_group')==10 || $this->session->userdata('id_group')==12 || $this->session->userdata('id_group')==7){ ?>
                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Merchant</label>
                  <div class="col-sm-10">
                    <select name="merchant_id" id="merchant_id" class="form-control">
                    <?php if($statEdit){ 
                        $selected = "";
                        foreach ($merchant as $key => $value) {
                          if ($value->id==$dataEdit[0]->merchant_id) {
                            $selected= "selected";
                          }
                      ?>
                        <option value="<?php echo $value->id; ?>" <?php echo $selected ?>><?php echo $value->name; ?></option>
                      <?php $selected = ""; } ?>

                    <?php } ?>
                    </select>
                  </div>
                </div>

                <?php } ?>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                          <input type="text" name="name" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->name; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Voucher Category</label>
                  <div class="col-sm-10">
                    <select name="voucher_category" id="voucher_category" class="form-control" required>
                      <option value="">Please Select</option>
                      <?php
                      $selectedPayment = "";
                      foreach ($voucher_category as $key => $value) {
                        if ($statEdit==true) {
                          if ($value->id==$dataEdit[0]->voucher_category) {
                            $selectedPayment = "selected";
                          }
                        }
                      ?>
                        <option value="<?php echo $value->id; ?>" <?php echo $selectedPayment ?>><?php echo $value->name; ?></option>
                      <?php $selectedPayment = ""; } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group" id="voucher_add">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Point Get (Voucher Point)</label>
                  <div class="col-sm-10">
                          <input type="number" id="point_get" name="point_get" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->point_get; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Payment Type</label>
                  <div class="col-sm-10">
                    <select name="payment_type" id="payment_type" class="form-control" required>
                      <option value="">Please Select</option>
                      <?php
                      $selectedPayment = "";
                      foreach ($payment_type as $key => $value) {
                        if ($statEdit==true) {
                          if ($value->id_ref==$dataEdit[0]->payment_type) {
                            $selectedPayment = "selected";
                          }
                        }
                      ?>
                        <option value="<?php echo $value->id_ref; ?>" <?php echo $selectedPayment ?>><?php echo $value->content; ?></option>]
                      <?php $selectedPayment = ""; } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group" id="price">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Price</label>
                  <div class="col-sm-10">
                          <input type="number" id="price_id" name="price" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->price; } ?>" required>
                  </div>
                </div>

                <div class="form-group" id="point">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Point</label>
                  <div class="col-sm-10">
                          <input type="number" id="point_id" name="point" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->point; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Expired Date</label>
                  <div class="col-sm-10">
                          <input type="text" id="datepicker" name="expired_date" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->expired_date; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Bonus Point</label>
                  <div class="col-sm-10">
                          <input type="number" name="bonus_point" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->bonus_point; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Discount</label>
                  <div class="col-sm-10">
                          <input type="number" name="discount" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->discount; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Discount Plus</label>
                  <div class="col-sm-10">
                          <input type="number" name="discount_plus" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->discount_plus; } ?>" required>
                  </div>
                </div>

                

                

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Buy (item)</label>
                  <div class="col-sm-10">
                          <input type="number" name="count_buy" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->count_buy; }else{ echo "0";} ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Get (item)</label>
                  <div class="col-sm-10">
                          <input type="number" name="get_buy" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->get_buy; }else{ echo "0";} ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Stock</label>
                  <div class="col-sm-10">
                          <input type="number" name="stock" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->stock; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Term and Condition</label>
                  <div class="col-sm-10">
                    <textarea id="editor1" class="form-control" name="term_condition"><?php if ($statEdit==true) { echo $dataEdit[0]->term_condition; } ?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Information</label>
                  <div class="col-sm-10">
                    <textarea id="editor2" class="form-control" name="information"><?php if ($statEdit==true) { echo $dataEdit[0]->information; } ?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">How to Use</label>
                  <div class="col-sm-10">
                    <textarea id="editor3" class="form-control" name="how_to_use"><?php if ($statEdit==true) { echo $dataEdit[0]->how_to_use; } ?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Benefit</label>
                  <div class="col-sm-10">
                    <textarea id="editor4" class="form-control" name="benefit"><?php if ($statEdit==true) { echo $dataEdit[0]->benefit; } ?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Image</label>
                  <div class="col-sm-10">
                    <input type="file" name="image">
                  </div>
                </div>


              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a class="btn btn-danger" href="javascript: history.go(-1)">kembali</a>
              </div>
            </form>
         
          </div>
          <!-- /.box -->
        </div>

        <!--/.col (left) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
  $(document).ready(function() {
      // instance, using default configuration.
      //$('#somecomponent').locationpicker();
    //initialization 
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('editor2');
    CKEDITOR.replace('editor3');
    CKEDITOR.replace('editor4');
    $('#price').hide();
    $("#point_id").prop('required',false);
    $('#point').hide();
    $("#point_id").prop('required',false);
    $('#voucher_add').hide();
    $("#point_get").prop('required',false);

    <?php if(!$statEdit){ ?>
    $('#merchant_id').html('<option value="" selected>Please select company first</option>');
    <?php } ?>
    $( "#datepicker" ).datetimepicker({format:'YYYY/MM/DD HH:mm:ss'});
    $( "#datepicker1 " ).datepicker({
      changeMonth: true,
      changeYear: true
    });
    $( "#datepicker2" ).datepicker({
      changeMonth: true,
      changeYear: true
    });

    $( "#datepicker3" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
    //conpany and merchant handle
    $('#company_id').on('change', function() {
      var company_id = this.value;
      if (company_id!="") {
        getMerchant(company_id);
      }else{
        $('#merchant_id').html('<option value="" selected>Please select company first</option>');
      }
    });

    //payment type handle
    $('#payment_type').on('change', function() {
      var payment_type = this.value;
      //$('#point').val(0);
      //$('#price').val(0);
      //document.getElementById('point').value = 0;
      //document.getElementById('price').value = 0;
      if (payment_type==1) {
        $('#point').hide(100);
        $("#point_id").prop('required',false);
        $('#price').show(100);
        $("#price_id").prop('required',true);
      }else if(payment_type==2){
        $('#price').hide(100);
        $("#price_id").prop('required',false);
        $('#point').show(100);
        $("#point_id").prop('required',true);
      }else if(payment_type==""){
        $('#price').hide(100);
        $('#point').hide(100);
        $("#point_id").prop('required',false);
        $("#price_id").prop('required',false);
        alert('Please Select Payment Type');
      }else{
        $('#point').show(100);
        $('#price').show(100); 
        $("#point_id").prop('required',true);
        $("#point_id").prop('required',true);
      }
      
    });


    //voucher point hsndle
    $('#voucher_category').on('change', function() {
      var voucher_category = this.value;  
      if (voucher_category==1) {
        $('#voucher_add').show(100);
        $("#point_get").prop('required',true);
      }else{
        $('#voucher_add').hide(100);
        $("#point_get").prop('required',false);
      }
    });



    <?php if($this->session->userdata('id_group')==9 && !$statEdit){ ?>
      getMerchant(<?php echo $this->session->userdata('company_id'); ?>);
    <?php } ?>

    <?php if($this->session->userdata('id_group')==10 || $this->session->userdata('id_group')==13 && !$statEdit){ ?>
      getMerchant(<?php echo $this->session->userdata('company_id'); ?>);
      $('#merchant_id').prop('disabled', 'disabled');
    <?php } ?>

    
  });

  function getMerchant(company_id){
    var link = '<?php echo base_url();?>product/getMerchant/';
    $.get(link,
    {
        company_id: company_id
    },
    function(data, status){
      var select = $('#merchant_id');
      select.html(data);
    });

  }
  </script>