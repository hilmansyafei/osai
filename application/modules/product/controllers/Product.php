<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if (!$this->session->userdata('authLogin')) {
            return redirect('/login');
        }
    }

    public function menu($idMenu=''){
        $getAll = explode('.',$idMenu);
        if (isset($getAll[0])) {
            $menu = $getAll[0];
            $subMenu = $getAll[1];

            $getContent = $this->datahandler->selectData('*','sub_menu','id='.$subMenu.' and id_menu='.$menu);
            if (empty($getContent)) {
                return $this->hlmhelper->throwError();
            }
            $funcName = $getContent[0]->route_submenu;
            try {
                return $this->$funcName('get',$menu.'.'.$subMenu);
            } catch (\Exception $exception) {
                return $this->hlmhelper->throwError();
            }
        }else{
           return $this->hlmhelper->throwError();
        }    
    }

    public function getMerchant(){
        $company_id = $_GET['company_id'];
        $getAllMerchant = $this->datahandler->selectData('*','merchant','company_id='.$company_id);
        $option = "";
        foreach ($getAllMerchant as $key => $value) {
            $option .="<option value='$value->id'>$value->name</option>";
        }

        echo $option;
    }

    public function voucher($type='get',$requestMenu='',$data='')
        {
            $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',"sb.content = 'product/voucher' AND id_user =".$this->session->userdata('id'));
            $priviledge = $getPriviledge[0]->access_grant;
            $priviledge = explode(',',$priviledge);
            if (empty($priviledge[0])) {
                return $this->hlmhelper->throwError();
            }
            $nextProcess = false;
            $data = array('requestMenu'=>$requestMenu);
            $subMeniId = explode('.', $requestMenu);
            $getSubmenuName = $this->datahandler->selectData('submenu_name','sub_menu','id='.$subMeniId[1]);
            $data['title'] = $getSubmenuName[0]->submenu_name;
            if (strripos($type,'.')!=false) {
                $getData = explode('.', $type);
                $mode = $getData[0];
                $idParam = $getData[1];
                $nextProcess = true;
                $data['idParam'] = $idParam;

                if ($mode=='save') {
                    $subMode = $idParam;
                }
            }else{
                $mode = $type;
                $nextProcess = true;
            }
            $data['voucher_category'] = $this->datahandler->selectData('*','voucher_category');
            $data['company'] = $this->datahandler->selectData('*','company');

            $data['payment_type'] = $this->datahandler->selectData('*','tbl_ref','group_ref=1');
            if ($nextProcess) {
                switch ($mode) {
                    case 'get':
                        return $this->hlmhelper->genView('product/voucher',$data);
                        break;
                    case 'save':
                        switch ($subMode) {
                            case 'new':
                                //for new data insert
                                if (in_array(2, $priviledge)) {
                                    //for new data insert
                                    $allData = $this->input->post();
                                    $dataInsert = array();
                                    foreach ($allData as $key => $value) {
                                        if($key!="idParam" && $value!="Please Select"){
                                            $dataInsert[$key] = $value;
                                        }
                                    }
                                    //tambahan sementara !!!!
                                    if ($this->session->userdata('id_group')==1 || $this->session->userdata('id_group')==7) {
                                        $mercant_id = $dataInsert['company_id'] =$allData['company_id'];
                                        $dataInsert['merchant_id'] = $allData['merchant_id'];
                                    }elseif ($this->session->userdata('id_group')==9 || $this->session->userdata('id_group')==12) {
                                        $mercant_id = $dataInsert['company_id'] =$this->session->userdata('company_id');
                                        $dataInsert['merchant_id'] = $allData['merchant_id'];
                                    }elseif ($this->session->userdata('id_group')==10 || $this->session->userdata('id_group')==13) {
                                        $mercant_id = $dataInsert['company_id'] =$this->session->userdata('company_id');
                                        $dataInsert['merchant_id'] = $this->session->userdata('merchant_id');
                                    }
                                    
                                    if (!file_exists('./data_uploads/voucher/'.($mercant_id))) {
                                        mkdir('./data_uploads/voucher/'.($mercant_id),0775);
                                    }
                                    //image process
                                    $config['upload_path']          = './data_uploads/voucher/'.($mercant_id);
                                    $config['allowed_types']        = 'jpg|png|bmp|jpeg|';
                                    $config['max_size']             = 1000000;

                                    if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                                        $fileName = $_FILES['image']['name'];
                                        $ext = explode('.', $fileName);
                                        $ext = $ext[1];
                                        $newFileName = $config['file_name'] = date('Ymdhis').".".$ext;
                                        $this->load->library('upload', $config);
                                        if ( ! $this->upload->do_upload('image'))
                                        {
                                            $error = array('error' => $this->upload->display_errors());
                                            $this->session->set_flashdata('error',$error['error']);
                                            
                                            return redirect('/product/menu/'.$requestMenu);
                                        }
                                        else
                                        {   
                                            $dataInsert['image'] = trim($newFileName);
                                        }
                                    }
                                    //end image process
                                    $dataInsert['voucher_id'] = $this->hlmhelper->genVoucherId($this->session->userdata('id'));

                                    //echo "<pre>";print_r($dataInsert);die();
                                    $this->datahandler->insertData('voucher',$dataInsert);
                                    
                                    $this->session->set_flashdata('success','Data berhasil ditambah');
                                    return redirect('product/menu/'.$requestMenu);
                                    
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, $priviledge)) {
                                    //for new data insert
                                    $allData = $this->input->post();
                                    $getOldData = $this->datahandler->selectData('*','voucher','id='.$allData['idParam']);
                                    $dataInsert = array();
                                    foreach ($allData as $key => $value) {
                                        if($key!="idParam" && $value!="Please Select"){
                                            if ($key=="voucher_id") {
                                                $dataInsert[$key] = $value;
                                            }else{
                                                $dataInsert[$key] = $value;    
                                            }
                                            
                                        }
                                    }


                                    
                                    //image process
                                    $config['upload_path']          = './data_uploads/voucher/'.($dataInsert['company_id']);
                                    $config['allowed_types']        = 'png|bmp|jpeg|jpg|';
                                    $config['max_size']             = 1000000;


                                    if (!file_exists('./data_uploads/voucher/'.($dataInsert['company_id']))) {
                                        mkdir('./data_uploads/voucher/'.($dataInsert['company_id']),0775);
                                    }

                                    if (is_uploaded_file($_FILES['image']['tmp_name'])) {
                                        $fileName = $_FILES['image']['name'];
                                        $ext = explode('.', $fileName);
                                        $ext = $ext[1];
                                        $newFileName = $config['file_name'] = date('Ymdhis').".".$ext;
                                        $this->load->library('upload', $config);
                                        if ( ! $this->upload->do_upload('image'))
                                        {
                                            $error = array('error' => $this->upload->display_errors());
                                            $this->session->set_flashdata('error',$error['error']);
                                            
                                            return redirect('/product/menu/'.$requestMenu);
                                        }
                                        else
                                        {   
                                            if (file_exists('./data_uploads/voucher/'.($dataInsert['company_id'])."/".$getOldData[0]->image)) {
                                                unlink('./data_uploads/voucher/'.($dataInsert['company_id'])."/".$getOldData[0]->image);
                                            }
                                            $dataInsert['image'] = trim($newFileName);
                                        }
                                    }
                                    //end image process

                                    $this->datahandler->updateData('voucher',$dataInsert,'id='.$allData['idParam']);
                                    
                                    $this->session->set_flashdata('success','Data berhasil diedit');
                                    return redirect('product/menu/'.$requestMenu);
                                    
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'ajax':
                        $columns = array( 
                        'v.id',
                        'v.voucher_id',
                        'v.name',
                        'v.price',
                        'v.point',
                        'concat(v.discount,"%") as doscount',
                        'concat(v.discount_plus,"%") as doscount_plus',
                        'tr.content',
                        'v.expired_date',
                        'v.bonus_point',
                        'vc.name as voucher_category',
                        'v.stock',
                        'v.sold',
                        'v.count_view',
                        'concat("<img height=\'auto\' width=\'100px\'  src=\''.base_url().'data_uploads/voucher/",v.company_id,"/",v.image,"\'>") as image'
                        );
                        $nama_tbl = " voucher as v 
                                        join voucher_category as vc 
                                            on v.voucher_category = vc.id 
                                        join tbl_ref as tr
                                            on v.payment_type = tr.id_ref
                                                and group_ref=1 ";
                        $custom_where = "1 = 1 ";
                        $hide_column = "";
                        $custom_order = "v.create_date desc";
                        $custom_if =array();
                        $custom_group = "";
                        $method_link = array();
                        $icon_img = array();

                        if (!empty($this->session->userdata('merchant_id'))) {
                            $custom_where .=" and v.merchant_id =".$this->session->userdata('merchant_id');
                        }

                        if (!empty($this->session->userdata('company_id'))) {
                            $custom_where .=" and v.company_id =".$this->session->userdata('company_id');
                        }

                        $custom_where .= " and expired_date >= NOW()";

                        if (in_array(2, $priviledge)) {
                            $method_link['edit'] = "product/voucher/viewEdit";
                            $icon_img['edit'] = "fa fa-pencil";
                        }
                        if (in_array(4, $priviledge)) {
                            $method_link['delete'] = "product/voucher/delete";
                            $icon_img['delete'] = "fa fa-trash";
                        }

                        $custom_field = array();
                        $custom_thick = "";

                        $hide_column="v.id";
                        $tbl_id = "v.id";

                        $custom_link = array();

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;  
                    case 'ajaxExpired':
                        $columns = array( 
                        'v.id',
                        'v.voucher_id',
                        'v.name',
                        'v.price',
                        'v.point',
                        'tr.content',
                        'v.expired_date',
                        'v.bonus_point',
                        'vc.name as voucher_category',
                        'v.stock',
                        'v.sold',
                        'v.count_view',
                        'concat("<img height=\'auto\' width=\'100px\'  src=\''.base_url().'data_uploads/voucher/",v.merchant_id,"/",v.image,"\'>") as image'
                        );
                        $nama_tbl = " voucher as v 
                                        join voucher_category as vc 
                                            on v.voucher_category = vc.id 
                                        join tbl_ref as tr
                                            on v.payment_type = tr.id_ref
                                                and group_ref=1 ";
                        $custom_where = "1 = 1 ";
                        $hide_column = " v.expired_date asc";
                        $custom_order = "";
                        $custom_if =array();
                        $custom_group = "";
                        $method_link = array();
                        $icon_img = array();

                        if (!empty($this->session->userdata('merchant_id'))) {
                            $custom_where .=" and v.merchant_id =".$this->session->userdata('merchant_id');
                        }

                        if (!empty($this->session->userdata('company_id'))) {
                            $custom_where .=" and v.company_id =".$this->session->userdata('company_id');
                        }

                        if (in_array(2, $priviledge)) {
                            $method_link['edit'] = "product/voucher/viewEdit";
                            $icon_img['edit'] = "fa fa-pencil";
                        }
                        if (in_array(4, $priviledge)) {
                            $method_link['delete'] = "product/voucher/delete";
                            $icon_img['delete'] = "fa fa-trash";
                        }

                        $custom_where .= " and expired_date < NOW()";

                        $custom_field = array();
                        $custom_thick = "";

                        $hide_column="v.id";
                        $tbl_id = "v.id";

                        $custom_link = array();

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;  
                    case 'viewNew':
                        $data['statEdit'] = false;
                        return $this->hlmhelper->genView('product/viewEdit',$data);
                        break;
                    case 'viewEdit':
                        $data['dataEdit'] = $this->datahandler->selectData('*','voucher','id = '.$idParam);
                        $data['merchant'] = $this->datahandler->selectData('*','merchant','company_id='.$data['dataEdit'][0]->company_id);
                        $data['statEdit'] = true;
                        return $this->hlmhelper->genView('product/viewEdit',$data);
                        break;
                    case 'delete':
                        if (in_array(4, $priviledge)) {
                            $getOldData = $this->datahandler->selectData('*','voucher','id='.$idParam);

                            if (file_exists('./data_uploads/voucher/'.($getOldData[0]->company_id)."/".$getOldData[0]->image)) {
                                unlink('./data_uploads/voucher/'.($getOldData[0]->company_id)."/".$getOldData[0]->image);
                            }
                            $this->datahandler->deleteData('voucher','id='.$idParam);
                            $this->session->set_flashdata('success','Data berhasil dihapus');
                            return redirect('product/menu/'.$requestMenu);
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return $this->hlmhelper->throwError();
            }
            
        }

        //[new function append]
}