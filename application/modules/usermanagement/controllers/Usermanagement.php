<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usermanagement extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if (!$this->session->userdata('authLogin')) {
            return redirect('/login');
        }
    }

    public function menu($idMenu=''){
        $getAll = explode('.',$idMenu);
        if (isset($getAll[0])) {
            $menu = $getAll[0];
            $subMenu = $getAll[1];

            $getContent = $this->datahandler->selectData('*','sub_menu','id='.$subMenu.' and id_menu='.$menu);
            if (empty($getContent)) {
                return $this->hlmhelper->throwError();
            }
            $funcName = $getContent[0]->route_submenu;
            try {
                return $this->$funcName('get',$menu.'.'.$subMenu);
            } catch (\Exception $exception) {
                return $this->hlmhelper->throwError();
            }
        }else{
           return $this->hlmhelper->throwError();
        }    
    }

    public function mercantUser($type='get',$requestMenu='',$data='')
        {
            $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',"sb.content = 'usermanagement/mercantUser' AND id_user =".$this->session->userdata('id'));
            $priviledge = $getPriviledge[0]->access_grant;
            $priviledge = explode(',',$priviledge);
            if (empty($priviledge[0])) {
                return $this->hlmhelper->throwError();
            }
            $nextProcess = false;
            $data = array('requestMenu'=>$requestMenu);
            $subMeniId = explode('.', $requestMenu);
            $getSubmenuName = $this->datahandler->selectData('submenu_name','sub_menu','id='.$subMeniId[1]);
            $data['title'] = $getSubmenuName[0]->submenu_name;
            if (strripos($type,'.')!=false) {
                $getData = explode('.', $type);
                $mode = $getData[0];
                $idParam = $getData[1];
                $nextProcess = true;
                $data['idParam'] = $idParam;

                if ($mode=='save') {
                    $subMode = $idParam;
                }
            }else{
                $mode = $type;
                $nextProcess = true;
            }

            if ($nextProcess) {
                switch ($mode) {
                    case 'get':
                        return $this->hlmhelper->genView('usermanagement/mercantUser',$data);
                        break;
                    case 'save':
                        switch ($subMode) {
                            case 'new':
                                //for new data insert
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, $priviledge)) {
                                    //edit access
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'delete':
                        if (in_array(4, $priviledge)) {
                            //delete access
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return $this->hlmhelper->throwError();
            }
            
        }

        public function cashier($type='get',$requestMenu='',$data='')
        {
            $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',"sb.content = 'usermanagement/cashier' AND id_user =".$this->session->userdata('id'));
            $priviledge = $getPriviledge[0]->access_grant;
            $priviledge = explode(',',$priviledge);
            if (empty($priviledge[0])) {
                return $this->hlmhelper->throwError();
            }
            $nextProcess = false;
            $data = array('requestMenu'=>$requestMenu);
            $subMeniId = explode('.', $requestMenu);
            $getSubmenuName = $this->datahandler->selectData('submenu_name','sub_menu','id='.$subMeniId[1]);
            $data['title'] = $getSubmenuName[0]->submenu_name;
            if (strripos($type,'.')!=false) {
                $getData = explode('.', $type);
                $mode = $getData[0];
                $idParam = $getData[1];
                $nextProcess = true;
                $data['idParam'] = $idParam;

                if ($mode=='save') {
                    $subMode = $idParam;
                }
            }else{
                $mode = $type;
                $nextProcess = true;
            }

            if ($nextProcess) {
                switch ($mode) {
                    case 'get':
                        return $this->hlmhelper->genView('usermanagement/cashier',$data);
                        break;
                    case 'save':
                        switch ($subMode) {
                            case 'new':
                                //for new data insert
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, $priviledge)) {
                                    //edit access
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'delete':
                        if (in_array(4, $priviledge)) {
                            //delete access
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return $this->hlmhelper->throwError();
            }
            
        }

        public function users($type='get',$requestMenu='',$data='')
        {
            $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',"sb.content = 'usermanagement/users' AND id_user =".$this->session->userdata('id'));
            $priviledge = $getPriviledge[0]->access_grant;
            $priviledge = explode(',',$priviledge);
            if (empty($priviledge[0])) {
                return $this->hlmhelper->throwError();
            }
            $nextProcess = false;
            $data = array('requestMenu'=>$requestMenu);
            $subMeniId = explode('.', $requestMenu);
            $getSubmenuName = $this->datahandler->selectData('submenu_name','sub_menu','id='.$subMeniId[1]);
            $data['title'] = $getSubmenuName[0]->submenu_name;
            if (strripos($type,'.')!=false) {
                $getData = explode('.', $type);
                $mode = $getData[0];
                $idParam = $getData[1];
                $nextProcess = true;
                $data['idParam'] = $idParam;

                if ($mode=='save') {
                    $subMode = $idParam;
                }
            }else{
                $mode = $type;
                $nextProcess = true;
            }

            if ($nextProcess) {
                switch ($mode) {
                    case 'get':
                        return $this->hlmhelper->genView('usermanagement/users',$data);
                        break;
                    case 'save':
                        switch ($subMode) {
                            case 'new':
                                //for new data insert
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, $priviledge)) {
                                    //edit access
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'delete':
                        if (in_array(4, $priviledge)) {
                            //delete access
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return $this->hlmhelper->throwError();
            }
            
        }

        //[new function append]
}