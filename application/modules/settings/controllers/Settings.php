<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
	/**
	 * Copyright
	 * Hilman Syafei
	 * Februari 2017 settings
	 */
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if (!$this->session->userdata('authLogin')) {
        	return redirect('/login');
        }
    }

    public function logout($value='')
    {
        $this->session->sess_destroy();
        $getUsername = $this->session->userdata('username');
        $this->db->query('delete from ci_sessions where data like "%'.$getUsername.'%"');
        return redirect('/login');
    }

    public function menu($idMenu=''){
        $getAll = explode('.',$idMenu);
        if (isset($getAll[0])) {
            $menu = $getAll[0];
            $subMenu = $getAll[1];

            $getContent = $this->datahandler->selectData('*','sub_menu','id='.$subMenu.' and id_menu='.$menu);
            if (empty($getContent)) {
                return $this->hlmhelper->throwError();
            }
            $funcName = $getContent[0]->route_submenu;
            try {
                return $this->$funcName('get',$menu.'.'.$subMenu);
            } catch (\Exception $exception) {
                return $this->hlmhelper->throwError();
            }
        }else{
           return $this->hlmhelper->throwError();
        }    
    }

    public function userManagement($type='get',$requestMenu='',$data='')
    {	
        $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_menu` = sb.id AND up.`id_submenu` = sb.`id_menu`',"sb.content = 'settings/userManagement' AND id_user =".$this->session->userdata('id'));
        
        $priviledge = $getPriviledge[0]->access_grant;
        $priviledge = explode(',',$priviledge);
        if (empty($priviledge[0])) {
            return $this->hlmhelper->throwError();
        }
        $nextProcess = false;
        $data = array('requestMenu'=>$requestMenu);
        $data['title'] = "User Management";
        if (strripos($type,'.')!=false) {
            $getData = explode('.', $type);
            $mode = $getData[0];
            $idParam = $getData[1];
            $nextProcess = true;
            $data['idParam'] = $idParam;

            if ($mode=='save') {
                $subMode = $idParam;
            }
        }else{
            $mode = $type;
            $nextProcess = true;
        }

        $data['company'] = $this->datahandler->selectData('*','company');
        if ($this->session->userdata('level_user') == 2) {
            $data['merchant'] = $this->datahandler->selectData('*','merchant','company_id='.$this->session->userdata('company_id'));
        }

        $customWhereGroup ="";
        if ($this->session->userdata('level_user')!=1) {
            $customWhereGroup .= "level_user >= ".$this->session->userdata('level_user');
            if ($this->session->userdata('id_group') == 9) {
                $customWhereGroup .=' and id != 11';
            }
        }else{
            $customWhereGroup .= "level_user in (1,7,9) ";
        }


        if ($nextProcess) {
            switch ($mode) {
                case 'get':
                    return $this->hlmhelper->genView('settings/userManagement',$data);
                    break;
                case 'save':
                    switch ($subMode) {
                        case 'new':
                            if (in_array(2, $priviledge)) {
                                //for new data insert
                                $getAll = $this->input->post(NULL);
                                $dataInsert = array('name'=>$getAll['name'],
                                                    'email'=>$getAll['username'],
                                                    'username'=>$getAll['username'],
                                                    'password'=>file_get_contents(BASE_API_URL.'hash_pass/'.$getAll['password']),
                                                    'user_group'=>$getAll['userGroup']);
                                if($this->session->userdata('level_user')==1){
                                    if (isset($getAll['company_id'])) {
                                        $dataInsert['company_id'] = $getAll['company_id'];
                                    }                                    
                                    if (empty($getAll['company_id'])) {
                                        $dataInsert['level_user'] = 1;
                                    }else{
                                        $dataInsert['level_user'] = 2;
                                    }
                                }elseif ($this->session->userdata('level_user')==2) {
                                    $dataInsert['company_id'] = $this->session->userdata('company_id');
                                    if (isset($getAll['merchant_id'])) {
                                        $dataInsert['merchant_id'] = $getAll['merchant_id'];
                                        $dataInsert['level_user'] = 3;
                                    }else{
                                        $dataInsert['level_user'] = 2;
                                    }
                                }elseif ($this->session->userdata('level_user')==3) {
                                    $dataInsert['merchant_id'] = $this->session->userdata('merchant_id');
                                    $dataInsert['level_user'] = 3;
                                    $dataInsert['company_id'] = $this->session->userdata('company_id');
                                }
                                $checkUser = count($this->datahandler->selectData("*","users","email ='".$getAll['username']."'"));
                                if ($checkUser==0) {
                                    $idUser = $this->datahandler->insertData('users',$dataInsert,true);
                                    $getGroupPrivi = $this->datahandler->selectData('*','group_priviledge','id_user_group='.$getAll['userGroup']);
                                    for ($i=0; $i < count($getGroupPrivi); $i++) { 
                                        $dataInsert = array('id_user'=>$idUser,
                                                            'id_menu'=>$getGroupPrivi[$i]->id_menu,
                                                            'id_submenu'=>$getGroupPrivi[$i]->id_submenu,
                                                            'access_grant'=>$getGroupPrivi[$i]->access_grant);
                                        $this->datahandler->insertData('user_priviledge',$dataInsert);
                                    }
                                    $pathFolder ="data_uploads/photo_profile/".$idUser;
                                    mkdir($pathFolder,0775);
                                    $this->session->set_flashdata('success', 'Username '.$getAll['username'].' is created');
                                    return redirect('settings/menu/'.$requestMenu);
                                }
                                $this->session->set_flashdata('warning','Username has been used');
                                return redirect('settings/menu/'.$requestMenu);
                            }else{
                                return $this->hlmhelper->throwForbidden();
                            }
                            break;
                        case 'editUser':
                            if (in_array(2, $priviledge)) {
                                $getAll = $this->input->post(NULL);
                                $dataUpdate = array("name"=>$getAll['name'],
                                                    "user_group"=>$getAll['userGroup']);
                                if($this->session->userdata('level_user')==1){
                                    $dataUpdate['company_id'] = $getAll['company_id'];
                                }elseif ($this->session->userdata('level_user')==2) {
                                    $dataUpdate['company_id'] = $this->session->userdata('company_id');
                                    $dataUpdate['merchant_id'] = $getAll['merchant_id'];
                                }
                                if (!empty($getAll['password'])) {
                                    $dataUpdate['password'] = file_get_contents(BASE_API_URL.'hash_pass/'.$getAll['password']);
                                }
                                $oldUserGroup = $this->datahandler->selectData('user_group','users','id='.$getAll['idParam']);
                                $this->datahandler->updateData('users',$dataUpdate,'id='.$getAll['idParam']);
                                if ($getAll['userGroup']!=$oldUserGroup[0]->user_group) {
                                    $this->datahandler->deleteData('user_priviledge','id_user='.$getAll['idParam']);
                                    $getGroupPrivi = $this->datahandler->selectData('*','group_priviledge','id_user_group='.$getAll['userGroup']);
                                    for ($i=0; $i < count($getGroupPrivi); $i++) { 
                                        $dataInsert = array('id_user'=>$getAll['idParam'],
                                                            'id_menu'=>$getGroupPrivi[$i]->id_menu,
                                                            'id_submenu'=>$getGroupPrivi[$i]->id_submenu,
                                                            'access_grant'=>$getGroupPrivi[$i]->access_grant);
                                        $this->datahandler->insertData('user_priviledge',$dataInsert);
                                    }
                                }
                                $this->session->set_flashdata('success', 'User profile has been edited');
                                return redirect('settings/menu/'.$requestMenu);
                                //for data update
                            }else{
                                return $this->hlmhelper->throwForbidden();
                            }
                            break;
                        case 'editPrivi':
                            if (in_array(2, $priviledge)) {
                                $getAll = $this->input->post(NULL);
                                $idGroup = $getAll['idUser'];
                                $this->datahandler->deleteData('user_priviledge',' id_user ='.$getAll['idUser']);
                                if(isset($getAll['priviledge'])){
                                    foreach ($getAll['priviledge'] as $menu => $subPrivi) {
                                        foreach ($subPrivi as $sub => $privi) {
                                            foreach ($privi as $priviName => $priviValue) {
                                                $dataInsert = array('id_user'=>$getAll['idUser'],
                                                                    'id_menu'=>$menu,
                                                                    'id_submenu'=>$sub,
                                                                    'access_grant'=>$priviValue);
                                                $this->datahandler->insertData('user_priviledge',$dataInsert);
                                            }
                                        }
                                    }
                                }
                                $this->session->set_flashdata('success', 'User priviledge has been edited');
                                return redirect('settings/menu/'.$requestMenu);
                            }else{
                                return $this->hlmhelper->throwForbidden();
                            }
                            break;
                        default:
                            //default action
                            break;
                    }
                    break;
                case 'deleteUser':
                    if (in_array(4, $priviledge)) {
                        $this->datahandler->deleteData('users','id='.$idParam);
                        $this->datahandler->deleteData('user_priviledge','id_user='.$idParam);
                        $pathFolder ="data_uploads/photo_profile/".$idParam;
                        rmdir($pathFolder);
                        $this->session->set_flashdata('success', 'User has been deleted');
                        return redirect('settings/menu/'.$requestMenu);
                    }else{
                        return $this->hlmhelper->throwForbidden();
                    }
                    break;
                case 'switchStatus':
                    $getData = $this->datahandler->selectData('status,email','users','id='.$idParam);
                    if ($getData[0]->status==1) {
                        $dataUpdate = array('status'=>0);
                        $status = "Inactive";
                    }else{
                        $dataUpdate = array('status'=>1);
                        $status = "Active";
                    }

                    $this->datahandler->updateData('users',$dataUpdate,'id='.$idParam);
                    $this->session->set_flashdata('success', 'Status user '.$getData[0]->email.' has been changed to '.$status);
                    return redirect('settings/menu/'.$requestMenu);
                    break;
                case 'clearSession':
                    $getAll = $this->datahandler->selectData('email,username','users',"id=$idParam");
                    $this->datahandler->deleteData('ci_sessions','data like "%'.$getAll[0]->email.'%"');
                    $this->datahandler->deleteData('ci_sessions','data like "%'.$getAll[0]->username.'%"');
                    $this->session->set_flashdata('success', "User's Session has been cleared");
                    return redirect('settings/menu/'.$requestMenu);
                    break;
                case 'ajax':
                    $columns = array( 
                    'u.id',
                    'u.email',
                    'u.name',
                    'ug.group_name',
                    'u.status',
                    'm.name as merchant_name',
                    'c.name as company_name'
                    );

                    $custom_where = "1 = 1 ";
                    if ($idParam==1) {
                        $custom_where .= "and (user_group = ".$idParam." or user_group = 7) ";
                    }else{
                        $custom_where .= "and user_group = ".$idParam;
                    }
                    
                    $nama_tbl = "users as u 
                                    left join user_group as ug 
                                        on u.user_group = ug.id
                                    left join company as c
                                        on u.company_id = c.company_id
                                    left join merchant  as m on u.merchant_id = m.id";

                    if ($this->session->userdata('level_user')==2) {
                        $custom_where .=" and u.company_id =".$this->session->userdata('company_id')." and u.level_user in (2,3) ";

                    }elseif($this->session->userdata('level_user')==3){
                        $custom_where .=" and u.company_id =".$this->session->userdata('company_id')." and u.level_user = 3 ";
                    }
                    
                    $hide_column = "";
                    $custom_order = "";
                    $custom_if =array();
                    $custom_group = "";

                    $method_link['edit'] = "settings/userManagement/viewEditUser";
                    $icon_img['edit'] = "fa fa-pencil";

                    $method_link['delete'] = "settings/userManagement/deleteUser";
                    $icon_img['delete'] = "fa fa-trash";

                    $method_link['switch status'] = "settings/userManagement/switchStatus";
                    $icon_img['switch status'] = "fa fa-exchange";

                    $method_link['clear session'] = "settings/userManagement/clearSession";
                    $icon_img['clear session'] = "fa fa-key";

                    $status = array('0'=>"Inactive",
                                    '1'=>"Active");
                    $custom_field = array('status'=>$status);
                    $custom_thick = "";

                    $hide_column="u.id";
                    $tbl_id = "u.id";

                    $custom_link = array('u.name'=>'/userManagement/update');

                    $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                    break;
                case 'viewEditUser':
                    $data['menu'] = $this->db->query("SELECT * FROM menu ORDER BY sort_position ASC")->result();
                    $data['subMenu'] = $this->db->query("SELECT * FROM sub_menu ORDER BY id_menu ASC,sort_position ASC")->result();
                    
                    $data['groupPrivi'] = $this->datahandler->selectData('*','user_priviledge'," id_user = $idParam");
                    
                    $data['dataEdit'] = $getData = $this->datahandler->selectData('*','users','id='.$idParam);
                    $data['username'] = $getData[0]->email;
                    $data['fullName'] = $getData[0]->name;
                    $data['status'] = $getData[0]->status;
                    $data['userGroupId'] = $getData[0]->user_group;
                    $data['statEdit'] = true;
                    $data['allGroup'] = $this->datahandler->selectData('*','user_group',$customWhereGroup,'id desc');
                    return $this->hlmhelper->genView('settings/viewEditUser',$data);
                    break;
                case 'viewNewUser':
                    $data['allGroup'] = $this->datahandler->selectData('*','user_group',$customWhereGroup,'id desc');
                    $data['statEdit'] = false;
                    return $this->hlmhelper->genView('settings/viewEditUser',$data);
                    break;
                case 'update':
                    //check acces update user
                    if ($idParam==$this->session->userdata('id')) {

                    }else{
                        //throw error forbidden access
                        return $this->hlmhelper->throwForbidden();
                    }
                    break;
                default:
                    //default action
                    break;
            }
        }else{
            //throw error
            $this->hlmhelper->throwError();
        }
    }

    public function GroupManagement($type='get',$requestMenu='',$data='')
    {   
        $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_menu` = sb.id AND up.`id_submenu` = sb.`id_menu`',"sb.content = 'settings/userManagement' AND id_user =".$this->session->userdata('id'));
        $priviledge = $getPriviledge[0]->access_grant;
        $priviledge = explode(',',$priviledge);
        if (empty($priviledge[0])) {
            return $this->hlmhelper->throwError();
        }
        $nextProcess = false;
        $data = array('requestMenu'=>$requestMenu);
        $data['title'] = "Group Management";
        if (strripos($type,'.')!=false) {
            $getData = explode('.', $type);
            $mode = $getData[0];
            $idParam = $getData[1];
            $nextProcess = true;
            $data['idParam'] = $idParam;

            if ($mode=='save') {
                $subMode = $idParam;
            }
        }else{
            $mode = $type;
            $nextProcess = true;
        }

        if ($nextProcess) {
            switch ($mode) {
                case 'get':
                    return $this->hlmhelper->genView('settings/groupManagement',$data);
                    break;
                case 'save':
                    switch ($subMode) {
                        case 'new':
                            if (in_array(2, $priviledge)) {
                                //for new data insert
                                $getAll = $this->input->post(NULL);
                                $dataInsert = array('group_name'=>$getAll['group_name']);
                                $checkGroup = count($this->datahandler->selectData("*","user_group","group_name ='".$getAll['group_name']."'"));
                                if ($checkGroup==0) {
                                    $this->datahandler->insertData('user_group',$dataInsert);
                                    $this->session->set_flashdata('success', 'Group '.$getAll['group_name'].' created');
                                    return redirect('settings/menu/'.$requestMenu);
                                }else{
                                    $this->session->set_flashdata('warning','User group name has been used');
                                    return redirect('settings/menu/'.$requestMenu);
                                }
                            }else{
                                return $this->hlmhelper->throwForbidden();
                            }
                            break;
                        case 'editGroup':
                            if (in_array(2, $priviledge)) {
                                $getAll = $this->input->post(NULL);
                                
                                $dataUpdate = array("group_name"=>$getAll['group_name']);
                                $this->datahandler->updateData('user_group',$dataUpdate,'id='.$getAll['idParam']);
                             
                                $this->session->set_flashdata('success', 'User group has been edited');
                                return redirect('settings/menu/'.$requestMenu);
                                //for data update
                            }else{
                                return $this->hlmhelper->throwForbidden();
                            }
                            break;
                        case 'editPrivi':
                            if (in_array(2, $priviledge)) {
                                $getAll = $this->input->post(NULL);
                                $idGroup = $getAll['idGroup'];
                                $this->datahandler->deleteData('group_priviledge',' id_user_group ='.$idGroup);
                                if(isset($getAll['priviledge'])){
                                    foreach ($getAll['priviledge'] as $menu => $subPrivi) {
                                        foreach ($subPrivi as $sub => $privi) {
                                            foreach ($privi as $priviName => $priviValue) {
                                                $dataInsert = array('id_user_group'=>$idGroup,
                                                                    'id_menu'=>$menu,
                                                                    'id_submenu'=>$sub,
                                                                    'access_grant'=>$priviValue);
                                                $this->datahandler->insertData('group_priviledge',$dataInsert);
                                            }
                                        }
                                    }
                                }
                                $this->session->set_flashdata('success', 'User group priviledge has been edited');
                                return redirect('settings/menu/'.$requestMenu);
                            }else{
                                return $this->hlmhelper->throwForbidden();
                            }
                            break;
                        default:
                            //default action
                            break;
                    }
                    break;
                case 'deleteGroup':
                    if (in_array(4, $priviledge)) {
                        $this->datahandler->deleteData('user_group','id='.$idParam);
                        $this->datahandler->deleteData('group_priviledge','id_user_group='.$idParam);
                        $this->session->set_flashdata('success', 'User group has been deleted');
                        return redirect('settings/menu/'.$requestMenu);
                    }else{
                        return $this->hlmhelper->throwForbidden();
                    }
                    break;
                case 'ajax':
                    $columns = array( 
                    'id',
                    'group_name'
                    );
                    $nama_tbl = "user_group";
                    $custom_where = "";
                    $hide_column = "";
                    $custom_order = "";
                    $custom_if =array();
                    $custom_group = "";

                    $method_link['edit'] = "settings/groupManagement/viewEditGroup";
                    $icon_img['edit'] = "fa fa-pencil";

                    $method_link['delete'] = "settings/groupManagement/deleteGroup";
                    $icon_img['delete'] = "fa fa-trash";

                    $custom_field = array();
                    $custom_thick = "";

                    $hide_column="id";
                    $tbl_id = "id";

                    $custom_link = array();

                    $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column="id",$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                    break;
                case 'viewEditGroup':
                    $data['menu'] = $this->db->query("SELECT * FROM menu ORDER BY sort_position ASC")->result();
                    $data['subMenu'] = $this->db->query("SELECT * FROM sub_menu ORDER BY id_menu ASC,sort_position ASC")->result();
                    
                    $data['groupPrivi'] = $this->datahandler->selectData('*','group_priviledge'," id_user_group = $idParam");
                    
                    $getData = $this->datahandler->selectData('*','user_group','id='.$idParam);
                    $data['groupName'] = $getData[0]->group_name;
                    $data['statEdit'] = true;
                    return $this->hlmhelper->genView('settings/viewEditGroup',$data);
                    break;
                case 'viewNewGroup':
                    $data['statEdit'] = false;
                    return $this->hlmhelper->genView('settings/viewEditGroup',$data);
                    break;
                default:
                    //default action
                    break;
            }
        }else{
            //throw error
            $this->hlmhelper->throwError();
        }
    }

    public function MenuManagement($type='get',$requestMenu='',$data='')
    {   
        $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_menu` = sb.id AND up.`id_submenu` = sb.`id_menu`',"sb.content = 'settings/userManagement' AND id_user =".$this->session->userdata('id'));
        $priviledge = $getPriviledge[0]->access_grant;
        $priviledge = explode(',',$priviledge);
        if (empty($priviledge[0])) {
            return $this->hlmhelper->throwError();
        }
        $nextProcess = false;
        $data = array('requestMenu'=>$requestMenu);
        $data['title'] = "Menu Management";
        if (strripos($type,'.')!=false) {
            $getData = explode('.', $type);
            $mode = $getData[0];
            $idParam = $getData[1];
            $nextProcess = true;
            $data['idParam'] = $idParam;

            if ($mode=='save') {
                $subMode = $idParam;
            }
        }else{
            $mode = $type;
            $nextProcess = true;
        }

        if ($nextProcess) {
            switch ($mode) {
                case 'get':
                    return $this->hlmhelper->genView('settings/menuManagement',$data);
                    break;
                case 'save':
                    switch ($subMode) {
                        case 'new':
                            if (in_array(2, $priviledge)) {
                                //for new data insert
                                $cekMaxPosition = $this->db->query('select max(sort_position) as max_sort from menu')->result();
                                $getAll = $this->input->post(NULL);
                                $dataInsert = array('menu_name'=>$getAll['menu_name'],
                                                    'image'=>'fa fa-cube',
                                                    'content'=>strtolower($getAll['content']),
                                                    'access_group'=>4,
                                                    'sort_position'=>$cekMaxPosition[0]->max_sort+1
                                                    );
                                $checkMenu = count($this->datahandler->selectData("*","menu","menu_name ='".$getAll['menu_name']."'"));
                                if ($checkMenu==0) {
                                    $this->datahandler->insertData('menu',$dataInsert);
                                    $this->newController($getAll['content']);
                                    $this->session->set_flashdata('success', 'Menu '.$getAll['menu_name'].' created');
                                    return redirect('settings/menu/'.$requestMenu);
                                }else{
                                    $this->session->set_flashdata('warning','Menu name has been used');
                                    return redirect('settings/menu/'.$requestMenu);
                                }
                            }else{
                                return $this->hlmhelper->throwForbidden();
                            }
                            break;
                        case 'editMenu':
                            if (in_array(2, $priviledge)) {
                                $getAll = $this->input->post(NULL);
                                $dataUpdate = array('menu_name'=>$getAll['menu_name'],
                                                    'image'=>'"'.$getAll["image"].'"');
                                $this->datahandler->updateData('menu',$dataUpdate,'id='.$getAll['idParam']);
                             
                                $this->session->set_flashdata('success', 'Menu has been edited');
                                return redirect('settings/menu/'.$requestMenu);
                                //for data update
                            }else{
                                return $this->hlmhelper->throwForbidden();
                            }
                            break;
                        case 'newSubmenu':
                            //for new data insert
                            $getAll = $this->input->post(NULL);
                            $getMaxOrderGet = $this->db->query('select max(sort_position) as sort_pst from sub_menu where id_menu ='.$getAll['idMenu'])->result();
                            if (!empty($getMaxOrderGet[0]->sort_pst) && $getMaxOrderGet[0]->sort_pst !=0 ) {
                                $getMaxOrder = $getMaxOrderGet[0]->sort_pst;   
                            }else{
                                $getMaxOrder = -1;
                            }
                            $controllerGet =  explode('/', $getAll['function_name']);
                            $controllerName = $controllerGet[0];
                            $functionName = $controllerGet[1];
                            if (empty($functionName)) {
                                $this->session->set_flashdata('error',"Function's Name Not Found");
                                return redirect('settings/menuManagement/viewSubmenu.'.$getAll['idMenu'].'/'.$requestMenu);
                            }
                            
                            $dataInsert = array('submenu_name' =>$getAll['submenu_name'],
                                                'id_menu' =>$getAll['idMenu'],
                                                'sort_position' => $getMaxOrder+1,
                                                'route_submenu' => $functionName,
                                                'content'   =>$controllerName."/".$functionName);
                            $checkSubmenu = $this->datahandler->selectData('*','sub_menu','content like "'.$controllerName.'/'.$functionName.'"');
                            if (count($checkSubmenu)>0) {
                                $this->session->set_flashdata('warning',"Function's name has been used");
                                return redirect('settings/menuManagement/viewSubmenu.'.$getAll['idMenu'].'/'.$requestMenu);
                            }else{
                                $dataUpdate = array('total_submenu'=>'total_submenu+1');
                                $this->datahandler->updateData('menu',$dataUpdate,' id = '.$getAll['idMenu']);
                                $this->newFunction($functionName,$controllerName,$getAll['submenu_name']);
                                $this->datahandler->insertData('sub_menu',$dataInsert);
                                $this->session->set_flashdata('success',"Submenu has been created");
                                return redirect('settings/menuManagement/viewSubmenu.'.$getAll['idMenu'].'/'.$requestMenu);
                            }  
                            break;
                        case 'editSubmenu':
                            //for data update
                            $getAll = $this->input->post(NULL);
                            $dataUpdate = array("submenu_name"=>$getAll['submenu_name']);
                            $this->datahandler->updateData('sub_menu',$dataUpdate,'id='.$getAll['idParam']);
                            
                            $this->session->set_flashdata('success',"Submenu has been edited");
                            return redirect('settings/menuManagement/viewSubmenu.'.$getAll['idMenu'].'/'.$requestMenu);
                            break;
                        default:
                            //default action
                            break;
                    }
                    break;
                case 'upSort':
                    $getData = $this->datahandler->selectData('*','menu','id='.$idParam);
                    $getDataAbove = $this->datahandler->selectData('*','menu','sort_position='.($getData[0]->sort_position-1));
                    $getMinSort = $this->datahandler->selectData('min(sort_position) as minSort','menu');
                    $minSort = $getMinSort[0]->minSort;
                    if ($getData[0]->sort_position!=$minSort) {
                        $dataUpdateMin = array('sort_position' =>'sort_position-1');
                        $dataUpdatePlus = array('sort_position' =>'sort_position+1');
                        $this->datahandler->updateData('menu',$dataUpdateMin,'id='.$idParam);  
                        $this->datahandler->updateData('menu',$dataUpdatePlus,'id='.$getDataAbove[0]->id); 
                    }
                    $this->session->set_flashdata('success','Sort position has been Changed');
                    return redirect('settings/menu/'.$requestMenu);
                    break;
                case 'downSort':
                    $getData = $this->datahandler->selectData('*','menu','id='.$idParam);
                    $getDataDown = $this->datahandler->selectData('*','menu','sort_position='.($getData[0]->sort_position+1));
                    $getMaxSort = $this->datahandler->selectData('max(sort_position) as maxSort','menu');
                    $maxSort = $getMaxSort[0]->maxSort;
                    if ($getData[0]->sort_position!=$maxSort) {
                        $dataUpdateMin = array('sort_position' =>'sort_position-1');
                        $dataUpdatePlus = array('sort_position' =>'sort_position+1');
                        $this->datahandler->updateData('menu',$dataUpdateMin,'id='.$getDataDown[0]->id); 
                        $this->datahandler->updateData('menu',$dataUpdatePlus,'id='.$idParam);  
                    }
                    $this->session->set_flashdata('success','Sort position has been Changed');
                    return redirect('settings/menu/'.$requestMenu);
                    break;
                case 'upSortSubmenu':
                    $getData = $this->datahandler->selectData('*','sub_menu','id='.$idParam);
                    $getDataAbove = $this->datahandler->selectData('*','sub_menu','sort_position='.($getData[0]->sort_position-1));
                    $getMinSort = $this->datahandler->selectData('min(sort_position) as minSort','sub_menu');
                    $minSort = $getMinSort[0]->minSort;
                    if ($getData[0]->sort_position!=$minSort) {
                        $dataUpdateMin = array('sort_position' =>'sort_position-1');
                        $dataUpdatePlus = array('sort_position' =>'sort_position+1');
                        $this->datahandler->updateData('sub_menu',$dataUpdateMin,'id='.$idParam);  
                        $this->datahandler->updateData('sub_menu',$dataUpdatePlus,'id='.$getDataAbove[0]->id); 
                    }
                    $this->session->set_flashdata('success','Sort position has been Changed');
                    return redirect('settings/menuManagement/viewSubmenu.'.$getData[0]->id_menu.'/'.$requestMenu);
                    break;
                case 'downSortSubmenu':
                    $getData = $this->datahandler->selectData('*','sub_menu','id='.$idParam);
                    $getDataDown = $this->datahandler->selectData('*','sub_menu','sort_position='.($getData[0]->sort_position+1));
                    $getMaxSort = $this->datahandler->selectData('max(sort_position) as maxSort','sub_menu');
                    $maxSort = $getMaxSort[0]->maxSort;
                    if ($getData[0]->sort_position!=$maxSort) {
                        $dataUpdateMin = array('sort_position' =>'sort_position-1');
                        $dataUpdatePlus = array('sort_position' =>'sort_position+1');
                        $this->datahandler->updateData('sub_menu',$dataUpdateMin,'id='.$getDataDown[0]->id); 
                        $this->datahandler->updateData('sub_menu',$dataUpdatePlus,'id='.$idParam);  
                    }
                    $this->session->set_flashdata('success','Sort position has been Changed');
                    return redirect('settings/menuManagement/viewSubmenu.'.$getData[0]->id_menu.'/'.$requestMenu);
                    break;
                case 'ajax':
                    $columns = array( 
                    'id',
                    'menu_name',
                    'total_submenu',
                    'image',
                    'sort_position'
                    );
                    $nama_tbl = "menu";
                    $custom_where = "";
                    $hide_column = "";
                    $custom_order = "sort_position asc";
                    $custom_if =array();
                    $custom_group = "";

                    $method_link['edit'] = "settings/menuManagement/viewEditMenu";
                    $icon_img['edit'] = "fa fa-pencil";

                    $method_link['up'] = "settings/menuManagement/upSort";
                    $icon_img['up'] = "fa fa-sort-asc";

                    $method_link['down'] = "settings/menuManagement/downSort";
                    $icon_img['down'] = "fa fa-sort-desc";

                    $hide_column="id";
                    $tbl_id = "id";

                    $custom_field = array();
                    $custom_thick = "";

                    $custom_link = array('menu_name' => 'settings/menuManagement/viewSubmenu');

                    return $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column="id",$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                    break;
                case 'ajaxSubmenu':
                    $columns = array( 
                    'id',
                    'submenu_name',
                    'content',
                    'sort_position'
                    );
                    $nama_tbl = "sub_menu";
                    $custom_where = " id_menu = $idParam";
                    $hide_column = "id";
                    $custom_order = "sort_position asc";
                    $custom_if =array();
                    $custom_group = "";

                    $method_link['edit'] = "settings/menuManagement/viewEditSubmenu";
                    $icon_img['edit'] = "fa fa-pencil";

                    $method_link['up'] = "settings/menuManagement/upSortSubmenu";
                    $icon_img['up'] = "fa fa-sort-asc";

                    $method_link['down'] = "settings/menuManagement/downSortSubmenu";
                    $icon_img['down'] = "fa fa-sort-desc";

                    $custom_field = array();
                    $custom_thick = "";

                    $hide_column="id";
                    $tbl_id = "id";

                    $custom_link = array();

                    return $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column="id",$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                    break;
                case 'viewSubmenu':
                    return $this->hlmhelper->genView('settings/submenuManagement',$data);
                    break;
                case 'viewEditMenu':
                    $getData = $this->db->query("SELECT * FROM menu WHERE id = $idParam")->result();
                    $data['menuName'] = $getData[0]->menu_name;
                    $data['image'] = $getData[0]->image;
                    $data['statEdit'] = true;
                    return $this->hlmhelper->genView('settings/viewEditMenu',$data);
                    break;
                case 'viewNewMenu':
                    $data['statEdit'] = false;
                    return $this->hlmhelper->genView('settings/viewEditMenu',$data);
                    break;
                case 'viewEditSubmenu':
                    $getData = $this->db->query("SELECT * FROM sub_menu WHERE id = $idParam")->result();
                    $data['submenuName'] = $getData[0]->submenu_name;
                    $data['functionName'] = $getData[0]->content;
                    $data['idMenu'] = $getData[0]->id_menu;
                    $data['statEdit'] = true;
                    return $this->hlmhelper->genView('settings/viewEditSubmenu',$data);
                    break;
                case 'viewNewSubmenu':
                    $data['statEdit'] = false;
                    $data['idMenu'] = $idParam;
                    $getDataMenu = $this->datahandler->selectData('content','menu',"id=$idParam");
                    $data['menuName'] = strtolower($getDataMenu[0]->content)."/";
                    return $this->hlmhelper->genView('settings/viewEditSubmenu',$data);
                    break;
                default:
                    //default action
                    break;
            }
        }else{
            //throw error
            $this->hlmhelper->throwError();
        }
    }

    public function newController($fileNameGet='New')
    {
        $fileName =strtolower($fileNameGet);
        //list path
        $pathFolder = "application/modules/".$fileName; 
        $pathViews = "application/modules/".$fileName."/views"; 
        $pathControllers = "application/modules/".$fileName."/controllers";
        $pathModels = "application/modules/".$fileName."/models";
        
        //create requirment folder
        mkdir($pathFolder,0775);
        mkdir($pathViews,0775);
        mkdir($pathControllers,0775);
        mkdir($pathModels,0775);

        $controllerFile = fopen($pathControllers."/".$fileNameGet.".php", "w") or die("Failed Create File");
        $defaultContent = "<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ".$fileNameGet." extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if (!\$this->session->userdata('authLogin')) {
            return redirect('/login');
        }
    }

    public function menu(\$idMenu=''){
        \$getAll = explode('.',\$idMenu);
        if (isset(\$getAll[0])) {
            \$menu = \$getAll[0];
            \$subMenu = \$getAll[1];

            \$getContent = \$this->datahandler->selectData('*','sub_menu','id='.\$subMenu.' and id_menu='.\$menu);
            if (empty(\$getContent)) {
                return \$this->hlmhelper->throwError();
            }
            \$funcName = \$getContent[0]->route_submenu;
            try {
                return \$this->\$funcName('get',\$menu.'.'.\$subMenu);
            } catch (\Exception \$exception) {
                return \$this->hlmhelper->throwError();
            }
        }else{
           return \$this->hlmhelper->throwError();
        }    
    }\n
    //[new function append]
}";
        fwrite($controllerFile, $defaultContent);
        fclose($controllerFile);
        chmod($pathControllers."/".$fileNameGet.".php", 0775);
        return 1;
    }

    public function newFunction($funcName="newFunction",$fileNameGet="Test",$submenuName="")
    {   
        $fileNameReal = strtoupper(substr($fileNameGet, 0,1));
        $fileNameReal .= substr($fileNameGet,1);

        $folderName = $fileNameGet;
        $fileName = strtolower($fileNameGet);
        $pathControllers = "application/modules/".$fileName."/controllers";
        $pathFolder = "application/modules/".$fileName; 
        ##create function
        $file = $pathControllers."/".$fileNameReal.".php";
        // Open the file to get existing content
        $current = file_get_contents($file);
        $newFunction ="public function ".$funcName."(\$type='get',\$requestMenu='',\$data='')
        {
            \$getPriviledge = \$this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',\"sb.content = '".$fileName."/".$funcName."' AND id_user =\".\$this->session->userdata('id'));
            \$priviledge = \$getPriviledge[0]->access_grant;
            \$priviledge = explode(',',\$priviledge);
            if (empty(\$priviledge[0])) {
                return \$this->hlmhelper->throwError();
            }
            \$nextProcess = false;
            \$data = array('requestMenu'=>\$requestMenu);
            \$subMeniId = explode('.', \$requestMenu);
            \$getSubmenuName = \$this->datahandler->selectData('submenu_name','sub_menu','id='.\$subMeniId[1]);
            \$data['title'] = \$getSubmenuName[0]->submenu_name;
            if (strripos(\$type,'.')!=false) {
                \$getData = explode('.', \$type);
                \$mode = \$getData[0];
                \$idParam = \$getData[1];
                \$nextProcess = true;
                \$data['idParam'] = \$idParam;

                if (\$mode=='save') {
                    \$subMode = \$idParam;
                }
            }else{
                \$mode = \$type;
                \$nextProcess = true;
            }

            if (\$nextProcess) {
                switch (\$mode) {
                    case 'get':
                        return \$this->hlmhelper->genView('".$fileName."/".$funcName."',\$data);
                        break;
                    case 'save':
                        switch (\$subMode) {
                            case 'new':
                                //for new data insert
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, \$priviledge)) {
                                    //edit access
                                }else{
                                    return \$this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'delete':
                        if (in_array(4, \$priviledge)) {
                            //delete access
                        }else{
                            return \$this->hlmhelper->throwForbidden();
                        }
                        break;
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return \$this->hlmhelper->throwError();
            }
            
        }\n
        //[new function append]";
        $newContent = str_replace('//[new function append]', $newFunction, $current);
        // Write the contents back to the file
        file_put_contents($file, $newContent); 

        copy("application/views/template/defaultView.php", $pathFolder."/views/".$funcName.".php");             
        chmod($pathFolder."/views/".$funcName.".php", 0775);
        
        return $current;
    }
}