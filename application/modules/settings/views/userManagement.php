<script>
  $(function () {
    $('#example7').DataTable({
      "scrollX": true,
      "scrollY": 'auto',
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :'<?php echo base_url()."settings/usermanagement/ajax.0/".$requestMenu?>', // json datasource
        type: "GET",  // method  , by default get
      }
    });

    $('#example1').DataTable({
      "scrollX": true,
      "scrollY": 'auto',
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :'<?php echo base_url()."settings/usermanagement/ajax.1/".$requestMenu?>', // json datasource
        type: "GET",  // method  , by default get
      }
    });

    $('#example2').DataTable({
      "scrollX": true,
      "scrollY": 'auto',
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :'<?php echo base_url()."settings/usermanagement/ajax.9/".$requestMenu?>', // json datasource
        type: "GET",  // method  , by default get
      }
    });

    $('#example3').DataTable({
      "scrollX": true,
      "scrollY": 'auto',
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :'<?php echo base_url()."settings/usermanagement/ajax.10/".$requestMenu?>', // json datasource
        type: "GET",  // method  , by default get
      }
    });

    $('#example4').DataTable({
      "scrollX": true,
      "scrollY": 'auto',
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :'<?php echo base_url()."settings/usermanagement/ajax.12/".$requestMenu?>', // json datasource
        type: "GET",  // method  , by default get
      }
    });

    $('#example5').DataTable({
      "scrollX": true,
      "scrollY": 'auto',
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :'<?php echo base_url()."settings/usermanagement/ajax.13/".$requestMenu?>', // json datasource
        type: "GET",  // method  , by default get
      }
    });

    $('#example6').DataTable({
      "scrollX": true,
      "scrollY": 'auto',
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        url :'<?php echo base_url()."settings/usermanagement/ajax.11/".$requestMenu?>', // json datasource
        type: "GET",  // method  , by default get
      }
    });
    $('.table th').addClass('bg-blue');
  });
</script>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $title; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Dashboard </a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">

        <?php if ($this->session->userdata('level_user') == 1) {?>
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"> <b> List Super Admin </b> </h3>
            </div>
            <!-- /.box-header -->
            <a href="<?=base_url()?>settings/userManagement/viewNewUser/<?php echo $requestMenu; ?>" class="btn btn-primary" style="margin-left:10px">Add New User</a>
            <div class="box-body">
              <?php if ($this->session->flashdata('success')) { ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>

              <?php if ($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?>

              <?php if ($this->session->flashdata('warning')) { ?>
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning"></i> Warning!</h4>
                <?php echo $this->session->flashdata('warning'); ?>
              </div>
              <?php } ?>

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Full Name</th>
                  <th>User Group</th>
                  <th>Status</th>
                  <th>Merchant</th>
                  <th>Company</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <?php } ?>
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"> <b> List Admin Company </b> </h3>
            </div>
            <!-- /.box-header -->
            <a href="<?=base_url()?>settings/userManagement/viewNewUser/<?php echo $requestMenu; ?>" class="btn btn-primary" style="margin-left:10px">Add New User</a>
            <div class="box-body">

              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Full Name</th>
                  <th>User Group</th>
                  <th>Status</th>
                  <th>Merchant</th>
                  <th>Company</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"> <b> List Admin Merchant </b> </h3>
            </div>
            <!-- /.box-header -->
            <a href="<?=base_url()?>settings/userManagement/viewNewUser/<?php echo $requestMenu; ?>" class="btn btn-primary" style="margin-left:10px">Add New User</a>
            <div class="box-body">

              <table id="example3" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Full Name</th>
                  <th>User Group</th>
                  <th>Status</th>
                  <th>Merchant</th>
                  <th>Company</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"> <b> List Manager Company </b> </h3>
            </div>
            <!-- /.box-header -->
            <a href="<?=base_url()?>settings/userManagement/viewNewUser/<?php echo $requestMenu; ?>" class="btn btn-primary" style="margin-left:10px">Add New User</a>
            <div class="box-body">

              <table id="example4" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Full Name</th>
                  <th>User Group</th>
                  <th>Status</th>
                  <th>Merchant</th>
                  <th>Company</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"> <b> List Manager Merchant </b> </h3>
            </div>
            <!-- /.box-header -->
            <a href="<?=base_url()?>settings/userManagement/viewNewUser/<?php echo $requestMenu; ?>" class="btn btn-primary" style="margin-left:10px">Add New User</a>
            <div class="box-body">

              <table id="example5" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Full Name</th>
                  <th>User Group</th>
                  <th>Status</th>
                  <th>Merchant</th>
                  <th>Company</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><b> List Cashier </b> </h3>
            </div>
            <!-- /.box-header -->
            <a href="<?=base_url()?>settings/userManagement/viewNewUser/<?php echo $requestMenu; ?>" class="btn btn-primary" style="margin-left:10px">Add New User</a>
            <div class="box-body">

              <table id="example6" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Full Name</th>
                  <th>User Group</th>
                  <th>Status</th>
                  <th>Merchant</th>
                  <th>Company</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"> <b> List Member </b> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table id="example7" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Full Name</th>
                  <th>User Group</th>
                  <th>Status</th>
                  <th>Merchant</th>
                  <th>Company</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->