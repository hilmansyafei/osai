<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $title; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Dashboard</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <!-- left column -->
        <div class="col-md-5">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Menu</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if ($statEdit==true) { ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>settings/menuManagement/save.editMenu/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php }else{ ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>settings/menuManagement/save.new/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php } ?>

            <?php $readonly = ""; 
                  $disabled = "";
            if ($statEdit==true) { $readonly = "readonly"; $disabled="disabled"; ?>
              <input name='idParam' type="hidden" value="<?php echo $idParam; ?>">
            <?php  } ?>

              <div class="box-body">

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-4 control-label">Menu Name</label>
                  <div class="col-sm-8">
                          <input type="text" name="menu_name" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $menuName; } ?>" required>
                  </div>
                </div>
                <?php if($statEdit==true){ ?>
                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-4 control-label">Image</label>
                  <div class="col-sm-8">
                          <input type="text" name="image" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $image; }else{ echo 'fa fa-cube'; } ?>" >
                  </div>
                </div>  
                <?php }else{ ?>
                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-4 control-label">Controller Name</label>
                  <div class="col-sm-8">
                          <input type="text" name="content" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $content; } ?>" required>
                  </div>
                </div>  
                <?php } ?>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a class="btn btn-danger" href="javascript: history.go(-1)">kembali</a>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>

        
        <!--/.col (left) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->