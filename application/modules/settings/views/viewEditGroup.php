<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $title; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i>Dashboard</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <!-- left column -->
        <div class="col-md-5">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Group</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if ($statEdit==true) { ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>settings/groupManagement/save.editGroup/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php }else{ ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>settings/groupManagement/save.new/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php } ?>

            <?php $readonly = ""; 
                  $disabled = "";
            if ($statEdit==true) { $readonly = "readonly"; $disabled="disabled"; ?>
              <input name='idParam' type="hidden" value="<?php echo $idParam; ?>">
            <?php  } ?>

              <div class="box-body">

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-4 control-label">Group Name</label>
                  <div class="col-sm-8">
                          <input type="text" name="group_name" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $groupName; } ?>" >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a class="btn btn-danger" href="javascript: history.go(-1)">kembali</a>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>

        <?php if($statEdit==true){ ?>
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Privilegde</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if ($statEdit==true) { ?>
              <form role="form" class="form-horizontal" method="POST" action="<?=base_url()?>settings/groupManagement/save.editPrivi/<?php echo $requestMenu; ?>" enctype="multipart/form-data">
            <?php }else{ ?>
              <form role="form" class="form-horizontal" method="POST" action="<?=base_url()?>settings/groupManagement/save.editPrivi/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php } ?>

            <?php $readonly = ""; 
                  $disabled = "";
            if ($statEdit==true) { $readonly = "readonly"; $disabled="disabled"; ?>
              <input name='idGroup' type="hidden" value="<?php echo $idParam ?>">
            <?php  } ?>

              <div class="box-body">

                <?php for ($i=0; $i <count($menu) ; $i++) {?>
                  <label><?php echo $menu[$i]->menu_name; ?></label>
                  <div class="well">
                    <?php for ($j=0; $j <count($subMenu) ; $j++) { 
                      if($menu[$i]->id == $subMenu[$j]->id_menu){ ?>
                        <b><?php echo $subMenu[$j]->submenu_name; ?></b>
                        <p style="padding: 5px;">
                          <?php 
                            $cek1 = "";
                            $cek2 = "";
                            $cek3 = "";
                            for ($k=0; $k < count($groupPrivi) ; $k++) { 
                              if ($groupPrivi[$k]->id_menu == $menu[$i]->id && $groupPrivi[$k]->id_submenu == $subMenu[$j]->id && $groupPrivi[$k]->access_grant == 1 ){
                                $cek1 = "checked";
                              }elseif ($groupPrivi[$k]->id_menu == $menu[$i]->id && $groupPrivi[$k]->id_submenu == $subMenu[$j]->id && $groupPrivi[$k]->access_grant == 2 ) {                                     
                                $cek2 = "checked";
                              }elseif ($groupPrivi[$k]->id_menu == $menu[$i]->id && $groupPrivi[$k]->id_submenu == $subMenu[$j]->id && $groupPrivi[$k]->access_grant == 4) {
                                $cek3 = "checked";
                              }
                            } ?>
                          <input type="checkbox" name="priviledge[<?php echo $subMenu[$j]->id_menu ?>][<?php echo $subMenu[$j]->id ?>][R]"  value="1"  class="flat-red" <?php echo $cek1 ?> /> Read
                          <input type="checkbox" name="priviledge[<?php echo $subMenu[$j]->id_menu ?>][<?php echo $subMenu[$j]->id ?>][W]"  value="2" class="flat"  <?php echo $cek2 ?> /> Write
                          <input type="checkbox" name="priviledge[<?php echo $subMenu[$j]->id_menu ?>][<?php echo $subMenu[$j]->id ?>][X]"  value="4" class="flat"  <?php echo $cek3 ?> /> Delete
                        </p>
                    <?php     
                          } 
                        } ?>
                  </div>
                <?php } ?>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a class="btn btn-danger" href="javascript: history.go(-1)">kembali</a>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <?php } ?>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->