<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $title; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Dashboard</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <!-- left column -->
        <div class="col-md-5">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Detail User</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if ($statEdit==true) { ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>settings/userManagement/save.editUser/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php }else{ ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>settings/userManagement/save.new/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php } ?>

            <?php $readonly = ""; 
                  $disabled = "";
            if ($statEdit==true) { $readonly = "readonly"; $disabled="disabled"; ?>
              <input name='idParam' type="hidden" value="<?php echo $idParam; ?>">
            <?php  } ?>

              <div class="box-body">

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Username</label>
                  <div class="col-sm-10">
                          <input type="text" name="username" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $username; } ?>" <?php if($statEdit==true){ echo 'readonly';} ?>>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                          <input type="text" name="name" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $fullName;  } ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                          <input type="password" name="password" class="form-control" 
                          value="">
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">User Group</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <select class="form-control" name="userGroup" required>
                      <option value="">Choose option</option>
                      <?php for ($i=0; $i < count($allGroup) ; $i++) { 
                              if ($statEdit==true) {?>
                                <?php if($allGroup[$i]->id == $userGroupId){ ?>
                                  <option value="<?php echo $allGroup[$i]->id; ?>" selected><?php echo $allGroup[$i]->group_name; ?></option>
                                <?php }else{ ?>
                                  <option value="<?php echo $allGroup[$i]->id; ?>" ><?php echo $allGroup[$i]->group_name; ?></option>
                                <?php } ?>
                              <?php }else{ ?>
                                <option value="<?php echo $allGroup[$i]->id; ?>" ><?php echo $allGroup[$i]->group_name; ?></option>
                      <?php   }
                            } ?> 
                    </select>
                  </div>
                </div>
                <?php if($this->session->userdata('level_user')==1){ ?>
                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Company</label>
                  <div class="col-sm-10">
                    <select name="company_id" class="form-control">
                      <option value="">All</option>
                      <?php 
                      $selectedCompany = "";
                      foreach ($company as $key => $value) {   
                        if ($statEdit==true) {
                          if ($dataEdit[0]->company_id==$value->company_id) {
                            $selectedCompany = "selected";
                          }
                        }
                      ?>
                        <option value="<?php echo $value->company_id ?>" <?php echo $selectedCompany; ?>> <?php echo $value->name; ?>  </option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <?php } ?>
                <?php if($this->session->userdata('level_user')==2){ ?>
                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Merchant</label>
                  <div class="col-sm-10">
                    <select name="merchant_id" class="form-control">
                      <option value="">All</option>
                      <?php 
                      $selectedCompany = "";
                      foreach ($merchant as $key => $value) {   
                        if ($statEdit==true) {
                          if ($dataEdit[0]->merchant_id==$value->id) {
                            $selectedCompany = "selected";
                          }
                        }
                      ?>
                        <option value="<?php echo $value->id ?>" <?php echo $selectedCompany; ?>> <?php echo $value->name; ?>  </option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <?php } ?>



              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a class="btn btn-danger" href="javascript: history.go(-1)">kembali</a>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>

        <?php if($statEdit==true){ ?>
        <div class="col-md-7">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Privilegde</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if ($statEdit==true) { ?>
              <form role="form" class="form-horizontal" method="POST" action="<?=base_url()?>settings/userManagement/save.editPrivi/<?php echo $requestMenu; ?>" enctype="multipart/form-data">
            <?php }else{ ?>
              <form role="form" class="form-horizontal" method="POST" action="<?=base_url()?>settings/userManagement/save.editPrivi/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php } ?>

            <?php $readonly = ""; 
                  $disabled = "";
            if ($statEdit==true) { $readonly = "readonly"; $disabled="disabled"; ?>
              <input name='idUser' type="hidden" value="<?php echo $idParam ?>">
            <?php  } ?>

              <div class="box-body">

                <?php for ($i=0; $i <count($menu) ; $i++) {?>
                  <label><?php echo $menu[$i]->menu_name; ?></label>
                  <div class="well">
                    <?php for ($j=0; $j <count($subMenu) ; $j++) { 
                      if($menu[$i]->id == $subMenu[$j]->id_menu){ ?>
                        <b><?php echo $subMenu[$j]->submenu_name; ?></b>
                        <p style="padding: 5px;">
                          <?php 
                            $cek1 = "";
                            $cek2 = "";
                            $cek3 = "";
                            for ($k=0; $k < count($groupPrivi) ; $k++) { 
                              if ($groupPrivi[$k]->id_menu == $menu[$i]->id && $groupPrivi[$k]->id_submenu == $subMenu[$j]->id && $groupPrivi[$k]->access_grant == 1 ){
                                $cek1 = "checked";
                              }elseif ($groupPrivi[$k]->id_menu == $menu[$i]->id && $groupPrivi[$k]->id_submenu == $subMenu[$j]->id && $groupPrivi[$k]->access_grant == 2 ) {                                     
                                $cek2 = "checked";
                              }elseif ($groupPrivi[$k]->id_menu == $menu[$i]->id && $groupPrivi[$k]->id_submenu == $subMenu[$j]->id && $groupPrivi[$k]->access_grant == 4) {
                                $cek3 = "checked";
                              }
                            } ?>
                          <input type="checkbox" name="priviledge[<?php echo $subMenu[$j]->id_menu ?>][<?php echo $subMenu[$j]->id ?>][R]"  value="1"  class="flat-red" <?php echo $cek1 ?> /> Read
                          <input type="checkbox" name="priviledge[<?php echo $subMenu[$j]->id_menu ?>][<?php echo $subMenu[$j]->id ?>][W]"  value="2" class="flat"  <?php echo $cek2 ?> /> Write
                          <input type="checkbox" name="priviledge[<?php echo $subMenu[$j]->id_menu ?>][<?php echo $subMenu[$j]->id ?>][X]"  value="4" class="flat"  <?php echo $cek3 ?> /> Delete
                        </p>
                    <?php     
                          } 
                        } ?>
                  </div>
                <?php } ?>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a class="btn btn-danger" href="javascript: history.go(-1)">kembali</a>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <?php } ?>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->