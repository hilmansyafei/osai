<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $title; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Dashboard</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <!-- left column -->
        <div class="col-md-5">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Submenu</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php if ($statEdit==true) { ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>settings/menuManagement/save.editSubmenu/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php }else{ ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>settings/menuManagement/save.newSubmenu/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php } ?>

            <?php $readonly = ""; 
                  $disabled = "";
            if ($statEdit==true) { $readonly = "readonly"; $disabled="disabled"; ?>
              <input name='idParam' type="hidden" value="<?php echo $idParam; ?>">
            <?php  } ?>
              <input name='idMenu' type="hidden" value="<?php echo $idMenu; ?>">

              <div class="box-body">

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-4 control-label">Submenu Name</label>
                  <div class="col-sm-8">
                          <input type="text" name="submenu_name" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $submenuName; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-4 control-label">Function Name</label>
                  <div class="col-sm-8">
                          <input type="text" name="function_name" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $functionName; }else{ echo $menuName; } ?>" <?php if ($statEdit==true) { echo 'readonly'; }?> >
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a class="btn btn-danger" href="javascript: history.go(-1)">kembali</a>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>

        
        <!--/.col (left) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->