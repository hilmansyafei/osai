<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if (!$this->session->userdata('authLogin')) {
            return redirect('/login');
        }
    }

    public function menu($idMenu=''){
        $getAll = explode('.',$idMenu);
        if (isset($getAll[0])) {
            $menu = $getAll[0];
            $subMenu = $getAll[1];

            $getContent = $this->datahandler->selectData('*','sub_menu','id='.$subMenu.' and id_menu='.$menu);
            if (empty($getContent)) {
                return $this->hlmhelper->throwError();
            }
            $funcName = $getContent[0]->route_submenu;
            try {
                return $this->$funcName('get',$menu.'.'.$subMenu);
            } catch (\Exception $exception) {
                return $this->hlmhelper->throwError();
            }
        }else{
           return $this->hlmhelper->throwError();
        }    
    }

    public function memberList($type='get',$requestMenu='',$data='')
        {
            $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',"sb.content = 'member/memberList' AND id_user =".$this->session->userdata('id'));
            $priviledge = $getPriviledge[0]->access_grant;
            $priviledge = explode(',',$priviledge);
            if (empty($priviledge[0])) {
                return $this->hlmhelper->throwError();
            }
            $nextProcess = false;
            $data = array('requestMenu'=>$requestMenu);
            $subMeniId = explode('.', $requestMenu);
            $getSubmenuName = $this->datahandler->selectData('submenu_name','sub_menu','id='.$subMeniId[1]);
            $data['title'] = $getSubmenuName[0]->submenu_name;
            if (strripos($type,'.')!=false) {
                $getData = explode('.', $type);
                $mode = $getData[0];
                $idParam = $getData[1];
                $nextProcess = true;
                $data['idParam'] = $idParam;

                if ($mode=='save') {
                    $subMode = $idParam;
                }
            }else{
                $mode = $type;
                $nextProcess = true;
            }

            if ($nextProcess) {
                switch ($mode) {
                    case 'get':
                        return $this->hlmhelper->genView('member/memberList',$data);
                        break;
                    case 'history':
                        return $this->hlmhelper->genView('member/history',$data);
                        break;
                    case 'invoiceHistory':
                        return $this->hlmhelper->genView('member/invoiceHistory',$data);
                        break;
                    case 'save':
                        switch ($subMode) {
                            case 'new':
                                //for new data insert
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, $priviledge)) {
                                    //edit access
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'delete':
                        if (in_array(4, $priviledge)) {
                            //delete access
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    case 'ajax':
                        $columns = array( 
                        'u.id',
                        'u.email',
                        'u.name',
                        'u.address',
                        'u.phone_number',
                        'u.birthdate',
                        'u.status',
                        'c.name as company_name',
                        'u.point'
                        );

                        if ($idParam==1) {
                            $custom_where = " (user_group = ".$idParam." or user_group = 7) ";
                        }else{
                            $custom_where = " user_group = ".$idParam;
                        }
                        
                        $nama_tbl = "users as u 
                                        left join company as c
                                            on u.company_id = c.company_id";

                        if ($this->session->userdata('level_user')==2) {
                            $custom_where =" and u.company_id =".$this->session->userdata('company_id')." and u.level_user in (2,3) ";

                        }elseif($this->session->userdata('level_user')==3){
                            $custom_where =" and u.company_id =".$this->session->userdata('company_id')." and u.level_user = 3 ";
                        }
                        
                        $hide_column = "";
                        $custom_order = "";
                        $custom_if =array();
                        $custom_group = "";

                        $method_link['History'] = "member/memberList/history";
                        $icon_img['History'] = "fa fa-history";

                        $method_link['Invoice History'] = "member/memberList/invoiceHistory";
                        $icon_img['Invoice History'] = "fa fa-money_success";

                        // $method_link['Detail'] = "member/memberList/detail";
                        // $icon_img['Detail'] = "fa fa-eye";

                        $status = array('0'=>"Inactive",
                                        '1'=>"Active");
                        $custom_field = array('status'=>$status);
                        $custom_thick = "";

                        $hide_column="u.id";
                        $tbl_id = "u.id";

                        $custom_link = array('u.name'=>'/userManagement/update');

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;
                    case 'ajaxHistory':
                        $columns = array( 
                        'hp.id',
                        'hp.type',
                        'u.name',
                        'hp.invoice_number',
                        'hp.point'
                        );
                        $custom_where = " customer_id = $idParam ";
                        $nama_tbl = "point_history as hp
                                        left join users as u 
                                            on hp.sent_by = u.id";
                        
                        $hide_column = "";
                        $custom_order = "";
                        $custom_if =array();
                        $custom_group = "";

                        $method_link['History'] = "member/memberList/history";
                        $icon_img['History'] = "fa fa-history";

                        $method_link['Detail'] = "member/memberList/detail";
                        $icon_img['Detail'] = "fa fa-eye";

                        $type = array('1'=>"Bonus",
                                      '3'=>"Voucher Point",
                                      '4'=>"Payment Point",
                                      '5'=>"Received Point",
                                      '6'=>"Sent Point",);
                        $custom_field = array('type'=>$type);
                        $custom_thick = "";

                        $hide_column="hp.id";
                        $tbl_id = "hp.id";

                        $custom_link = array();

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;
                    case 'ajaxInvoiceHistory':
                        $columns = array( 
                            'i.id',
                            'i.order_id',
                            'i.invoice_number',
                            'c.name as customer',
                            'm.name as merchant',
                            'i.total',
                            // 'if(i.payment_type=3,concat(i.total_price_point," (Point)"),concat("Rp ",FORMAT(i.total_price,0))) as price',
                            'format(total_price,0) as total_price',
                            'total_price_point',
                            'u.name',
                            'i.order_date',
                            'py.payment_type',
                            'if(i.payment_type=3 and i.total_price=0,"By Point",
                            if(py.transaction_status="capture","<small class=\'label bg-green\'>capture</small>",if(py.transaction_status="settlement","<small class=\'label bg-blue\'>settlement</small>",
                            if(py.transaction_status="cancel","<small class=\'label bg-red\'>cancel</small>",if(py.transaction_status="pending","<small class=\'label bg-yellow\'>pending</small>",
                            if(py.transaction_status="expire","<small class=\'label bg-black\'>expire</small>",
                            if(py.transaction_status="","<small class=\'label bg-yellow\'>pending</small>","<small class=\'label bg-yellow\'>deny</small>"))))))) as transaction_status',
                            'if(i.payment_type=3 and i.total_price=0,"By Point",if(py.fraud_status="accept","<small class=\'label bg-green\'>accept</small>",if(py.fraud_status="challenge","<small class=\'label bg-yellow\'>challenge</small>",if(py.fraud_status="" and py.transaction_status!="cancel" and py.transaction_status!="expire","<small class=\'label bg-yellow\'>pending</small>",
                            if(py.fraud_status="" and (py.transaction_status="cancel" or py.transaction_status="expire"),"<small class=\'label bg-green\'>accept</small>","<small class=\'label bg-red\'>deny</small>"))))) as fraud_status'

                        );

                        $nama_tbl = " invoice as i 
                                        join merchant as m 
                                            on i.merchant_id = m.id 
                                        join company as c 
                                            on c.company_id = i.company_id
                                        join users as u 
                                            on u.id = i.customer_id
                                        left join payment_transaction as py
                                            on i.order_id = py.order_id";
                        $custom_where = "1=1 and i.customer_id = ".$idParam;

                        $hide_column = "";
                        $custom_order = " id desc";
                        $custom_if =array();
                        $custom_group = "";
                        $method_link = array();
                        $icon_img = array();

                        $custom_field = array();
                        $custom_thick = "";

                        $hide_column="i.id";
                        $tbl_id = "i.id";

                        $custom_link = array('order_id'=>'finance/invoiceHistory/detailInvoice');

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;  
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return $this->hlmhelper->throwError();
            }
            
        }

        //[new function append]
}