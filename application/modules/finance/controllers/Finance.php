<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Finance extends CI_Controller
{
    public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if (!$this->session->userdata('authLogin')) {
            return redirect('/login');
        }
    }

    public function menu($idMenu=''){
        $getAll = explode('.',$idMenu);
        if (isset($getAll[0])) {
            $menu = $getAll[0];
            $subMenu = $getAll[1];

            $getContent = $this->datahandler->selectData('*','sub_menu','id='.$subMenu.' and id_menu='.$menu);
            if (empty($getContent)) {
                return $this->hlmhelper->throwError();
            }
            $funcName = $getContent[0]->route_submenu;
            try {
                return $this->$funcName('get',$menu.'.'.$subMenu);
            } catch (\Exception $exception) {
                return $this->hlmhelper->throwError();
            }
        }else{
           return $this->hlmhelper->throwError();
        }    
    }

    public function getImage($name){
        $link = "http://scoproject.com/api_osai/public/qr/$name";
        return readfile($link);
    }

    public function getStatus($requestMenu){
        
    }

    public function invoiceHistory($type='get',$requestMenu='',$data='')
        {
            $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',"sb.content = 'finance/invoiceHistory' AND id_user =".$this->session->userdata('id'));
            $priviledge = $getPriviledge[0]->access_grant;
            $priviledge = explode(',',$priviledge);
            if (empty($priviledge[0])) {
                return $this->hlmhelper->throwError();
            }
            $nextProcess = false;
            $data = array('requestMenu'=>$requestMenu);
            $subMeniId = explode('.', $requestMenu);
            $getSubmenuName = $this->datahandler->selectData('submenu_name','sub_menu','id='.$subMeniId[1]);
            $data['title'] = $getSubmenuName[0]->submenu_name;
            if (strripos($type,'.')!=false) {
                $getData = explode('.', $type);
                $mode = $getData[0];
                $idParam = $getData[1];
                $nextProcess = true;
                $data['idParam'] = $idParam;

                if ($mode=='save') {
                    $subMode = $idParam;
                }
            }else{
                $mode = $type;
                $nextProcess = true;
            }

            if ($nextProcess) {
                switch ($mode) {
                    case 'get':
                        return $this->hlmhelper->genView('finance/invoiceHistory',$data);
                        break;
                    case 'detailInvoice':
                        return $this->hlmhelper->genView('finance/detailInvoice',$data);
                        break;
                    case 'save':
                        switch ($subMode) {
                            case 'new':
                                //for new data insert
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, $priviledge)) {
                                    //edit access
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'ajax':
                        $columns = array( 
                            'i.id',
                            'i.order_id',
                            'i.invoice_number',
                            'c.name as customer',
                            'm.name as merchant',
                            'i.total',
                            // 'if(i.payment_type=3,concat(i.total_price_point," (Point)"),concat("Rp ",FORMAT(i.total_price,0))) as price',
                            'format(total_price,0) as total_price',
                            'total_price_point',
                            'u.name',
                            'i.order_date',
                            'py.payment_type',
                            'if(i.payment_type=3 and i.total_price=0,"By Point",
                            if(py.transaction_status="capture","<small class=\'label bg-green\'>capture</small>",if(py.transaction_status="settlement","<small class=\'label bg-blue\'>settlement</small>",
                            if(py.transaction_status="cancel","<small class=\'label bg-red\'>cancel</small>",if(py.transaction_status="pending","<small class=\'label bg-yellow\'>pending</small>",
                            if(py.transaction_status="expire","<small class=\'label bg-black\'>expire</small>",
                            if(py.transaction_status="","<small class=\'label bg-yellow\'>pending</small>","<small class=\'label bg-yellow\'>deny</small>"))))))) as transaction_status',
                            'if(i.payment_type=3 and i.total_price=0,"By Point",if(py.fraud_status="accept","<small class=\'label bg-green\'>accept</small>",if(py.fraud_status="challenge","<small class=\'label bg-yellow\'>challenge</small>",if(py.fraud_status="" and py.transaction_status!="cancel" and py.transaction_status!="expire","<small class=\'label bg-yellow\'>pending</small>",
                            if(py.fraud_status="" and (py.transaction_status="cancel" or py.transaction_status="expire"),"<small class=\'label bg-green\'>accept</small>","<small class=\'label bg-red\'>deny</small>"))))) as fraud_status'

                        );

                        $nama_tbl = " invoice as i 
                                        join merchant as m 
                                            on i.merchant_id = m.id 
                                        join company as c 
                                            on c.company_id = i.company_id
                                        join users as u 
                                            on u.id = i.customer_id
                                        left join payment_transaction as py
                                            on i.order_id = py.order_id";
                        $custom_where = "1=1";

                        if (!empty($this->session->userdata('merchant_id'))) {
                            $custom_where .=" and m.id =".$this->session->userdata('merchant_id');
                        }

                        if (!empty($this->session->userdata('company_id'))) {
                            $custom_where .=" and c.company_id =".$this->session->userdata('company_id');
                        }

                        $hide_column = "";
                        $custom_order = " id desc";
                        $custom_if =array();
                        $custom_group = "";
                        $method_link = array();
                        $icon_img = array();

                        if (in_array(4, $priviledge)) {
                            $method_link['Get Status'] = "finance/invoiceHistory/getStatus";
                            $icon_img['Get Status'] = "fa fa-send";
                        }

                        $custom_field = array();
                        $custom_thick = "";

                        $hide_column="i.id";
                        $tbl_id = "i.id";

                        $custom_link = array('order_id'=>'finance/invoiceHistory/detailInvoice');

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;  
                    case 'ajaxDetailInvoice':
                        $link = base_url()."finance/getImage/";
                        $columns = array( 
                            'ih.id',
                            'ih.order_id',
                            'v.voucher_id',
                            'v.name',
                            'if(i.payment_type=3,concat(ih.point," (Point)"),concat("Rp ",FORMAT(ih.price,0))) as price',
                            'ih.bonus_point',
                            'tr.content',
                            'if(qr_code<>"",concat("<img height=\'auto\' width=\'110px\'  src=\''.$link.'",ih.qr_code,".png\'>"),"") as qr',
                            'ih.qr_code',
                            'vc.name as category',
                            'ih.expired_date',
                            'if(v.expired_date<now() and used =0 ,"Expired",if(used=1,"Done","Waiting")) as redeem',
                            'tr2.content'
                        );
                        $nama_tbl = " invoice_history as ih
                                                join voucher as v
                                                    on ih.voucher_id = v.voucher_id
                                                join voucher_category as vc 
                                                    on ih.voucher_category = vc.id
                                                join tbl_ref as tr
                                                    on ih.payment_type = tr.id_ref and group_ref =1
                                                left join tbl_ref as tr2
                                                    on ih.withdraw_status = tr2.id_ref 
                                                        and tr2.group_ref =3
                                                join invoice as i 
                                                    on i.invoice_number = ih.invoice_number
                                                    and i.id = ".$idParam;

                        $custom_where = "";
                        $hide_column = "";
                        $custom_order = "";
                        $custom_if =array();
                        $custom_group = "";
                        $method_link = array();
                        $icon_img = array();

                        $custom_field = array();
                        $custom_thick = "";

                        $hide_column="ih.id";
                        $tbl_id = "ih.id";

                        $custom_link = array();

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;  
                    case 'getStatus':
                        # guzzle client define
                        $client     = new GuzzleHttp\Client();
                        try {
                            $data = $this->datahandler->selectData('order_id','invoice','id='.$idParam);

                            // Create a client with a base URI
                            $response = $client->request('POST', 'http://scoproject.com/api_osai/public/index.php/get_status', [ 'form_params' 
                                                        => [ 'order_id' => $data[0]->order_id ] 
                                                  ]);
                            $statCode = $response->getStatusCode(); // 200
                            if ($statCode==200) {
                                $this->session->set_flashdata('success','Payment status has been updated');
                            }else{
                                $this->session->set_flashdata('warning','Payment status failed update, please try again');
                            }
                            
                            return redirect('finance/menu/'.$requestMenu);
                        } catch (GuzzleHttp\Exception\BadResponseException $e) {
                            $this->session->set_flashdata('warning','Payment status failed update');
                            return redirect('finance/menu/'.$requestMenu);

                        }
                        break;
                    case 'delete':
                        if (in_array(4, $priviledge)) {
                            //delete access
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return $this->hlmhelper->throwError();
            }
            
        }

        public function withdraw($type='get',$requestMenu='',$data='')
        {
            $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',"sb.content = 'finance/withdraw' AND id_user =".$this->session->userdata('id'));
            $priviledge = $getPriviledge[0]->access_grant;
            $priviledge = explode(',',$priviledge);
            if (empty($priviledge[0])) {
                return $this->hlmhelper->throwError();
            }
            $nextProcess = false;
            $data = array('requestMenu'=>$requestMenu);
            $subMeniId = explode('.', $requestMenu);
            $getSubmenuName = $this->datahandler->selectData('submenu_name','sub_menu','id='.$subMeniId[1]);
            $data['title'] = $getSubmenuName[0]->submenu_name;
            if (strripos($type,'.')!=false) {
                $getData = explode('.', $type);
                $mode = $getData[0];
                $idParam = $getData[1];
                $nextProcess = true;
                $data['idParam'] = $idParam;

                if ($mode=='save') {
                    $subMode = $idParam;
                }
            }else{
                $mode = $type;
                $nextProcess = true;
            }

            if ($nextProcess) {
                switch ($mode) {
                    case 'get':
                        return $this->hlmhelper->genView('finance/withdraw',$data);
                        break;
                    case 'save':
                        switch ($subMode) {
                            case 'new':
                                //for new data insert
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, $priviledge)) {
                                    //edit access
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'approve':
                        if (in_array(4, $priviledge)) {
                            $updateData = array('status' => 2,
                                                'process_date'=>date("Y-m-d H:i:s"),
                                                'processed_by'=>$this->session->userdata('id'));
                            $getData = $this->datahandler->selectData('id_invoice_history','withdraw_history','id='.$idParam);

                            $updateInvoiceHistory = array('withdraw_status' =>2);
                            $this->datahandler->updateData('invoice_history',$updateInvoiceHistory,'id='.$getData[0]->id_invoice_history);

                            $this->datahandler->updateData('withdraw_history',$updateData,'id='.$idParam);
                            $this->session->set_flashdata('success','Withdraw Approved');
                            return redirect('finance/menu/'.$requestMenu);
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    case 'reject':
                        if (in_array(4, $priviledge)) {
                            $updateData = array('status' => 3,
                                                'process_date'=>date("Y-m-d H:i:s"),
                                                'processed_by'=>$this->session->userdata('id'));
                            $this->datahandler->updateData('withdraw_history',$updateData,'id='.$idParam);

                            $getData = $this->datahandler->selectData('id_invoice_history','withdraw_history','id='.$idParam);

                            $updateInvoiceHistory = array('withdraw_status' =>3);
                            $this->datahandler->updateData('invoice_history',$updateInvoiceHistory,'id='.$getData[0]->id_invoice_history);
                            $this->session->set_flashdata('warning','Withdraw Rejected');
                            return redirect('finance/menu/'.$requestMenu);
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    case 'ajax':
                        $link = base_url()."finance/getImage/";
                        $columns = array( 
                            'wh.id',
                            'c.name as company',
                            'm.name as merchant',
                            'i.invoice_number',
                            'i.order_id',
                            'concat("Rp ",FORMAT(i.total_price,0)) as price',
                            'tr.content as payment_type',
                            'tr2.content',
                            'wh.process_date',
                            'u.name'
                            );

                        $nama_tbl = " withdraw_history as wh
                                        join invoice_history as ih
                                            on ih.id = wh.id_invoice_history
                                        join invoice as i
                                            on ih.order_id = i.order_id
                                        join tbl_ref as tr
                                            on i.payment_type = tr.id_ref and tr.group_ref =1
                                        join tbl_ref as tr2
                                            on wh.status = tr2.id_ref and tr2.group_ref =3
                                        join merchant as m 
                                            on i.merchant_id = m.id 
                                        join company as c 
                                            on c.company_id = i.company_id
                                        left join users as u
                                            on wh.processed_by = u.id";

                        $custom_where = "1=1 ";

                        if (!empty($this->session->userdata('merchant_id'))) {
                            $custom_where .=" and m.id =".$this->session->userdata('merchant_id');
                        }

                        if (!empty($this->session->userdata('company_id'))) {
                            $custom_where .=" and c.company_id =".$this->session->userdata('company_id');
                        }

                        $hide_column = "";
                        $custom_order = "";
                        $custom_if =array();
                        $custom_group = "";
                        $method_link = array();
                        $icon_img = array();

                        if (in_array(4, $priviledge)) {
                            $method_link['Approve'] = "finance/withdraw/approve";
                            $icon_img['Approve'] = "fa fa-check";

                            $method_link['Reject'] = "finance/withdraw/reject";
                            $icon_img['Reject'] = "fa fa-close";
                        }

                        $custom_field = array();
                        $custom_thick = "";

                        $hide_column="wh.id";
                        $tbl_id = "wh.id";

                        $custom_link = array();

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;  
                    case 'delete':
                        if (in_array(4, $priviledge)) {
                            //delete access
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return $this->hlmhelper->throwError();
            }
            
        }

        public function kursPoint($type='get',$requestMenu='',$data='')
        {
            $getPriviledge = $this->datahandler->selectData('GROUP_CONCAT(up.`access_grant`) as access_grant','sub_menu AS sb JOIN user_priviledge AS up ON up.`id_submenu` = sb.id AND up.`id_menu` = sb.`id_menu`',"sb.content = 'finance/kursPoint' AND id_user =".$this->session->userdata('id'));
            $priviledge = $getPriviledge[0]->access_grant;
            $priviledge = explode(',',$priviledge);
            if (empty($priviledge[0])) {
                return $this->hlmhelper->throwError();
            }
            $nextProcess = false;
            $data = array('requestMenu'=>$requestMenu);
            $subMeniId = explode('.', $requestMenu);
            $getSubmenuName = $this->datahandler->selectData('submenu_name','sub_menu','id='.$subMeniId[1]);
            $data['title'] = $getSubmenuName[0]->submenu_name;
            if (strripos($type,'.')!=false) {
                $getData = explode('.', $type);
                $mode = $getData[0];
                $idParam = $getData[1];
                $nextProcess = true;
                $data['idParam'] = $idParam;

                if ($mode=='save') {
                    $subMode = $idParam;
                }
            }else{
                $mode = $type;
                $nextProcess = true;
            }

            if ($nextProcess) {
                switch ($mode) {
                    case 'get':
                        return $this->hlmhelper->genView('finance/kursPoint',$data);
                        break;
                    case 'viewEdit':
                        $data['statEdit'] = true;
                        $data['dataEdit'] = $this->datahandler->selectData('*','kurs_point','id = '.$idParam);
                        return $this->hlmhelper->genView('finance/viewEditKurs',$data);
                        break;
                    case 'save':
                        switch ($subMode) {
                            case 'new':
                                //for new data insert
                                break;
                            case 'edit':
                                //for data update
                                if (in_array(2, $priviledge)) {
                                    //edit access
                                    $getPos = $this->input->post(NULL);
                                    $dataUpdate = array(
                                        'money' => $getPos['money'],
                                        'point' => $getPos['point']
                                    );
                                    $this->datahandler->updateData('kurs_point',$dataUpdate,'id = '.$getPos['idParam']);
                                    $this->session->set_flashdata('success','Kurs changed successfully');
                                    return redirect('finance/menu/'.$requestMenu);
                                }else{
                                    return $this->hlmhelper->throwForbidden();
                                }
                                break;
                            default:
                                //default action
                                break;
                        }
                        break;
                    case 'ajax':
                        $columns = array( 
                            'id',
                            'point',
                            'money'    
                        );

                        $nama_tbl = "kurs_point";

                        $custom_where = "1=1 ";

                        $hide_column = "";
                        $custom_order = "";
                        $custom_if =array();
                        $custom_group = "";

                        if (in_array(4, $priviledge)) {
                            $method_link['Edit'] = "finance/kursPoint/viewEdit";
                            $icon_img['Edit'] = "fa fa-pencil";
                        }

                        $custom_field = array();
                        $custom_thick = "";

                        $hide_column="id";
                        $tbl_id = "id";

                        $custom_link = array();

                        $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                        break;  
                    case 'delete':
                        if (in_array(4, $priviledge)) {
                            //delete access
                        }else{
                            return $this->hlmhelper->throwForbidden();
                        }
                        break;
                    default:
                        //default action
                        break;
                }
            }else{
                //throw error
                return $this->hlmhelper->throwError();
            }
            
        }

        //[new function append]
}