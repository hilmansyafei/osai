<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	/**
	 * Copyright
	 * Hilman Syafei
	 * Februari 2017 settings
	 */
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        
        if ($this->session->userdata('authLogin')) {
            return redirect('/dashboard');
        }
    }

    public function index(){
    	if ($this->session->userdata('authLogin')) {
			$url='/dashboard';
			redirect($url, 'refresh');
		}else{
			$this->load->view('login');
		}
    	
    }

    public function authLogin()
    {	
	    $userdata = $this->input->post(NULL);
        $checkCredentials = file_get_contents(BASE_API_URL."auth_app/".$userdata['username']."/".$userdata['password']);
        if (!$checkCredentials) {
            $this->session->set_flashdata('warning','Invalid username and password');
			return redirect("/login", 'refresh');
        }else{
            $getAll = $this->db->where('email',$userdata['username'])
                           ->where('level_user !=',4)
                           ->get('users')
                           ->result();
            $getUsername = $getAll[0]->email;
            if (count($getAll)==0) {
                $getAll = $this->db->where('username',$userdata['username'])
                           ->where('level_user !=',4)
                           ->get('users')
                           ->result();
            $getUsername = $getAll[0]->username;
            }
        }

        if (count($getAll)==0) {
            $this->session->set_flashdata('warning','Invalid username and password');
            return redirect("/login", 'refresh');
        }

        if ($getAll[0]->status==0) {
            $this->session->set_flashdata('warning','User inactive');
			return redirect("/login", 'refresh');
        }
        
        $getName = $getAll[0]->name;

        $getDataSesion = $this->db->query("Select * from ci_sessions where data like '%$getUsername%'");
        $cek_user = $getDataSesion->result();

    	if (count($cek_user)>0) {
            $last_activity = $cek_user[0]->timestamp;
            if (strtotime(date('Y-m-d H:i:s',strtotime("-120 minutes",strtotime(date('Y-m-d H:i:s'))))) < $last_activity) {
                $this->session->set_flashdata('warning','Account is using');
                return redirect("/login","refresh");
            }else{
            	//$this->db->where('id',$cek_user[0]->id)->delete("ci_sessions");
                $this->db->query('delete from ci_sessions where data like "%'.$getUsername.'%"');
            }	
    	}

	    if (count($getAll)) {
            $getUserGroup = $this->db->where('id',$getAll[0]->user_group)->get('user_group')->result();
            $this->session->set_userdata('id',$getAll[0]->id);
            $this->session->set_userdata('username',$getUsername);
            $this->session->set_userdata('company_id',$getAll[0]->company_id);
            $this->session->set_userdata('merchant_id',$getAll[0]->merchant_id);
            $this->session->set_userdata('level_user',$getAll[0]->level_user);
            $this->session->set_userdata('name',$getName);
            $this->session->set_userdata('accountType',$getUserGroup[0]->group_name);
            $this->session->set_userdata('id_group',$getUserGroup[0]->id);
            $this->session->set_userdata('authLogin',md5($getUsername));
	        return redirect('/dashboard');
	    }
    	
    }

    
}