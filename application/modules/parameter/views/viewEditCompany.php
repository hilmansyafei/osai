<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small><?php echo $title; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Dashboard</a></li>
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <!-- left column -->
        <div class="col-md-10">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Detail</h3>
            </div>
            <?php if ($statEdit==true) { ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>parameter/company/save.edit/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php }else{ ?>
              <form role="form" class="form-horizontal" action="<?=base_url()?>parameter/company/save.new/<?php echo $requestMenu; ?>" method="POST" enctype="multipart/form-data">
            <?php } ?>

            <?php $readonly = ""; 
                  $disabled = "";
            if ($statEdit==true) { $readonly = "readonly"; $disabled="disabled"; ?>
              <input name='idParam' type="hidden" value="<?php echo $idParam; ?>">
            <?php  } ?>

              <div class="box-body">

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                          <input type="text" name="name" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->name; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Phone Number</label>
                  <div class="col-sm-10">
                          <input type="number" name="phone_number" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->phone_number; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                          <input type="email" name="email" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->email; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Zip Code</label>
                  <div class="col-sm-10">
                          <input type="number" name="zip_code" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->zip_code; } ?>" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Location</label>
                  <div class="col-sm-10">
                          <input type="text" name="location" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->location; } ?>" required>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <a class="btn btn-danger" href="javascript: history.go(-1)">kembali</a>
              </div>
            </form>
         
          </div>
          <!-- /.box -->
        </div>

        <!--/.col (left) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
  $(function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
    $( "#datepicker1 " ).datepicker({
      changeMonth: true,
      changeYear: true
    });
    $( "#datepicker2" ).datepicker({
      changeMonth: true,
      changeYear: true
    });

    $( "#datepicker3" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  });
  </script>