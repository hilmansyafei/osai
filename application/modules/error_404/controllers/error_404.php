<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class error_404 extends CI_Controller {
	/**
	 * Copyright
	 * Hilman Syafei
	 * Februari 2017settings
	 */
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function index(){
    	echo "Error 404 Not Found";
    }
}