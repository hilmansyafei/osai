<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>User Profile</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profil</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
         <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <?php if ($this->session->flashdata('success')) { ?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                <?php echo $this->session->flashdata('success'); ?>
              </div>
              <?php } ?>

              <?php if ($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Error!</h4>
                <?php echo $this->session->flashdata('error'); ?>
              </div>
              <?php } ?>

              <?php if ($this->session->flashdata('warning')) { ?>
              <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-warning"></i> Warning!</h4>
                <?php echo $this->session->flashdata('warning'); ?>
              </div>
              <?php } ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Data</h3>
            </div>
            
            <!-- /.box-header -->
            <!-- form start -->
            <?php if ($statEdit==true) { ?>
              <form role="form" action="<?=base_url()?>dashboard/updateProfile" method="POST" enctype="multipart/form-data">
            <?php } ?>

            <?php $readonly = ""; 
                  $disabled = "";
            if ($statEdit==true) { $readonly = "readonly"; $disabled="disabled"; ?>
              <input name='idUser' type="hidden" value="<?php echo $idUser ?>">
            <?php  } ?>

              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Username</label>
                   <input type="text" name="username" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->email; } ?>" <?=$readonly?>>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Full Name</label>
                          <input type="text" name="name" class="form-control" 
                          value="<?php if ($statEdit==true) { echo $dataEdit[0]->name; } ?>">
                     
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <input type="password" name="password" class="form-control">
                </div>

                <div class="form-group">
                  <label for="exampleInputFile">Profile Foto</label>
                  <input type="file" id="exampleInputFile" name="imgSrc">
                  <!-- <p class="help-block">Profile Foto</p> -->
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->