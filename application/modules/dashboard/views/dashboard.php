<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<script>
  function topVoucher(dataSend){
    $('#topVoucher').DataTable({
      "scrollX": true,
      "scrollY": 'auto',
      "paging": true,
      "destroy":true,
      "lengthChange": true,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
         url :'<?php echo base_url() ?>dashboard/processData/ajaxTopVoucher/0/1.1', // json datasource
         type: "GET", // method  , by default get
         data:  dataSend 
       }
    });
  }
  function topUser(dataSend){
    $('#topUsers').DataTable({
      "scrollX": true,
      "scrollY": 'auto',
      "paging": true,
      "destroy":true,
      "lengthChange": true,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
         url :'<?php echo base_url() ?>dashboard/processData/ajaxTopUser/0/1.1', // json 
         type: "GET",  // method  , by default get
         data:  dataSend 
       }
    });
  }
  function topPoint(dataSend){
    $('#pointTransaction').DataTable({
      "scrollX": true,
      "scrollY": 'auto',
      "paging": true,
      "destroy":true,
      "lengthChange": true,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
         url :'<?php echo base_url() ?>dashboard/processData/ajaxTopTransaction/0/1.1', // json 
         type: "GET",  // method  , by default get
         data: dataSend
       }
    });
  }
  $(function () {
    topVoucher();
    topUser();
    topPoint();
    
    $('.table th').addClass('bg-blue');
  });
</script>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo count($totalUser); ?></h3>
              <hr>
              <p>Total User</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-bag"></i> -->
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo count($totalVoucher); ?></h3>
              <hr>
              <p>Total Voucher</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-stats-bars"></i> -->
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo count($totalCategory); ?></h3>
              <hr>
              <p>Total Category</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-person-add"></i> -->
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo count($totalMerchant); ?></h3>
              <hr>
              <p>Total Merchant</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-pie-graph"></i> -->
            </div>
            <a href="#" class="small-box-footer"></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-md-8">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">User Statistic</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <canvas id="pieChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-md-8">
          <!-- Vocher CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Voucher Statistic</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <form role="form" class="form-horizontal" action="#">

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Merchant</label>
                  <div class="col-sm-4">
                      <select name="merchant_id" id="merchant_id" class="selectpicker" data-live-search="true">
                        <option value="">Pelease Select</option>
                        <option value="xx">All Merchant</option>
                        <?php foreach ($totalMerchant as $key => $value) {
                          echo '<option value="'.$value->id.'" data-tokens="'.$value->id.' - '.$value->name.'">'.$value->name.'</option>';
                        } ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Category</label>
                  <div class="col-sm-4">
                      <select name="category_id" id="category_id" class="selectpicker" data-live-search="true">
                        <option value="">Pelease Select</option>
                        <?php foreach ($totalCategory as $key => $value) {
                          echo '<option value="'.$value->id.'" data-tokens="'.$value->id.' - '.$value->name.'">'.$value->name.'</option>';
                        } ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Voucher</label>
                  <div class="col-sm-4">
                      <select name="voucher_id" id="voucher_id" class="selectpicker" data-live-search="true">
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Start Date</label>
                  
                  <div class="col-sm-4">
                    <div class=" input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="start_date" id="start_date" class="form-control" data-date-format='dd/mm/yyyy'
                            value="" placeholder="" required>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">End Date</label>
                  
                  <div class="col-sm-4">
                    <div class=" input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="end_date" id="end_date" class="form-control" data-date-format='dd/mm/yyyy'
                            value="" placeholder="" required>
                    </div>
                  </div>
                </div>
              </form>
              <div class="box-footer">
                <div class="col-sm-6">
                  <button id="statVoucher" class="btn btn-primary">Search</button>
                </div>
              </div>
              <canvas id="voucher" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <!-- <div class="col-md-8">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Category Voucher</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <form role="form" class="form-horizontal" action="#">
                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Category</label>
                  <div class="col-sm-4">
                      <select name="voucher_id" id="category_id" class="selectpicker" data-live-search="true">
                        <option value="">Pelease Select</option>
                        <?php foreach ($totalCategory as $key => $value) {
                          echo '<option value="'.$value->id.'" data-tokens="'.$value->name.'">'.$value->name.'</option>';
                        } ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Start Date</label>
                  
                  <div class="col-sm-4">
                    <div class=" input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="start_date" id="start_date_cat" class="form-control" data-date-format='dd/mm/yyyy'
                            value="" placeholder="" required>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">End Date</label>
                  
                  <div class="col-sm-4">
                    <div class=" input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="end_date" id="end_date_cat" class="form-control" data-date-format='dd/mm/yyyy'
                            value="" placeholder="" required>
                    </div>
                  </div>
                </div>
              </form>
              <div class="box-footer">
                <div class="col-sm-6">
                  <button id="statCategory" class="btn btn-primary">Search</button>
                </div>
              </div>
              <canvas id="categoryVoucher" style="height:250px"></canvas>
            </div>
          </div>
        </div> -->

        <div class="col-md-8">
          <!-- USER BUY VOUCHER CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">User Payment</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <form role="form" class="form-horizontal" action="#">
                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">User</label>
                  <div class="col-sm-4">
                      <select name="" id="user_id" class="selectpicker" data-live-search="true">
                        <option value="">Pelease Select</option>
                        <?php foreach ($totalUser as $key => $value) {
                          echo '<option value="'.$value->id.'" data-tokens="'.$value->email.' - '.$value->name.'">'.$value->email.' - '.$value->name.'</option>';
                        } ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">Start Date</label>
                  
                  <div class="col-sm-4">
                    <div class=" input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="start_date_user" id="start_date_user" class="form-control" data-date-format='dd/mm/yyyy'
                            value="" placeholder="" required>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">End Date</label>
                  
                  <div class="col-sm-4">
                    <div class=" input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="end_date_user" id="end_date_user" class="form-control" data-date-format='dd/mm/yyyy'
                            value="" placeholder="" required>
                    </div>
                  </div>
                </div>
              </form>
              <div class="box-footer">
                <div class="col-sm-6">
                  <button id="statUser" class="btn btn-primary">Search</button>
                </div>
              </div>
              <canvas id="userBuyVoucher" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"> <b>Top Voucher</b> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" class="form-horizontal" action="#">

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">1 Order By</label>
                  <div class="col-sm-4">
                      <select name="1_order" id="1_order">
                        <?php
                          foreach ($orderBy as $key => $value) {?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php }
                        ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">2 Order By</label>
                  <div class="col-sm-4">
                      <select name="2_order" id="2_order">
                        <?php
                          foreach ($orderBy as $key => $value) {?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php }
                        ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">3 Order By</label>
                  <div class="col-sm-4">
                      <select name="3_order" id="3_order">
                        <?php
                          foreach ($orderBy as $key => $value) {?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php }
                        ?>
                      </select>
                  </div>
                </div>
                <span>*Default order by item</span>
              </form>
              <div class="box-footer">
                <div class="col-sm-6">
                  <button id="btnTopVoucher" class="btn btn-primary">Search</button>
                </div>
              </div>
              <hr>
              <table id="topVoucher" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Merchant</th>
                  <th>Voucher ID</th>
                  <th>Voucher Name</th>
                  <th>Voucher Category</th>
                  <th>Item</th>
                  <th>Cash (Rp)</th>
                  <th>Point</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"> <b>Top Users</b> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" class="form-horizontal" action="#">

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">1 Order By</label>
                  <div class="col-sm-4">
                      <select name="u1_order" id="u1_order">
                        <?php
                          foreach ($orderBy as $key => $value) {?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php }
                        ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">2 Order By</label>
                  <div class="col-sm-4">
                      <select name="u2_order" id="u2_order">
                        <?php
                          foreach ($orderBy as $key => $value) {?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php }
                        ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">3 Order By</label>
                  <div class="col-sm-4">
                      <select name="u3_order" id="u3_order">
                        <?php
                          foreach ($orderBy as $key => $value) {?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php }
                        ?>
                      </select>
                  </div>
                </div>
                <span>*Default order by item</span>
              </form>
              <div class="box-footer">
                <div class="col-sm-6">
                  <button id="btnTopUser" class="btn btn-primary">Search</button>
                </div>
              </div>
              <hr>
              <table id="topUsers" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th>Item</th>
                  <th>Cash (Rp)</th>
                  <th>Point</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"> <b>Top Point Transaction</b> </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" class="form-horizontal" action="#">

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">1 Order By</label>
                  <div class="col-sm-4">
                      <select name="p1_order" id="p1_order">
                        <?php
                          foreach ($orderByPoint as $key => $value) {?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php }
                        ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">2 Order By</label>
                  <div class="col-sm-4">
                      <select name="p2_order" id="p2_order">
                        <?php
                          foreach ($orderByPoint as $key => $value) {?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php }
                        ?>
                      </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1" class="col-sm-2 control-label">3 Order By</label>
                  <div class="col-sm-4">
                      <select name="p3_order" id="p3_order">
                        <?php
                          foreach ($orderByPoint as $key => $value) {?>
                            <option value="<?php echo $key ?>"><?php echo $value ?></option>
                        <?php }
                        ?>
                      </select>
                  </div>
                </div>
                <span>*Default order by total</span>
              </form>
              <div class="box-footer">
                <div class="col-sm-6">
                  <button id="btnTopPoint" class="btn btn-primary">Search</button>
                </div>
              </div>
              <hr>
              <table id="pointTransaction" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Email</th>
                  <th>Name</th>
                  <th>Point In</th>
                  <th>Point Out</th>
                  <th>Total</th>
                </tr>
                </thead>
                <tbody>
               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
    var randomScalingFactor = function() {
      return Math.round(Math.random() * 1000);
    };
    //- PIE CHART - for user
    var config = {
      type: 'doughnut',
      data: {
          datasets: [{
              data: [
                  <?php echo $statisticUser[0]->female; ?>,
                  <?php echo $statisticUser[0]->male; ?>,
                  <?php echo $statisticUser[0]->unk; ?>,
              ],
              backgroundColor: [
                  '#ff6384',
                  '#0B43F0',
                  '#adbdd6'
              ],
              label: 'Dataset 1'
          }],
          labels: [
              "Female",
              "Male",
              "Unknown"
          ]
      },
      options: {
          responsive: true,
          legend: {
              position: 'top',
          },
          title: {
              display: true,
              text: 'Data Statistic User'
          },
          animation: {
              animateScale: true,
              animateRotate: true
          }
      }
    };

    //LINE for voucher
    var configVoucher = {
        type: 'line',
        data: {
            labels: ["01/01/17", "02/01/17", "03/01/17", "04/01/17", "05/01/17", "06/01/17"],
            datasets: [{
                label: "Voucher",
                backgroundColor: '#F01C0B',
                borderColor: "#F01C0B",
                data: [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                ],
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:'Statistic Voucher'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Day'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    };

    //LINE for voucher
    var configCategoryVoucher = {
        type: 'line',
        data: {
            labels: ["01/01/17", "02/01/17", "03/01/17", "04/01/17", "05/01/17", "06/01/17"],
            datasets: [{
                label: "Category",
                backgroundColor: '#7FC060',
                borderColor: "#7FC060",
                data: [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                ],
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:'Statistic Category Voucher'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Day'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    };

    // LINE for user buy voucher
    var configUserBuyVoucher = {
        type: 'line',
        data: {
            labels: ["01/01/17", "02/01/17", "03/01/17", "04/01/17", "05/01/17", "06/01/17"],
            datasets: [{
                label: "Member",
                backgroundColor: '#0E0BF0',
                borderColor: "#0E0BF0",
                data: [
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                    0
                ],
                fill: false,
            }]
        },
        options: {
            responsive: true,
            title:{
                display:true,
                text:'Statistic Voucher'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Day'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    };

    //load to view
    window.onload = function() {
      //load voucher
      var voucher = document.getElementById("voucher").getContext("2d");
      window.myLine = new Chart(voucher, configVoucher);

      //load user
      var user = document.getElementById("pieChart").getContext("2d");
      window.myDoughnut = new Chart(user, config);

      //load user buy voucher
      var userBuyVoucher = document.getElementById("userBuyVoucher").getContext("2d");
      window.myMember = new Chart(userBuyVoucher, configUserBuyVoucher);

      //load category voucher
      //var categoryVoucher = document.getElementById("categoryVoucher").getContext("2d");
      //window.myCategory = new Chart(categoryVoucher, configCategoryVoucher);

      

      // config.data.datasets.forEach(function(dataset) {
      //   dataset.data =[
      //       randomScalingFactor(),
      //       randomScalingFactor(),
      //   ];
      // });

      // window.myDoughnut.update();
    };

    $(function(){
      $('#category_id').prop('disabled', true);
      $('#voucher_id').prop('disabled', true);

      //change of merchant
      $('#merchant_id').on('change', function() {
        var merchant_id = this.value;
        if (merchant_id != "") {
          $('#category_id').prop('disabled', false);
          $('#voucher_id').html('<option value="" selected>Pelease Select</option>');
          $('select[name=category_id]').val("");
          $('#category_id').selectpicker('refresh');
          $('#voucher_id').selectpicker('refresh');
        }else{
          $('#category_id').prop('disabled', true);
          $('select[name=category_id]').val("");
          $('#voucher_id').html('<option value="" selected>Pelease Select</option>');
          $('#category_id').selectpicker('refresh');
          $('#voucher_id').selectpicker('refresh');
        }
      });

      //change of category
      $('#category_id').on('change', function() {
        var category_id = this.value;
        var merchant_id = $("#merchant_id").val();
        if (category_id == "") {
          $('#voucher_id').prop('disabled', true);
          $('#voucher_id').html('<option value="" selected>Pelease Select</option>');
          $('#voucher_id').selectpicker('refresh');
        }else{
          getVoucher(merchant_id,category_id);
        }
      });

      function getVoucher(merchant_id,category_id){
        var link = '<?php echo base_url();?>dashboard/processData/dropDownVoucher/v';
        $.get(link,
        {
          merchant_id : merchant_id,
          category_id : category_id
        },
        function(data, status){
          $('#voucher_id').html(data);
          $('#voucher_id').prop('disabled', false);
          $('#voucher_id').selectpicker('refresh');
        });
      }
      //datepicker
      $( "#start_date" ).datepicker({
        changeMonth: true,
        changeYear: true,
        autoclose: true,
        todayHighlight: true,
      });
      $( "#end_date" ).datepicker({
        changeMonth: true,
        changeYear: true,
        autoclose: true,
        todayHighlight: true,
      });
      $( "#start_date_cat" ).datepicker({
        changeMonth: true,
        changeYear: true,
        autoclose: true,
        todayHighlight: true,
      });
      $( "#end_date_cat" ).datepicker({
        changeMonth: true,
        changeYear: true,
        autoclose: true,
        todayHighlight: true,
      });
      $( "#start_date_user" ).datepicker({
        changeMonth: true,
        changeYear: true,
        autoclose: true,
        todayHighlight: true,
      });
      $( "#end_date_user" ).datepicker({
        changeMonth: true,
        changeYear: true,
        autoclose: true,
        todayHighlight: true,
      });

      //handler top voucher
      $('#btnTopVoucher').click(function(){
        var f_order = $('#1_order').val();
        var s_order = $('#2_order').val();
        var t_order = $('#3_order').val();

        var params = {
          'f_order' : f_order,
          's_order' : s_order,
          't_order' : t_order
        }; 

        topVoucher(params);
      });

      //handler top user
      $('#btnTopUser').click(function(){
        var f_order = $('#u1_order').val();
        var s_order = $('#u2_order').val();
        var t_order = $('#u3_order').val();

        var params = {
          'f_order' : f_order,
          's_order' : s_order,
          't_order' : t_order
        }; 

        topUser(params);
      });

      //handler top user
      $('#btnTopPoint').click(function(){
        var f_order = $('#p1_order').val();
        var s_order = $('#p2_order').val();
        var t_order = $('#p3_order').val();

        var params = {
          'f_order' : f_order,
          's_order' : s_order,
          't_order' : t_order
        }; 

        topPoint(params);
      });

      //handler search voucher
      $('#statVoucher').click(function(){
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();

        var merchant_id = $('#merchant_id').val();
        var category_id = $('#category_id').val();
        var voucher_id = $('#voucher_id').val();

        var type = "";
        var dataSend = "";
        if (merchant_id == "") {
          alert('Please Select Merchant');
        }else if(start_date == ""){
          alert('Please Select Start Date');
        }else if(end_date == ""){
          alert('Please Select End Date');
        }else if(merchant_id != "" && category_id == "" && voucher_id == ""){
          type = 1;
          dataSend = {
            'merchant_id' : merchant_id,
            'start_date' : start_date,
            'end_date' : end_date,
            'type' : 1
          };
        }else if(merchant_id != "" && category_id != "" && voucher_id ==""){
          type = 2;
          dataSend = {
            'merchant_id' : merchant_id,
            'category_id' : category_id,
            'start_date' : start_date,
            'end_date' : end_date,
            'type' : 2
          };
        }else if(merchant_id != "" && category_id != "" && voucher_id != ""){
          type = 3;
          dataSend = {
            'merchant_id' : merchant_id,
            'category_id' : category_id,
            'voucher_id' : voucher_id,
            'start_date' : start_date,
            'end_date' : end_date,
            'type' : 3
          };
        }

        if (type != "" && dataSend != "") {
          var link = '<?php echo base_url();?>dashboard/processData/statVoucher';
          $.get(link,dataSend,
          function(data, status){
            //obj = JSON.parse(data);
            console.log(data[1]);
            console.log(data[0]);
            configVoucher.data.datasets.forEach(function(dataset) {
              dataset.data = data[1] ;
            });
            configVoucher.data.labels = data[0];


            window.myLine.update();
          });
        }
      });

      //handler category
      $('#statCategory').click(function(){
        var start_date = $('#start_date_cat').val();
        var end_date = $('#end_date_cat').val();
        var category_id = $('#category_id').val();
        var link = '<?php echo base_url();?>dashboard/processData/statCategory';
        $.get(link,
        {
            start_date : start_date,
            end_date : end_date,
            category_id : category_id
        },
        function(data, status){
          //obj = JSON.parse(data);
          configCategoryVoucher.data.datasets.forEach(function(dataset) {
            dataset.data = data[1] ;
          });
          configCategoryVoucher.data.labels = data[0];
          window.myCategory.update();
        });
      });

      //handler user
      $('#statUser').click(function(){
        var start_date = $('#start_date_user').val();
        var end_date = $('#end_date_user').val();
        var user_id = $('#user_id').val();
        var link = '<?php echo base_url();?>dashboard/processData/statUser';
        $.get(link,
        {
            start_date : start_date,
            end_date : end_date,
            user_id : user_id
        },
        function(data, status){
          //obj = JSON.parse(data);
          configUserBuyVoucher.data.datasets.forEach(function(dataset) {
            dataset.data = data[1] ;
          });
          configUserBuyVoucher.data.labels = data[0];
          window.myMember.update();
        });
      });
    });
  </script>