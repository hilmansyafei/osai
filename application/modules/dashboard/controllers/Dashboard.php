<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	/**
	 * Copyright
	 * Hilman Syafei
	 * Februari 2017settings
	 */
	public function __construct(){
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        if (!$this->session->userdata('authLogin')) {
        	return redirect('/login');
        }
    }

    public function index(){
        $data = array();
        //echo "<pre>";
        //print_r($_SESSION);
        $addWhere = "";
        $addWhereMerchant = "";
        $levelUser = $this->session->userdata('level_user');
        if ($levelUser == 2) {
            $company_id = $this->session->userdata('company_id');
            $addWhere = " and company_id = ".$company_id;
            $addWhereMerchant = " and company_id = ".$company_id;
        }elseif ($levelUser == 3) {
            $company_id = $this->session->userdata('company_id');
            $merchant_id = $this->session->userdata('merchant_id');
            $addWhere = " and company_id = ".$company_id." and merchant_id =".$merchant_id;
            $addWhereMerchant = " and company_id = ".$company_id;
        }
        //dashboard box
        $data['totalUser'] = $this->datahandler->selectData('*','users','user_group=0');
        $data['totalVoucher'] = $this->datahandler->selectData('*','voucher',"1 = 1 $addWhere");
        $data['totalCategory'] = $this->datahandler->selectData('*','voucher_category','1 = 1');
        $data['totalMerchant'] = $this->datahandler->selectData('*','merchant',"1 = 1 $addWhereMerchant");
        $data['orderBy'] = array(
            '0'=>'Please Select',
            '1'=>'Item',
            '2'=>'Cash',
            '3'=>'Point'
        );

        $data['orderByPoint'] = array(
            '0'=>'Please Select',
            '1'=>'Point In',
            '2'=>'Point Out',
            '3'=>'Total'
        );

        //dashboard User
        $data['statisticUser'] = $this->datahandler->selectData('sum(if(gender = 0,1,0)) as female,sum(if(gender = 1,1,0)) as male,sum(if(gender is null,1,0)) as unk','users','user_group=0');

    	$this->hlmhelper->genView('dashboard/dashboard',$data);
    }

    public function processData($type,$id="",$requestMenu=""){
        switch ($type) {
            case 'statVoucher':
                $startDate = explode("/",$_GET['start_date']);
                $endDate = explode("/",$_GET['end_date']);

                $startDate = $startDate[2]."-".$startDate[1]."-".$startDate[0];
                $endDate = $endDate[2]."-".$endDate[1]."-".$endDate[0];

                $getType = $_GET['type'];
                $merchant_id = "";
                $category_id = "";
                $voucher_id = "";
                $where = "";
                if ($getType == 1) {
                    $merchant_id = $_GET['merchant_id'];
                    if ($merchant_id != "xx") {
                        $where = 'and i.merchant_id = '.$merchant_id;
                    }
                }elseif($getType == 2){
                    $merchant_id = $_GET['merchant_id'];
                    $category_id = $_GET['category_id'];
                    $where = ' and ih.voucher_category = '.$category_id;
                    if ($merchant_id != "xx") {
                        $where .= ' and i.merchant_id = '.$merchant_id;
                    }
                    
                }else{
                    $merchant_id = $_GET['merchant_id'];
                    $category_id = $_GET['category_id'];
                    $voucher_id = $_GET['voucher_id'];
                    $where = ' and ih.voucher_category = '.$category_id.' and ih.voucher_id = "'.$voucher_id.'"';
                    if ($merchant_id != "xx") {
                        $where .= ' and i.merchant_id = '.$merchant_id;
                    }
                }
                $getData = $this->datahandler->selectData('count(*) as total,date(order_date) as order_date','invoice_history as ih 
                                    join invoice as i 
                                        on i.`order_id` = ih.order_id'
                                ,' date(i.`order_date`) >= "'.$startDate.'" 
                                    and date(order_date) <= "'.$endDate.'" '.$where,'','date(order_date)');
                $begin = new DateTime( $startDate );
                $end = new DateTime( $endDate );
                $end = $end->modify( '+1 day' );

                $interval = DateInterval::createFromDateString('1 day');
                $period = new DatePeriod($begin, $interval, $end);

                $month = array();
                $total = array();
                foreach ( $period as $dt ){
                    $getMon =  $dt->format( "Y-m-d" );
                    $month[] = $getMon;
                    $stat = false;
                    foreach ($getData as $key => $value) {
                        if ($value->order_date == $getMon) {
                            $total[] = $value->total;
                            $stat = true;
                            break;
                        }
                    }
                    if (!$stat) {
                        $total[] = 0;
                    }
                }

                $allRet = array($month,$total);
                header('Content-Type: application/json');
                echo json_encode($allRet);
                break;
            case 'statCategory':
                $startDate = explode("/",$_GET['start_date']);
                $endDate = explode("/",$_GET['end_date']);

                $startDate = $startDate[2]."-".$startDate[1]."-".$startDate[0];
                $endDate = $endDate[2]."-".$endDate[1]."-".$endDate[0];

                $category_id = $_GET['category_id'];
                $getData = $this->datahandler->selectData('count(*) as total,date(order_date) as order_date','invoice_history as ih 
                                    join invoice as i 
                                        on i.`order_id` = ih.order_id'
                                ,'ih.voucher_category = "'.$category_id.'" 
                                    and date(i.`order_date`) >= "'.$startDate.'" 
                                    and date(order_date) <= "'.$endDate.'"','','date(order_date)');
                $begin = new DateTime( $startDate );
                $end = new DateTime( $endDate );
                $end = $end->modify( '+1 day' );

                $interval = DateInterval::createFromDateString('1 day');
                $period = new DatePeriod($begin, $interval, $end);

                $month = array();
                $total = array();
                foreach ( $period as $dt ){
                    $getMon =  $dt->format( "Y-m-d" );
                    $month[] = $getMon;
                    $stat = false;
                    foreach ($getData as $key => $value) {
                        if ($value->order_date == $getMon) {
                            $total[] = $value->total;
                            $stat = true;
                            break;
                        }
                    }
                    if (!$stat) {
                        $total[] = 0;
                    }
                }

                $allRet = array($month,$total);
                header('Content-Type: application/json');
                echo json_encode($allRet);
                break;
            case 'statUser':
                $startDate = explode("/",$_GET['start_date']);
                $endDate = explode("/",$_GET['end_date']);

                $startDate = $startDate[2]."-".$startDate[1]."-".$startDate[0];
                $endDate = $endDate[2]."-".$endDate[1]."-".$endDate[0];

                $user_id = $_GET['user_id'];
                $getData = $this->datahandler->selectData('count(*) as total,date(order_date) as order_date','invoice_history as ih 
                                    join invoice as i 
                                        on i.`order_id` = ih.order_id'
                                ,'i.customer_id = "'.$user_id.'" 
                                    and date(i.`order_date`) >= "'.$startDate.'" 
                                    and date(order_date) <= "'.$endDate.'"','','date(order_date)');
                $begin = new DateTime( $startDate );
                $end = new DateTime( $endDate );
                $end = $end->modify( '+1 day' );

                $interval = DateInterval::createFromDateString('1 day');
                $period = new DatePeriod($begin, $interval, $end);

                $month = array();
                $total = array();
                foreach ( $period as $dt ){
                    $getMon =  $dt->format( "Y-m-d" );
                    $month[] = $getMon;
                    $stat = false;
                    foreach ($getData as $key => $value) {
                        if ($value->order_date == $getMon) {
                            $total[] = $value->total;
                            $stat = true;
                            break;
                        }
                    }
                    if (!$stat) {
                        $total[] = 0;
                    }
                }

                $allRet = array($month,$total);
                header('Content-Type: application/json');
                echo json_encode($allRet);
                break;
            case 'ajaxTopVoucher':
                $columns = array( 
                    'ih.id',
                    'm.name as merchant_id',
                    'v.voucher_id as voucher_id',
                    'v.name as name',
                    'vc.name as voucher_category',
                    'count(*) as total_order',
                    'format(sum(ih.price),2) as cash',
                    'sum(ih.point) as point'
                );
                $nama_tbl = "invoice_history as ih 
                                join invoice as i 
                                    on i.`order_id` = ih.order_id
                                join voucher as v 
                                    on ih.voucher_id = v.voucher_id
                                join voucher_category as vc 
                                    on v.voucher_category = vc.id
                                join merchant as m 
                                    on v.merchant_id = m.id";
                $orderBy = "";
                //print_r($_GET);
                if (isset($_GET['f_order'])) {
                    $f_order = $_GET['f_order'];
                    $s_order = $_GET['s_order'];
                    $t_order = $_GET['t_order'];

                    if ($f_order != "0") {
                        if ($f_order==1) {
                            $orderBy .= "count(*) desc";
                        }elseif ($f_order==2) {
                            $orderBy .= "sum(ih.price) desc";
                        }else{
                            $orderBy .= "sum(ih.point) desc";
                        }
                    }
                    if ($s_order != "0") {
                        if ($orderBy!="") {
                            $orderBy .= ",";
                        }
                        if ($s_order==1) {
                            $orderBy .= "count(*) desc";
                        }elseif ($s_order==2) {
                            $orderBy .= "sum(ih.price) desc";
                        }else{
                            $orderBy .= "sum(ih.point) desc";
                        }
                    }

                    if ($t_order != "0") {
                        if ($orderBy!="") {
                            $orderBy .= ",";
                        }
                        if ($t_order==1) {
                            $orderBy .= "count(*) desc";
                        }elseif ($t_order==2) {
                            $orderBy .= "sum(ih.price) desc";
                        }else{
                            $orderBy .= "sum(ih.point) desc";
                        }
                    }
                }

                if ($orderBy == "") {
                    $orderBy = "count(*) desc";
                }
                
                $custom_where = "";
                $hide_column = "";
                $custom_order = $orderBy;
                $custom_if =array();
                $custom_group = "v.voucher_id";

                $method_link = array();
                $icon_img = array();

                $custom_field = array();
                $custom_thick = "";

                $hide_column="ih.id";
                $tbl_id = "ih.id";

                $custom_link = array();

                $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                break;
            case 'ajaxTopUser':
                $columns = array( 
                    'ih.id',
                    'u.email',
                    'u.name',
                    'count(*) as total_order',
                    'format(sum(ih.price),2) as cash',
                    'sum(ih.point) as point'
                );
                $nama_tbl = "invoice_history as ih 
                                join invoice as i 
                                    on i.`order_id` = ih.order_id
                                join voucher as v 
                                    on ih.voucher_id = v.voucher_id
                                join users as u 
                                    on i.customer_id = u.id
                                join voucher_category as vc 
                                    on ih.voucher_category = vc.id";
                $orderBy = "";
                //print_r($_GET);
                if (isset($_GET['f_order'])) {
                    $f_order = $_GET['f_order'];
                    $s_order = $_GET['s_order'];
                    $t_order = $_GET['t_order'];

                    if ($f_order != "0") {
                        if ($f_order==1) {
                            $orderBy .= "count(*) desc";
                        }elseif ($f_order==2) {
                            $orderBy .= "sum(ih.price) desc";
                        }else{
                            $orderBy .= "sum(ih.point) desc";
                        }
                    }
                    if ($s_order != "0") {
                        if ($orderBy!="") {
                            $orderBy .= ",";
                        }
                        if ($s_order==1) {
                            $orderBy .= "count(*) desc";
                        }elseif ($s_order==2) {
                            $orderBy .= "sum(ih.price) desc";
                        }else{
                            $orderBy .= "sum(ih.point) desc";
                        }
                    }

                    if ($t_order != "0") {
                        if ($orderBy!="") {
                            $orderBy .= ",";
                        }
                        if ($t_order==1) {
                            $orderBy .= "count(*) desc";
                        }elseif ($t_order==2) {
                            $orderBy .= "sum(ih.price) desc";
                        }else{
                            $orderBy .= "sum(ih.point) desc";
                        }
                    }
                }

                if ($orderBy == "") {
                    $orderBy = "count(*) desc";
                }

                $custom_where = "";
                $hide_column = "";
                $custom_order = $orderBy;
                $custom_if =array();
                $custom_group = "i.customer_id";

                $method_link = array();
                $icon_img = array();

                $custom_field = array();
                $custom_thick = "";

                $hide_column="ih.id";
                $tbl_id = "ih.id";

                $custom_link = array();

                $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                break;
            case 'ajaxTopTransaction':
                $columns = array( 
                    'ph.id',
                    'u.email',
                    'u.name',
                    'sum(if(type=5 and mutation = 1,ph.point,0)) as total_in',
                    'sum(if(type=6 and mutation = 2,ph.point,0)) as total_out',
                    '(sum(if(type=5 and mutation = 1,ph.point,0))+sum(if(type=6 and mutation = 2,ph.point,0))) as total'
                );
                $nama_tbl = "point_history as ph
                                join users as u 
                                    on ph.customer_id = u.id";

                $orderBy = "";
                if (isset($_GET['f_order'])) {
                    $f_order = $_GET['f_order'];
                    $s_order = $_GET['s_order'];
                    $t_order = $_GET['t_order'];

                    if ($f_order != "0") {
                        if ($f_order==1) {
                            $orderBy .= "sum(if(type=5 and mutation = 1,ph.point,0)) desc";
                        }elseif ($f_order==2) {
                            $orderBy .= "sum(if(type=6 and mutation = 2,ph.point,0)) desc";
                        }else{
                            $orderBy .= "sum(if(type=5 and mutation = 1,ph.point,0))+sum(if(type=6 and mutation = 2,ph.point,0)) desc";
                        }
                    }
                    if ($s_order != "0") {
                        if ($orderBy!="") {
                            $orderBy .= ",";
                        }
                        if ($s_order==1) {
                            $orderBy .= "sum(if(type=5 and mutation = 1,ph.point,0)) desc";
                        }elseif ($s_order==2) {
                            $orderBy .= "sum(if(type=6 and mutation = 2,ph.point,0)) desc";
                        }else{
                            $orderBy .= "sum(if(type=5 and mutation = 1,ph.point,0))+sum(if(type=6 and mutation = 2,ph.point,0)) desc";
                        }
                    }

                    if ($t_order != "0") {
                        if ($orderBy!="") {
                            $orderBy .= ",";
                        }
                        if ($t_order==1) {
                            $orderBy .= "sum(if(type=5 and mutation = 1,ph.point,0)) desc";
                        }elseif ($t_order==2) {
                            $orderBy .= "sum(if(type=6 and mutation = 2,ph.point,0)) desc";
                        }else{
                            $orderBy .= "sum(if(type=5 and mutation = 1,ph.point,0))+sum(if(type=6 and mutation = 2,ph.point,0)) desc";
                        }
                    }
                }

                if ($orderBy == "") {
                    $orderBy = "sum(if(type=5 and mutation = 1,ph.point,0))+sum(if(type=6 and mutation = 2,ph.point,0)) desc";
                }
                $custom_where = "";
                $hide_column = "";
                $custom_order = $orderBy;
                $custom_if =array();
                $custom_group = "ph.customer_id";

                $method_link = array();
                $icon_img = array();

                $custom_field = array();
                $custom_thick = "";

                $hide_column="ph.id";
                $tbl_id = "ph.id";

                $custom_link = array();

                $this->hlmhelper->ajaxhilman($nama_tbl,$tbl_id,$hide_column,$columns,$custom_where,$custom_order,$custom_if,$method_link,$icon_img,$custom_group,$custom_link,$custom_field,$custom_thick,$requestMenu);
                break;
            case 'dropDownVoucher':
                $merchant_id = $_GET['merchant_id'];
                $category_id = $_GET['category_id'];
                $where = "";
                if ($merchant_id == "xx") {
                    $where = 'voucher_category = '.$category_id;
                }else{
                    $where = 'merchant_id = '.$merchant_id.' and voucher_category = '.$category_id;
                }
                $getVoucher = $this->datahandler->selectData('*','voucher',$where);
                $op = "<option value=''>Please Select</option>";
                foreach ($getVoucher as $key => $value) {
                    $op .= "<option value='".$value->voucher_id."' data-tokens='".$value->voucher_id."-".$value->name."'>".$value->voucher_id." - ".$value->name."</option>";
                }
                echo $op;
                break;
            default:
                # code...
                break;
        }
    }

    public function profile($id=""){
        $params = array();
        if($id==$this->session->userdata('id')) {
            $params['dataEdit'] = $this->db->where('id',$id)
                                           ->get('users')->result();
            $params['idUser'] = $id;
            $params['statEdit'] = true;
            return $this->hlmhelper->genView('profile',$params);
        }else{
            return $this->hlmhelper->throwError();
        }   
    }

    function updateProfile(){
        $allData = $this->input->post();
        $dataUpdate = array();
        if ($allData['password']!="") {
            $dataUpdate['password'] = file_get_contents(BASE_API_URL.'hash_pass/'.$allData['password']);
        }

        $dataUpdate['name'] = $allData['name'];

        $config['upload_path']          = './data_uploads/photo_profile/'.$allData['idUser'];
        $config['allowed_types']        = 'gif|jpg|png|jpg';
        $config['max_size']             = 10000;

        $oldData = $this->db->where('id',$allData['idUser'])->get('users')->result();
        if (is_uploaded_file($_FILES['imgSrc']['tmp_name'])) {
            $fileName = $_FILES['imgSrc']['name'];
            $ext = explode('.', $fileName);
            $ext = $ext[1];
            $newFileName = $config['file_name'] = date('Ymdhis').".".$ext;
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('imgSrc'))
            {
                $this->session->set_flashdata('error','Uploading problem, please check your file extention !!!');
                return redirect('/dashboard/profile/'.$allData['idUser']);
            }
            else
            {   
                if (file_exists('./data_uploads/photo_profile/'.$allData['idUser']."/".$oldData[0]->photo)) {
                    unlink('./data_uploads/photo_profile/'.$allData['idUser']."/".$oldData[0]->photo);
                }
                $dataUpdate['foto'] = trim($newFileName);
            }
        }

        $this->datahandler->updateData('users',$dataUpdate,'id='.$allData['idUser']);

        $this->session->set_flashdata('success','Profile details is updated');
        return redirect('/dashboard/profile/'.$allData['idUser']);
    }

    //[new function append]
}