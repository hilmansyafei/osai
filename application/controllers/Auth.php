<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Copyright
	 * Hilanproject.com
	 * November 2016
	 */
	public function login(){
		if (isset($this->session->userdata['authApp'])) {
			$url='admin/dashboard';
			redirect($url, 'refresh');
		}else{
			$this->load->view('admin/login');
		}
	}
	public function authcheck(){
		date_default_timezone_set('Asia/Jakarta');
		$dataAuth = $this->input->post();
		$username = $_POST['username'];	
		$password = $_POST['password'];
		$salt = "!)@^#$%(*&^";
		
		$cekUser = $this->db->where('username',$username)->where('password',hash('sha512',$password.$salt))->get('users');
		$datauser = $cekUser->result();
		$cek = $cekUser->num_rows();
		if ($cek>0) {
			if ($datauser[0]->status==0) {
				$url='/auth/login';
				$this->session->set_flashdata('warning','Username Inactive');
			}else{
				$dataInsert = array('user_agent'=>"username ".$username." ".$_SERVER['HTTP_USER_AGENT']." from ".$_SERVER['REMOTE_ADDR']." date ".date('Y-m-d H:i:s'));
				$this->db->insert('log',$dataInsert);
				$this->session->set_userdata('username',$username);
				$this->session->set_userdata('idUser',$datauser[0]->id);
				$this->session->set_userdata('authApp',md5($username));
				$this->session->set_userdata('nama_pegawai',$datauser[0]->nama_pegawai);
				$this->session->set_userdata('nip',$datauser[0]->nip);
				$this->session->set_userdata('nomor_anggota',$datauser[0]->nomor_anggota);
				$this->session->set_userdata('kode_unit_organisasi',$datauser[0]->kode_unit_organisasi);
				$this->session->set_userdata('kode_unit_kerja',$datauser[0]->kode_unit_kerja);
				$this->session->set_userdata('jabatan',$datauser[0]->jabatan);
				$url='/admin/dashboard';
			}
			redirect($url, 'refresh');
		}else{
			$url='/auth/login';
			$this->session->set_flashdata('warning','Invalid username and password');
			redirect($url, 'refresh');
		}

	}

	public function logout(){
		$this->session->sess_destroy();
		$url='/auth/login';
		redirect($url, 'refresh');
	}

}
