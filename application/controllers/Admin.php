<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	/**
	 * Copyright
	 * Hilman Syafei
	 * Februari 2017
	 */
	public function __construct(){
        parent::__construct();
        $this->auth();
        date_default_timezone_set('Asia/Jakarta');
    }

    public function auth(){
		if (!isset($this->session->userdata['authApp'])) {
			$url='auth/login';
			redirect($url, 'refresh');
		}
	}

	public function checkPriviledge($id="",$table)
	{
		$nip = $this->session->userdata('nip');
		$chekNum = $this->db->where('id',$id)->where('nip',$nip)->get($table)->num_rows();
		if (!$chekNum) {
			return false;
		}else{
			return true;
		}
	}

	public function getUnitKerja(){
		$kode_organisasi = $_GET['s_kode_organisasi'];
		$allData = $this->db->where('kode_unit_organisasi',$kode_organisasi)->get('unit_kerja')->result();
		$dataRet = "";
		foreach ($allData as $key => $value) {
			$dataRet.="<option value='".$value->kode_unit_kerja."'>".$value->nama_unit_kerja."</option>";
		}
		echo $dataRet;
		return 1;
	}

	public function render_view($content = "main", $params = array()){
		$nip = $this->session->userdata('nip');
		$allData = $this->db->where('nip',$nip)->get('users')->result();
		$params['foto'] = $allData[0]->photo;
		$params['nama_pegawai'] = $allData[0]->nama_pegawai;
		$params['nomor_anggota'] = $allData[0]->nomor_anggota;
		$params['jabatan'] = $allData[0]->jabatan;
		$getUnitKerja = $this->db->where('kode_unit_kerja',$allData[0]->kode_unit_kerja)->get('unit_kerja')->result();
		$params['nama_unit_kerja']  = $getUnitKerja[0]->nama_unit_kerja;
		$params['kode_unit_kerja'] = $allData[0]->kode_unit_kerja;
		$params['kode_unit_organisasi'] = $allData[0]->kode_unit_organisasi;
		$this->load->view('admin/header',$params);
		$this->load->view('admin/sidebar',$params);
		$this->load->view("admin/".$content,$params);
		$this->load->view('admin/footer',$params);
	}

	public function errorPage($value='')
	{
		$this->render_view('error');
	}

	public function index(){
		$this->dashboard();
	}

	public function dashboard(){
		$params = array();
		$this->render_view('dashboard',$params);
	}

	public function profile($id=""){
		$params = array();
		if($id==$this->session->userdata('idUser')) {
			$params['dataEdit'] = $this->db->where('id',$id)
										   ->get('users')->result();
			$params['idUser'] = $id;
			$params['statEdit'] = true;
			$this->render_view('profile',$params);
		}else{
			$this->render_view('error',$params);
		}	
	}

	public function userManagement($type="get",$id=""){
		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'users');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idUser'],'users');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'get':
					if (isset($_GET['edit'])) {
						$params['dataEdit'] = $this->db->where('id',$_GET['edit'])
													   ->get('users')->result();
						$params['idUser'] = $_GET['edit'];
						$params['statEdit'] = true;
					}else{
						$params['statEdit'] = false;
					}
					$params['title'] = "";
					$this->render_view('userManagement',$params);
					break;
				case 'saveEdit':
					$allData = $this->input->post();
					$dataUpdate = array();
					if ($allData['password']!="") {
						$dataUpdate['password'] = md5($allData['password']);
					}

					if (!isset($allData['status'])) {
						$dataUpdate['status'] = 1;
					}else{
						$dataUpdate['status'] = $allData['status'];
					}
					$dataUpdate['nama_pegawai'] = $allData['nama_pegawai'];

					$config['upload_path']          = './data_uploads/photo_profile';
	                $config['allowed_types']        = 'gif|jpg|png|jpg';
	                $config['max_size']             = 10000;

	                $oldData = $this->db->where('id',$allData['idUser'])->get('users')->result();
					if (is_uploaded_file($_FILES['imgSrc']['tmp_name'])) {
						$fileName = $_FILES['imgSrc']['name'];
		                $ext = explode('.', $fileName);
		                $ext = $ext[1];
		                $newFileName = $config['file_name'] = date('Ymdhis').".".$ext;
		                $this->load->library('upload', $config);
		                if ( ! $this->upload->do_upload('imgSrc'))
		                {
		                    $this->session->set_flashdata('message','Uploading error !!!');
							echo $this->upload->display_errors();
							die();
		                }
		                else
		                {	
	                		if (file_exists('./data_uploads/photo_profile/'.$oldData[0]->photo)) {
	                			unlink('./data_uploads/photo_profile/'.$oldData[0]->photo);
	                		}
	                        $dataUpdate['photo'] = trim($newFileName);
		                }
		            }
		            $dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idUser'])
							 ->update('users',$dataUpdate);

					$this->session->set_flashdata('message','Profile details updated');
					redirect('/admin/profile/'.$allData['idUser']);
					break;
				case 'saveNew':
					$allData = $this->input->post();
					$dataInsert = array();
					

					$dataInsert['password'] = md5($allData['password']);
					$dataInsert['status'] = $allData['status'];
					$dataInsert['username'] = $allData['username'];
					$dataInsert['nama_pegawai'] = $allData['nama_pegawai'];

					$this->db->insert('users',$dataInsert);

					$this->session->set_flashdata('message','1 User added');
					redirect('/admin/userList');
					break;
				case 'delete':
					$this->db->where('id',$id)
							 ->delete('users');
					$this->session->set_flashdata('message','1 User deleted');
					redirect('/admin/userList');
					break;
				default:
					# code...
					break;
			}
		}else{
			$this->errorPage();
		}	
	}

	public function contentManagement($type="get",$id=""){
		$params['pendidikan'] = $this->db->where('jenis_ref','pendidikan')->get('referensi')->result_array();
		$params['warga_negara'] = $this->db->where('jenis_ref','warga_negara')->get('referensi')->result_array();
		$params['status_perkawinan'] = $this->db->where('jenis_ref','status_perkawinan')->get('referensi')->result_array();
		$params['agama'] = $this->db->where('jenis_ref','agama')->get('referensi')->result_array();
		$params['nama_bank'] = $this->db->where('jenis_ref','nama_bank')->get('referensi')->result_array();
		$params['jenis_kelamin'] = $this->db->where('jenis_ref','jenis_kelamin')->get('referensi')->result_array();
		$params['jenis_kelamin_index']  = array('L'=>"Laki-Laki",'P'=>"Perempuan");
		$params['status_jabatan'] = $this->db->where('jenis_ref','status_jabatan')->get('referensi')->result_array();
		$params['status_rwy_jabatan'] = $this->db->where('jenis_ref','status_rwy_jabatan')->get('referensi')->result_array();
		$params['unit_organisasi'] = $this->db->get('unit_organisasi')->result_array();
		$params['unit_kerja'] = $this->db->get('unit_kerja')->result_array();
		$params['bulan'] = array('1' =>'Januari',
								 '2'=>'Februari',
								 '3'=>'Maret',
								 '4'=>'April',
								 '5'=>'Mei',
								 '6'=>'Juni',
								 '7'=>'Juli',
								 '8'=>'Agustus',
								 '9'=>'September',
								 '10'=>'Oktober',
								 '11'=>'November',
								 '12'=>'Desember' );
		switch ($type) {
			case 'dataPribadiPegawai':
				$idUser = $this->session->userdata('idUser');
				$params['title'] = "Data Pribadi Pegawai";
				$params['dataEdit'] = $this->db->where('id',$idUser)->get('users')->result();;
				$params['idUser'] = $idUser;
				$params['statEdit'] = true;
				$this->render_view('dataPribadiPegawai',$params);
				break;
			case 'riwayatTempatTinggal':
				$nip = $this->session->userdata('nip');
				$params['title'] = "Riwayat Tempat Tinggal";
				$params['allData'] = $this->db->where('nip',$nip)->order_by('tanggal_mulai_ditempati','ASC')->get('rwy_tempat_tinggal')->result();;
				$params['nip'] = $nip;
				$params['statEdit'] = true;
				$this->render_view('riwayatTempatTinggal',$params);
				break;
			case 'riwayatKeluarga':
				$nip = $this->session->userdata('nip');
				$params['title'] = "Riwayat Keluarga";
				$params['allData'] = $this->db->where('nip',$nip)->get('rwy_keluarga')->result();;
				$params['nip'] = $nip;
				$params['statEdit'] = true;
				$this->render_view('riwayatKeluarga',$params);
				break;
			case 'riwayatPendidikan':
				$nip = $this->session->userdata('nip');
				$params['title'] = "Riwayat Pendidikan Formal";
				$params['allData'] = $this->db->where('nip',$nip)->order_by('tanggal_ijazah','ASC')->get('rwy_pendidikan')->result();;
				$params['nip'] = $nip;
				$params['statEdit'] = true;
				$this->render_view('riwayatPendidikan',$params);
				break;
			case 'riwayatPenguasaanBahasa':
				$nip = $this->session->userdata('nip');
				$params['title'] = "Riwayat Penguasaan Bahasa";
				$params['allData'] = $this->db->where('nip',$nip)->order_by('change_date','ASC')->get('rwy_penguasaan_bahasa')->result();;
				$params['nip'] = $nip;
				$params['statEdit'] = true;
				$this->render_view('riwayatPenguasaanBahasa',$params);
				break;
			case 'ciriCiriJasmani':
				$nip = $this->session->userdata('nip');
				$params['title'] = "Ciri-ciri Jasmani";
				$params['allData'] = $this->db->where('nip',$nip)->order_by('change_date','ASC')->get('ciri_ciri_jasmani')->result();;
				$params['nip'] = $nip;
				$params['statEdit'] = true;
				$this->render_view('ciriCiriJasmani',$params);
				break;
			case 'riwayatBidangKeahlian':
				$nip = $this->session->userdata('nip');
				$params['title'] = "Riwayat Bidang Keahlian";
				$params['allData'] = $this->db->where('nip',$nip)->order_by('change_date','ASC')->get('rwy_bidang_keahlian')->result();;
				$params['nip'] = $nip;
				$params['statEdit'] = true;
				$this->render_view('riwayatBidangKeahlian',$params);
				break;
			case 'relasi':
				$nip = $this->session->userdata('nip');
				$params['title'] = "Relasi";
				$params['allData'] = $this->db->where('nip',$nip)->order_by('change_date','ASC')->get('relasi')->result();;
				$params['nip'] = $nip;
				$params['statEdit'] = true;
				$this->render_view('relasi',$params);
				break;
			case 'riwayatJabatan':
				$nip = $this->session->userdata('nip');
				$params['title'] = "RIwayat Jabatan";
				$params['allData'] = $this->db->query("SELECT 
														  rj.`id`,
														  uo.`nama_organisasi`,
														  uk.`nama_unit_kerja`,
														  rj.`jabatan`,
														  rj.`fungsi_bidang`,
														  rj.`tanggal_sk`,
														  rj.`berlaku_sejak`,
														  rj.`tanggal_berakhir`,
														  rj.`no_sk`,
														  rj.`status_jabatan`,
														  rj.`status_rwy_jabatan`,
														  rj.`stat_validasi` 
														FROM
														  rwy_jabatan AS rj 
														  JOIN users AS u 
														    ON rj.`nip` = u.`nip` 
														  JOIN unit_organisasi AS uo 
														    ON rj.`kode_unit_organisasi` = uo.`kode_organisasi` 
														  JOIN unit_kerja AS uk 
														    ON rj.`kode_unit_kerja` = uk.`kode_unit_kerja` 
														WHERE u.`nip` = '".$nip."' order by rj.id ASC ")->result();
				$params['nip'] = $nip;
				$this->render_view('rwy_jabatan',$params);
				break;
			case 'rwySahamPegawai':
				$nip = $this->session->userdata('nip');
				$params['title'] = "Riwayat Persesiaan Saham Pegawai";
				$params['allData'] = $this->db->query("SELECT 
														  u.`nomor_anggota`,
														  u.`nama_pegawai`,
														  uo.`nama_organisasi`,
														  uk.`nama_unit_kerja`,
														  rph.* 
														FROM
														  rwy_persediaan_saham_pegawai AS rph 
														  JOIN users AS u 
														    ON rph.`nip` = u.`nip` 
														  JOIN unit_organisasi AS uo 
														    ON rph.`kode_unit_organisasi` = uo.`kode_organisasi` 
														  JOIN unit_kerja AS uk 
														    ON rph.`kode_unit_kerja` = uk.`kode_unit_kerja`
														WHERE u.`nip` = '".$nip."'")->result();
				$params['nip'] = $nip;
				$params['statEdit'] = true;
				$this->render_view('rwySahamPegawai',$params);
				break;
		}
	}

	public function dataPribadiPegawaiManajemen($type="get",$id=""){
		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'users');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idUser'],'users');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'saveEdit':
					$dataUpdate = array();
					foreach ($allData as $key => $value) {
						if($key!="idUser" && $value!="Belum Diset"){
							$dataUpdate[$key] = $value;
						}
					}
					$dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idUser'])->update('users',$dataUpdate);

					$this->session->set_flashdata('message','Icon details updated');
					redirect('/admin/contentManagement/dataPribadiPegawai');
					break;
				case 'saveNew':

					break;
				case 'delete':

					$this->db->where('id',$id)
							 ->delete('users');

					$this->session->set_flashdata('message','1 Data Berhasil Dihapus');
					redirect('/admin/contentManagement/dataPribadiPegawai');
					break;
			}
		}else{
			$this->errorPage();
		}
	}

	public function riwayatTempatTinggalManajemen($type="get",$id=""){
		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'rwy_tempat_tinggal');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idRwyTempatTinggal'],'rwy_tempat_tinggal');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'addNew':
					$nip = $this->session->userdata('nip');
					$params['title'] = "Riwayat Tempat Tinggal";
					$params['statEdit'] = false;
					$params['nip'] = $nip;
					$this->render_view('riwayatTempatTinggalManajemen',$params);
					break;
				case 'edit':
					$params['title'] = "Riwayat Tempat Tinggal";
					$params['dataEdit'] = $this->db->where('id',$id)->get('rwy_tempat_tinggal')->result();
					$params['statEdit'] = true;
					$params['idRwyTempatTinggal'] = $id;
					$this->render_view('riwayatTempatTinggalManajemen',$params);
					break;
				case 'saveEdit':
					$allData = $this->input->post();
					$dataUpdate = array();
					foreach ($allData as $key => $value) {
						if($key!="idRwyTempatTinggal" && $value!="Belum Diset"){
							$dataUpdate[$key] = $value;
						}
					}
					$dataUpdate['stat_validasi'] = 0;
					$dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idRwyTempatTinggal'])->update('rwy_tempat_tinggal',$dataUpdate);

					$this->session->set_flashdata('message','Data Berhasil Diubah');
					redirect('/admin/contentManagement/riwayatTempatTinggal');
					break;
				case 'saveNew':
					$allData = $this->input->post();
					$dataInsert = array();
					foreach ($allData as $key => $value) {
						if($key!="idUser" && $value!="Belum Diset"){
							$dataInsert[$key] = $value;
						}
					}
					$dataInsert['change_date'] = date("Y-m-d H:i:s");
		            $dataInsert['change_by'] = $this->session->userdata('username');

	                $this->db->insert('rwy_tempat_tinggal',$dataInsert);
					$this->session->set_flashdata('message','Data Berhasil Ditambah');
					redirect('/admin/contentManagement/riwayatTempatTinggal');
					break;
				case 'delete':
					$this->db->where('id',$id)->delete('rwy_tempat_tinggal')->result();

					$this->session->set_flashdata('message',' 1 Data Berhasil Dihapus');
					redirect('/admin/contentManagement/riwayatTempatTinggal');
					break;
			}
		}else{
			$this->errorPage();
		}
	}

	public function riwayatKeluargaManajemen($type="get",$id=""){
		$params['pendidikan'] = $this->db->where('jenis_ref','pendidikan')->get('referensi')->result_array();
		$params['hubungan_keluarga'] = $this->db->where('jenis_ref','hubungan_keluarga')->get('referensi')->result_array();
		$params['jenis_kelamin'] = $this->db->where('jenis_ref','jenis_kelamin')->get('referensi')->result_array();
		$params['jenis_kelamin_index']  = array('L'=>"Laki-Laki",'P'=>"Perempuan");
		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'rwy_keluarga');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idRwyKeluarga'],'rwy_keluarga');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'addNew':
					$nip = $this->session->userdata('nip');
					$params['title'] = "Riwayat Keluarga";
					$params['statEdit'] = false;
					$params['nip'] = $nip;
					$this->render_view('riwayatKeluargaManajemen',$params);
					break;
				case 'edit':
					$params['title'] = "Riwayat Keluarga";
					$params['dataEdit'] = $this->db->where('id',$id)->get('rwy_keluarga')->result();
					$params['statEdit'] = true;
					$params['idRwyKeluarga'] = $id;
					$this->render_view('riwayatKeluargaManajemen',$params);
					break;
				case 'saveEdit':
					$allData = $this->input->post();
					$dataUpdate = array();
					foreach ($allData as $key => $value) {
						if($key!="idRwyKeluarga" && $value!="Belum Diset"){
							$dataUpdate[$key] = $value;
						}
					}
					$dataUpdate['stat_validasi'] = 0;
					$dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idRwyKeluarga'])->update('rwy_keluarga',$dataUpdate);

					$this->session->set_flashdata('message','Data Berhasil Diubah');
					redirect('/admin/contentManagement/riwayatKeluarga');
					break;
				case 'saveNew':
					$allData = $this->input->post();
					$dataInsert = array();
					foreach ($allData as $key => $value) {
						if($key!="idRwyKeluarga" && $value!="Belum Diset"){
							$dataInsert[$key] = $value;
						}
					}
					$dataInsert['nip'] = $this->session->userdata('nip');
					$dataInsert['change_date'] = date("Y-m-d H:i:s");
		            $dataInsert['change_by'] = $this->session->userdata('username');

	                $this->db->insert('rwy_keluarga',$dataInsert);
					$this->session->set_flashdata('message','Data Berhasil Ditambah');
					redirect('/admin/contentManagement/riwayatKeluarga');
					break;
				case 'delete':
					$this->db->where('id',$id)->delete('rwy_keluarga')->result();

					$this->session->set_flashdata('message',' 1 Data Berhasil Dihapus');
					redirect('/admin/contentManagement/riwayatKeluarga');
					break;
			}
		}else{
			$this->errorPage();
		}
	}

	public function riwayatPendidikanManajemen($type="get",$id=""){
		$params['pendidikan'] = $this->db->where('jenis_ref','pendidikan')->get('referensi')->result_array();
		$params['lokasi_pendidikan'] = $this->db->where('jenis_ref','lokasi_pendidikan')->get('referensi')->result_array();

		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'rwy_pendidikan');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idPendidikan'],'rwy_pendidikan');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'addNew':
					$nip = $this->session->userdata('nip');
					$params['title'] = "Riwayat Pendidikan Formal";
					$params['statEdit'] = false;
					$params['nip'] = $nip;
					$this->render_view('riwayatPendidikanManajemen',$params);
					break;
				case 'edit':
					$params['title'] = "Riwayat Pendidikan Formal";
					$params['dataEdit'] = $this->db->where('id',$id)->get('rwy_pendidikan')->result();
					$params['statEdit'] = true;
					$params['idPendidikan'] = $id;
					$this->render_view('riwayatPendidikanManajemen',$params);
					break;
				case 'saveEdit':
					$allData = $this->input->post();
					$dataUpdate = array();
					foreach ($allData as $key => $value) {
						if($key!="idPendidikan" && $value!="Belum Diset"){
							$dataUpdate[$key] = $value;
						}
					}
					$dataUpdate['stat_validasi'] = 0;
					$dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idPendidikan'])->update('rwy_pendidikan',$dataUpdate);

					$this->session->set_flashdata('message','Data Berhasil Diubah');
					redirect('/admin/contentManagement/riwayatPendidikan');
					break;
				case 'saveNew':
					$allData = $this->input->post();
					$dataInsert = array();
					foreach ($allData as $key => $value) {
						if($key!="idPendidikan" && $value!="Belum Diset"){
							$dataInsert[$key] = $value;
						}
					}
					$dataInsert['nip'] = $this->session->userdata('nip');
					$dataInsert['change_date'] = date("Y-m-d H:i:s");
		            $dataInsert['change_by'] = $this->session->userdata('username');

	                $this->db->insert('rwy_pendidikan',$dataInsert);
					$this->session->set_flashdata('message','Data Berhasil Ditambah');
					redirect('/admin/contentManagement/riwayatPendidikan');
					break;
				case 'delete':
					$this->db->where('id',$id)->delete('rwy_pendidikan')->result();

					$this->session->set_flashdata('message',' 1 Data Berhasil Dihapus');
					redirect('/admin/contentManagement/riwayatPendidikan');
					break;
			}
		}else{
			$this->errorPage();
		}
	}

	public function riwayatPenguasaanBahasaManajemen($type="get",$id=""){
		$params['pendidikan'] = $this->db->where('jenis_ref','pendidikan')->get('referensi')->result_array();
		$params['lokasi_pendidikan'] = $this->db->where('jenis_ref','lokasi_pendidikan')->get('referensi')->result_array();

		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'rwy_penguasaan_bahasa');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idPenguasaanBahasa'],'rwy_penguasaan_bahasa');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'addNew':
					$nip = $this->session->userdata('nip');
					$params['title'] = "Riwayat Penguasaan Bahasa";
					$params['statEdit'] = false;
					$params['nip'] = $nip;
					$this->render_view('riwayatPenguasaanBahasaManajemen',$params);
					break;
				case 'edit':
					$params['title'] = "Riwayat Pendidikan Formal";
					$params['dataEdit'] = $this->db->where('id',$id)->get('rwy_penguasaan_bahasa')->result();
					$params['statEdit'] = true;
					$params['idPenguasaanBahasa'] = $id;
					$this->render_view('riwayatPenguasaanBahasaManajemen',$params);
					break;
				case 'saveEdit':
					$allData = $this->input->post();
					$dataUpdate = array();
					foreach ($allData as $key => $value) {
						if($key!="idPenguasaanBahasa" && $value!="Belum Diset"){
							$dataUpdate[$key] = $value;
						}
					}
					$dataUpdate['stat_validasi'] = 0;
					$dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idPenguasaanBahasa'])->update('rwy_penguasaan_bahasa',$dataUpdate);

					$this->session->set_flashdata('message','Data Berhasil Diubah');
					redirect('/admin/contentManagement/riwayatPenguasaanBahasa');
					break;
				case 'saveNew':
					$allData = $this->input->post();
					$dataInsert = array();
					foreach ($allData as $key => $value) {
						if($key!="idPenguasaanBahasa" && $value!="Belum Diset"){
							$dataInsert[$key] = $value;
						}
					}
					$dataInsert['nip'] = $this->session->userdata('nip');
					$dataInsert['change_date'] = date("Y-m-d H:i:s");
		            $dataInsert['change_by'] = $this->session->userdata('username');

	                $this->db->insert('rwy_penguasaan_bahasa',$dataInsert);
					$this->session->set_flashdata('message','Data Berhasil Ditambah');
					redirect('/admin/contentManagement/riwayatPenguasaanBahasa');
					break;
				case 'delete':
					$this->db->where('id',$id)->delete('rwy_penguasaan_bahasa')->result();

					$this->session->set_flashdata('message',' 1 Data Berhasil Dihapus');
					redirect('/admin/contentManagement/riwayatPenguasaanBahasa');
					break;
			}
		}else{
			$this->errorPage();
		}
	}

	public function CiriciriJasmaniManajemen($type="get",$id=""){
		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'ciri_ciri_jasmani');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idCiriCiriJasmani'],'ciri_ciri_jasmani');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'addNew':
					$nip = $this->session->userdata('nip');
					$params['title'] = "Ciri-ciri Jasmani";
					$params['statEdit'] = false;
					$params['nip'] = $nip;
					$this->render_view('ciriCiriJasmaniManajemen',$params);
					break;
				case 'edit':
					$params['title'] = "Ciri-ciri Jasmani";
					$params['dataEdit'] = $this->db->where('id',$id)->get('ciri_ciri_jasmani')->result();
					$params['statEdit'] = true;
					$params['idCiriCiriJasmani'] = $id;
					$this->render_view('ciriCiriJasmaniManajemen',$params);
					break;
				case 'saveEdit':
					$allData = $this->input->post();
					$dataUpdate = array();
					foreach ($allData as $key => $value) {
						if($key!="idCiriCiriJasmani" && $value!="Belum Diset"){
							$dataUpdate[$key] = $value;
						}
					}
					$dataUpdate['stat_validasi'] = 0;
					$dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idCiriCiriJasmani'])->update('ciri_ciri_jasmani',$dataUpdate);

					$this->session->set_flashdata('message','Data Berhasil Diubah');
					redirect('/admin/contentManagement/ciriCiriJasmani');
					break;
				case 'saveNew':
					$allData = $this->input->post();
					$dataInsert = array();
					foreach ($allData as $key => $value) {
						if($key!="idCiriCiriJasmani" && $value!="Belum Diset"){
							$dataInsert[$key] = $value;
						}
					}
					$dataInsert['nip'] = $this->session->userdata('nip');
					$dataInsert['change_date'] = date("Y-m-d H:i:s");
		            $dataInsert['change_by'] = $this->session->userdata('username');

	                $this->db->insert('ciri_ciri_jasmani',$dataInsert);
					$this->session->set_flashdata('message','Data Berhasil Ditambah');
					redirect('/admin/contentManagement/ciriCiriJasmani');
					break;
				case 'delete':
					$this->db->where('id',$id)->delete('ciri_ciri_jasmani')->result();

					$this->session->set_flashdata('message',' 1 Data Berhasil Dihapus');
					redirect('/admin/contentManagement/ciriCiriJasmani');
					break;
			}
		}else{
			$this->errorPage();
		}
	}

	public function riwayatBidangKeahlianManajemen($type="get",$id=""){
		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'rwy_bidang_keahlian');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idRwyBidangKeahlian'],'rwy_bidang_keahlian');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'addNew':
					$nip = $this->session->userdata('nip');
					$params['title'] = "Riwayat Bidang Keahlian";
					$params['statEdit'] = false;
					$params['nip'] = $nip;
					$this->render_view('riwayatBidangKeahlianManajemen',$params);
					break;
				case 'edit':
					$params['title'] = "Riwayat Bidang Keahlian";
					$params['dataEdit'] = $this->db->where('id',$id)->get('rwy_bidang_keahlian')->result();
					$params['statEdit'] = true;
					$params['idRwyBidangKeahlian'] = $id;
					$this->render_view('riwayatBidangKeahlianManajemen',$params);
					break;
				case 'saveEdit':
					$allData = $this->input->post();
					$dataUpdate = array();
					foreach ($allData as $key => $value) {
						if($key!="idRwyBidangKeahlian" && $value!="Belum Diset"){
							$dataUpdate[$key] = $value;
						}
					}
					$dataUpdate['stat_validasi'] = 0;
					$dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idRwyBidangKeahlian'])->update('rwy_bidang_keahlian',$dataUpdate);

					$this->session->set_flashdata('message','Data Berhasil Diubah');
					redirect('/admin/contentManagement/riwayatBidangKeahlian');
					break;
				case 'saveNew':
					$allData = $this->input->post();
					$dataInsert = array();
					foreach ($allData as $key => $value) {
						if($key!="idRwyBidangKeahlian" && $value!="Belum Diset"){
							$dataInsert[$key] = $value;
						}
					}
					$dataInsert['nip'] = $this->session->userdata('nip');
					$dataInsert['change_date'] = date("Y-m-d H:i:s");
		            $dataInsert['change_by'] = $this->session->userdata('username');

	                $this->db->insert('rwy_bidang_keahlian',$dataInsert);
					$this->session->set_flashdata('message','Data Berhasil Ditambah');
					redirect('/admin/contentManagement/riwayatBidangKeahlian');
					break;
				case 'delete':
					$this->db->where('id',$id)->delete('rwy_bidang_keahlian')->result();

					$this->session->set_flashdata('message',' 1 Data Berhasil Dihapus');
					redirect('/admin/contentManagement/riwayatBidangKeahlian');
					break;
			}
		}else{
			$this->errorPage();
		}
	}

	public function relasiManajemen($type="get",$id=""){
		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'relasi');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idRelasi'],'relasi');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'addNew':
					$nip = $this->session->userdata('nip');
					$params['title'] = "Relasi";
					$params['statEdit'] = false;
					$params['nip'] = $nip;
					$this->render_view('relasiManajemen',$params);
					break;
				case 'edit':
					$params['title'] = "Relasi";
					$params['dataEdit'] = $this->db->where('id',$id)->get('relasi')->result();
					$params['statEdit'] = true;
					$params['idRelasi'] = $id;
					$this->render_view('relasiManajemen',$params);
					break;
				case 'saveEdit':
					$allData = $this->input->post();
					$dataUpdate = array();
					foreach ($allData as $key => $value) {
						if($key!="idRelasi" && $value!="Belum Diset"){
							$dataUpdate[$key] = $value;
						}
					}
					$dataUpdate['stat_validasi'] = 0;
					$dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idRelasi'])->update('relasi',$dataUpdate);

					$this->session->set_flashdata('message','Data Berhasil Diubah');
					redirect('/admin/contentManagement/relasi');
					break;
				case 'saveNew':
					$allData = $this->input->post();
					$dataInsert = array();
					foreach ($allData as $key => $value) {
						if($key!="idRelasi" && $value!="Belum Diset"){
							$dataInsert[$key] = $value;
						}
					}
					$dataInsert['nip'] = $this->session->userdata('nip');
					$dataInsert['change_date'] = date("Y-m-d H:i:s");
		            $dataInsert['change_by'] = $this->session->userdata('username');

	                $this->db->insert('relasi',$dataInsert);
					$this->session->set_flashdata('message','Data Berhasil Ditambah');
					redirect('/admin/contentManagement/relasi');
					break;
				case 'delete':
					$this->db->where('id',$id)->delete('relasi')->result();

					$this->session->set_flashdata('message',' 1 Data Berhasil Dihapus');
					redirect('/admin/contentManagement/relasi');
					break;
			}
		}else{
			$this->errorPage();
		}
	}

	public function riwayatJabatanManajemen($type="get",$id=""){
		$params['status_jabatan'] = $this->db->where('jenis_ref','status_jabatan')->get('referensi')->result_array();
		$params['status_rwy_jabatan'] = $this->db->where('jenis_ref','status_rwy_jabatan')->get('referensi')->result_array();
		$params['unit_organisasi'] = $this->db->get('unit_organisasi')->result_array();
		$params['unit_kerja'] = $this->db->get('unit_kerja')->result_array();
		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'rwy_jabatan');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idRwyJabatan'],'rwy_jabatan');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'addNew':
					$nip = $this->session->userdata('nip');
					$params['title'] = "Riwayat Jabatan";
					$params['statEdit'] = false;
					$params['nip'] = $nip;
					$this->render_view('riwayatJabatanManajemen',$params);
					break;
				case 'edit':
					$params['title'] = "Riwayat Jabatan";
					$params['dataEdit'] = $this->db->where('id',$id)->get('rwy_jabatan')->result();
					$params['statEdit'] = true;
					$params['idRwyJabatan'] = $id;
					$this->render_view('riwayatJabatanManajemen',$params);
					break;
				case 'saveEdit':
					$allData = $this->input->post();
					$dataUpdate = array();
					foreach ($allData as $key => $value) {
						if($key!="idRwyJabatan" && $value!="Belum Diset"){
							$dataUpdate[$key] = $value;
						}
					}
					$dataUpdate['stat_validasi'] = 0;
					$dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idRwyJabatan'])->update('rwy_jabatan',$dataUpdate);

					$this->session->set_flashdata('message','Data Berhasil Diubah');
					redirect('/admin/contentManagement/riwayatJabatan');
					break;
				case 'saveNew':
					$allData = $this->input->post();
					$dataInsert = array();
					foreach ($allData as $key => $value) {
						if($key!="idRwyJabatan" && $value!="Belum Diset"){
							$dataInsert[$key] = $value;
						}
					}
					$dataInsert['nip'] = $this->session->userdata('nip');
					$dataInsert['change_date'] = date("Y-m-d H:i:s");
		            $dataInsert['change_by'] = $this->session->userdata('username');

	                $this->db->insert('rwy_jabatan',$dataInsert);
					$this->session->set_flashdata('message','Data Berhasil Ditambah');
					redirect('/admin/contentManagement/riwayatJabatan');
					break;
				case 'delete':
					$this->db->where('id',$id)->delete('rwy_jabatan')->result();

					$this->session->set_flashdata('message',' 1 Data Berhasil Dihapus');
					redirect('/admin/contentManagement/riwayatJabatan');
					break;
			}
		}else{
			$this->errorPage();
		}
	}

	public function riwayatSahamManajemen($type="get",$id=""){
		$params['status_jabatan'] = $this->db->where('jenis_ref','status_jabatan')->get('referensi')->result_array();
		$params['status_rwy_jabatan'] = $this->db->where('jenis_ref','status_rwy_jabatan')->get('referensi')->result_array();
		$params['unit_organisasi'] = $this->db->get('unit_organisasi')->result_array();
		$params['unit_kerja'] = $this->db->get('unit_kerja')->result_array();
		$params['bulan'] = array('1' =>'Januari',
								 '2'=>'Februari',
								 '3'=>'Maret',
								 '4'=>'April',
								 '5'=>'Mei',
								 '6'=>'Juni',
								 '7'=>'Juli',
								 '8'=>'Agustus',
								 '9'=>'September',
								 '10'=>'Oktober',
								 '11'=>'November',
								 '12'=>'Desember' );
		$ret = true;
		if ($type=="edit" || $type=="saveEdit") {
			if ($type=="edit") {
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($id,'rwy_persediaan_saham_pegawai');
			}else{
				$allData = $this->input->post();
				$ret = $this->checkPriviledge($allData['idRwyJabatan'],'rwy_persediaan_saham_pegawai');
			}
		}

		if ($ret) {
			switch ($type) {
				case 'addNew':
					$nip = $this->session->userdata('nip');
					$params['title'] = "Riwayat Jabatan";
					$params['statEdit'] = false;
					$params['nip'] = $nip;
					$this->render_view('riwayatSahamManajemen',$params);
					break;
				case 'edit':
					$params['title'] = "Riwayat Jabatan";
					$params['dataEdit'] = $this->db->select("u.`nomor_anggota`,
														  	 u.`nama_pegawai`,
														  	 rph.* ")
												   ->from('rwy_persediaan_saham_pegawai as rph')
												   ->join('users as u','u.nip = rph.nip')
												   ->where('rph.id',$id)
												   ->get()->result();
					$params['statEdit'] = true;
					$params['idRwyJabatan'] = $id;
					$this->render_view('riwayatSahamManajemen',$params);
					break;
				case 'saveEdit':
					$allData = $this->input->post();
					$dataUpdate = array();
					foreach ($allData as $key => $value) {
						if($key!="idRwyJabatan" && $value!="Belum Diset" && $key!="no_add"){
							$dataUpdate[$key] = $value;
						}
					}
					//$dataUpdate['stat_validasi'] = 0;
					$dataUpdate['change_date'] = date("Y-m-d H:i:s");
		            $dataUpdate['change_by'] = $this->session->userdata('username');
					$this->db->where('id',$allData['idRwyJabatan'])->update('rwy_persediaan_saham_pegawai',$dataUpdate);

					$this->session->set_flashdata('message','Data Berhasil Diubah');
					redirect('/admin/contentManagement/rwySahamPegawai');
					break;
				case 'saveNew':
					$allData = $this->input->post();
					$dataInsert = array();
					foreach ($allData as $key => $value) {
						if($key!="idRwyJabatan" && $value!="Belum Diset" && $key!="no_add"){
							$dataInsert[$key] = $value;
						}
					}
					$dataInsert['nip'] = $this->session->userdata('nip');
					$dataInsert['change_date'] = date("Y-m-d H:i:s");
		            $dataInsert['change_by'] = $this->session->userdata('username');

	                $this->db->insert('rwy_persediaan_saham_pegawai',$dataInsert);
					$this->session->set_flashdata('message','Data Berhasil Ditambah');
					redirect('/admin/contentManagement/rwySahamPegawai');
					break;
				case 'delete':
					$this->db->where('id',$id)->delete('rwy_persediaan_saham_pegawai')->result();

					$this->session->set_flashdata('message',' 1 Data Berhasil Dihapus');
					redirect('/admin/contentManagement/rwySahamPegawai');
					break;
			}
		}else{
			$this->errorPage();
		}
	}
}
