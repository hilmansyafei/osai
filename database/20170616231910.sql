-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: osai
-- ------------------------------------------------------
-- Server version	5.5.53-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `log_id` int(11) NOT NULL,
  `action` varchar(20) DEFAULT NULL,
  `result` varchar(250) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `log_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,NULL,NULL,NULL,'2017-06-15 04:19:06'),(2,NULL,NULL,NULL,'2017-06-15 04:28:16'),(3,NULL,NULL,NULL,'2017-06-15 04:28:59'),(4,NULL,NULL,NULL,'2017-06-15 04:29:21');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('2j257p74mkqclc6jtqdbj92vkfgctb86','180.245.81.194',1496895826,'__ci_last_regenerate|i:1496895826;'),('vml9rkfhtfb7p28l8k7npl5lvompiqat','180.245.81.194',1496895898,'__ci_last_regenerate|i:1496895828;warning|s:16:\"Account is using\";__ci_vars|a:1:{s:7:\"warning\";s:3:\"old\";}'),('co7e2nnn1qc22urgdguvn3ldh9dctl3m','180.245.81.194',1496910911,'__ci_last_regenerate|i:1496910911;'),('04qqn366msoqpitak87ircgombuumngd','37.9.113.38',1497120581,'__ci_last_regenerate|i:1497120581;'),('jhm9rh0rmn0clftg5lhhopq8ut548arv','178.154.189.201',1497181144,'__ci_last_regenerate|i:1497181144;'),('ucdl92oitlnuv7oi3bbbc6cd12idmu5g','104.131.29.255',1497209912,'__ci_last_regenerate|i:1497209912;'),('2213j93nqe5cbenl7qm54iicp4ruc34a','5.9.98.130',1497218195,'__ci_last_regenerate|i:1497218195;'),('j96anp9h7rnqo0aa0mf54v2f15v1op4n','5.255.253.61',1497246684,'__ci_last_regenerate|i:1497246684;'),('1l9u3r6r5ngoo6mi886oh6pjn3uodm7t','182.253.178.36',1497319700,'__ci_last_regenerate|i:1497319700;'),('5ug4gimgtssesubbito79igmekut3lse','66.249.79.136',1497383867,'__ci_last_regenerate|i:1497383867;'),('ukh33fsfkjsqni90s8ice325e1lqjomk','95.108.213.18',1497479899,'__ci_last_regenerate|i:1497479899;'),('pq3s4v6c9j6gdt878et3cfplad08dvi5','110.138.153.179',1497484642,'__ci_last_regenerate|i:1497484642;'),('6fhgn4vm3nlkv8764f3b1mmqi185jse6','110.138.153.179',1497484726,'__ci_last_regenerate|i:1497484726;'),('9b0tg2bv7b0s5v49attq9tmj974vhodd','110.138.153.179',1497501039,'__ci_last_regenerate|i:1497501039;'),('gpftf2tfmat6h9ihebf25dmvg70ujqo7','110.138.153.179',1497510008,'__ci_last_regenerate|i:1497510008;'),('ssgbv4668er4hugeai9j9o11circ78cn','114.124.213.21',1497614980,'__ci_last_regenerate|i:1497614979;'),('5dqsjkicgu2v6kt30pn9luv7g4nlfnqd','114.124.233.4',1497615012,'__ci_last_regenerate|i:1497614995;id|s:1:\"1\";username|s:6:\"hilman\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('aj7gojv8jmprfk4hahba3ro18rnif3jm','114.124.236.149',1497616515,'__ci_last_regenerate|i:1497616218;id|s:1:\"1\";username|s:6:\"hilman\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('5tcr6fa6tc86a2rutekv46q3ibv7vich','114.124.215.165',1497616695,'__ci_last_regenerate|i:1497616538;id|s:1:\"1\";username|s:6:\"hilman\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('aid6tjms4kuhd4j9i2i6hla4kut7k0a0','114.124.236.21',1497617282,'__ci_last_regenerate|i:1497617282;'),('pin562j0k26oua9h0e0h35agi3jfjilv','114.124.215.165',1497617888,'__ci_last_regenerate|i:1497617886;');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_ads`
--

DROP TABLE IF EXISTS `eq_ads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_ads` (
  `ads_id` int(5) NOT NULL AUTO_INCREMENT,
  `position` varchar(20) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `created_by` varchar(30) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  PRIMARY KEY (`ads_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_ads`
--

LOCK TABLES `eq_ads` WRITE;
/*!40000 ALTER TABLE `eq_ads` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_ads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_comp`
--

DROP TABLE IF EXISTS `eq_comp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_comp` (
  `comp_id` int(5) NOT NULL AUTO_INCREMENT,
  `user_id` int(5) DEFAULT NULL,
  `title` varchar(30) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_by` varchar(39) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`comp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_comp`
--

LOCK TABLES `eq_comp` WRITE;
/*!40000 ALTER TABLE `eq_comp` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_comp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_comp_item`
--

DROP TABLE IF EXISTS `eq_comp_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_comp_item` (
  `item_id` int(5) NOT NULL AUTO_INCREMENT,
  `member_id` int(5) DEFAULT NULL,
  `comp_id` int(5) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `desc` text,
  `image` varchar(100) DEFAULT NULL,
  `content` text,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_comp_item`
--

LOCK TABLES `eq_comp_item` WRITE;
/*!40000 ALTER TABLE `eq_comp_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_comp_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_member`
--

DROP TABLE IF EXISTS `eq_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_member` (
  `member_id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `genre` varchar(5) DEFAULT NULL,
  `identity_num` int(20) DEFAULT NULL,
  `address` text,
  `email_address` varchar(40) DEFAULT NULL,
  `phone_number` int(20) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `facebook_account` varchar(20) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_member`
--

LOCK TABLES `eq_member` WRITE;
/*!40000 ALTER TABLE `eq_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_slider`
--

DROP TABLE IF EXISTS `eq_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_slider` (
  `slider_id` int(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`slider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_slider`
--

LOCK TABLES `eq_slider` WRITE;
/*!40000 ALTER TABLE `eq_slider` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_visitor`
--

DROP TABLE IF EXISTS `eq_visitor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_visitor` (
  `visitor_id` int(5) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(32) DEFAULT NULL,
  `media` varchar(50) DEFAULT NULL,
  `location` varchar(1000) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`visitor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_visitor`
--

LOCK TABLES `eq_visitor` WRITE;
/*!40000 ALTER TABLE `eq_visitor` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_visitor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eq_votes`
--

DROP TABLE IF EXISTS `eq_votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eq_votes` (
  `vote_id` int(5) NOT NULL AUTO_INCREMENT,
  `item_id` int(5) DEFAULT NULL,
  `member_id` int(5) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eq_votes`
--

LOCK TABLES `eq_votes` WRITE;
/*!40000 ALTER TABLE `eq_votes` DISABLE KEYS */;
/*!40000 ALTER TABLE `eq_votes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_priviledge`
--

DROP TABLE IF EXISTS `group_priviledge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_priviledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_group` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `id_submenu` int(11) DEFAULT NULL,
  `access_grant` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inx` (`id_user_group`,`id_menu`,`id_submenu`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_priviledge`
--

LOCK TABLES `group_priviledge` WRITE;
/*!40000 ALTER TABLE `group_priviledge` DISABLE KEYS */;
INSERT INTO `group_priviledge` VALUES (167,7,1,1,1,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(168,7,1,1,2,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(169,7,1,1,4,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(170,7,1,2,1,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(171,7,1,2,2,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(172,7,1,2,4,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(173,7,1,3,1,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(174,7,1,3,2,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(175,7,1,3,4,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(182,1,26,42,1,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(183,1,26,42,2,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(184,1,26,42,4,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(185,1,1,1,1,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(186,1,1,1,2,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(187,1,1,1,4,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(188,1,1,2,1,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(189,1,1,2,2,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(190,1,1,2,4,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(197,8,26,43,1,'2017-05-19 04:48:16','2017-05-19 04:48:16','hilman'),(198,8,26,43,2,'2017-05-19 04:48:16','2017-05-19 04:48:16','hilman'),(199,8,26,42,1,'2017-05-19 04:48:16','2017-05-19 04:48:16','hilman'),(200,8,26,42,2,'2017-05-19 04:48:16','2017-05-19 04:48:16','hilman');
/*!40000 ALTER TABLE `group_priviledge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(100) NOT NULL,
  `sort_position` int(4) NOT NULL DEFAULT '0',
  `total_submenu` int(2) NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `access_group` int(11) DEFAULT NULL,
  `image` text,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx` (`sort_position`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Settings',3,3,'settings',4,'fa fa-cube','2017-03-29 13:28:44','2017-05-12 07:45:44','hilman');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_menu`
--

DROP TABLE IF EXISTS `sub_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(4) DEFAULT NULL,
  `submenu_name` varchar(100) NOT NULL,
  `sort_position` int(11) NOT NULL,
  `route_submenu` text NOT NULL,
  `content` text,
  `image` text,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inx` (`id_menu`,`sort_position`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_menu`
--

LOCK TABLES `sub_menu` WRITE;
/*!40000 ALTER TABLE `sub_menu` DISABLE KEYS */;
INSERT INTO `sub_menu` VALUES (1,1,'User Management',1,'userManagement','settings/userManagement',NULL,'2017-03-29 13:32:17','2017-06-16 19:42:34','hilman'),(2,1,'Group Management',0,'groupManagement','settings/groupManagement',NULL,'2017-04-10 10:37:28','2017-05-18 06:34:31','hilman'),(3,1,'Menu Management',0,'menuManagement','settings/menuManagement',NULL,'2017-04-10 10:37:28','2017-06-16 19:42:34','hilman');
/*!40000 ALTER TABLE `sub_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (1,'Administrator','2017-03-29 11:18:57','2017-04-10 11:03:55','hilman'),(7,'Developer','2017-04-19 11:39:11','2017-04-19 11:40:07','hilman'),(8,'PIC','2017-05-19 04:47:56','2017-05-19 04:47:56','hilman');
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_priviledge`
--

DROP TABLE IF EXISTS `user_priviledge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_priviledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_submenu` int(11) NOT NULL,
  `access_grant` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`,`id_user`,`id_menu`,`id_submenu`,`change_by`),
  KEY `idx` (`id_user`,`id_menu`,`id_submenu`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=1281 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_priviledge`
--

LOCK TABLES `user_priviledge` WRITE;
/*!40000 ALTER TABLE `user_priviledge` DISABLE KEYS */;
INSERT INTO `user_priviledge` VALUES (1230,1,26,42,1,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1231,1,26,42,2,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1232,1,26,42,4,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1233,1,26,43,1,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1234,1,26,43,2,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1235,1,26,43,4,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1236,1,1,1,1,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1237,1,1,1,2,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1238,1,1,1,4,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1239,1,1,2,1,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1240,1,1,2,2,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1241,1,1,2,4,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1242,1,1,3,1,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1243,1,1,3,2,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1244,1,1,3,4,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1275,9,1,1,1,'2017-06-15 06:58:27','2017-06-15 06:58:27','hilman'),(1276,9,1,1,2,'2017-06-15 06:58:27','2017-06-15 06:58:27','hilman'),(1277,9,1,1,4,'2017-06-15 06:58:27','2017-06-15 06:58:27','hilman'),(1278,9,1,2,1,'2017-06-15 06:58:27','2017-06-15 06:58:27','hilman'),(1279,9,1,2,2,'2017-06-15 06:58:27','2017-06-15 06:58:27','hilman'),(1280,9,1,2,4,'2017-06-15 06:58:27','2017-06-15 06:58:27','hilman');
/*!40000 ALTER TABLE `user_priviledge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_group` int(11) NOT NULL,
  `foto` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NULL DEFAULT NULL,
  `change_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'hilman','hilman','6fd4829c2ae25048d24bde25a6759a985bf2b1d44d7c0e5ce7dceb324cba7d93eb6c364319b9c3530775e799b619f360730e23b8756103cc276b78a4bc6751c5',7,'20170311084801.png',1,NULL,'2017-03-30 07:50:13','2017-06-15 06:58:39','hilman'),(9,'Fitrah Ramadhan','fitrah','458d1d516f82c568062a1e54fb8f8d2c7a6de121faefa497d04a03b4e0ac7abf42522d58a6bfbe8e64d0fa0a601f608515f87e45865f0f00f1540161d024a125',7,NULL,1,NULL,'2017-06-15 06:58:18','2017-06-16 19:38:15','hilman');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-16 23:19:10
