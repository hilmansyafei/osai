/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.0.31-MariaDB : Database - hilanpro_equity
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `activity_log` */

DROP TABLE IF EXISTS `activity_log`;

CREATE TABLE `activity_log` (
  `log_id` int(11) NOT NULL,
  `action` varchar(20) DEFAULT NULL,
  `result` varchar(250) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `log_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `activity_log` */

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`id`,`ip_address`,`timestamp`,`data`) values ('2j257p74mkqclc6jtqdbj92vkfgctb86','180.245.81.194',1496895826,'__ci_last_regenerate|i:1496895826;'),('vml9rkfhtfb7p28l8k7npl5lvompiqat','180.245.81.194',1496895898,'__ci_last_regenerate|i:1496895828;warning|s:16:\"Account is using\";__ci_vars|a:1:{s:7:\"warning\";s:3:\"old\";}'),('co7e2nnn1qc22urgdguvn3ldh9dctl3m','180.245.81.194',1496910911,'__ci_last_regenerate|i:1496910911;'),('04qqn366msoqpitak87ircgombuumngd','37.9.113.38',1497120581,'__ci_last_regenerate|i:1497120581;'),('jhm9rh0rmn0clftg5lhhopq8ut548arv','178.154.189.201',1497181144,'__ci_last_regenerate|i:1497181144;'),('ucdl92oitlnuv7oi3bbbc6cd12idmu5g','104.131.29.255',1497209912,'__ci_last_regenerate|i:1497209912;'),('2213j93nqe5cbenl7qm54iicp4ruc34a','5.9.98.130',1497218195,'__ci_last_regenerate|i:1497218195;'),('j96anp9h7rnqo0aa0mf54v2f15v1op4n','5.255.253.61',1497246684,'__ci_last_regenerate|i:1497246684;'),('1l9u3r6r5ngoo6mi886oh6pjn3uodm7t','182.253.178.36',1497319700,'__ci_last_regenerate|i:1497319700;'),('5ug4gimgtssesubbito79igmekut3lse','66.249.79.136',1497383867,'__ci_last_regenerate|i:1497383867;'),('ukh33fsfkjsqni90s8ice325e1lqjomk','95.108.213.18',1497479899,'__ci_last_regenerate|i:1497479899;');

/*Table structure for table `eq_ads` */

DROP TABLE IF EXISTS `eq_ads`;

CREATE TABLE `eq_ads` (
  `ads_id` int(5) NOT NULL AUTO_INCREMENT,
  `position` varchar(20) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `created_by` varchar(30) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  PRIMARY KEY (`ads_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `eq_ads` */

/*Table structure for table `eq_comp` */

DROP TABLE IF EXISTS `eq_comp`;

CREATE TABLE `eq_comp` (
  `comp_id` int(5) NOT NULL AUTO_INCREMENT,
  `user_id` int(5) DEFAULT NULL,
  `title` varchar(30) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_by` varchar(39) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`comp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `eq_comp` */

/*Table structure for table `eq_comp_item` */

DROP TABLE IF EXISTS `eq_comp_item`;

CREATE TABLE `eq_comp_item` (
  `item_id` int(5) NOT NULL AUTO_INCREMENT,
  `member_id` int(5) DEFAULT NULL,
  `comp_id` int(5) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `desc` text,
  `image` varchar(100) DEFAULT NULL,
  `content` text,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `eq_comp_item` */

/*Table structure for table `eq_member` */

DROP TABLE IF EXISTS `eq_member`;

CREATE TABLE `eq_member` (
  `member_id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `genre` varchar(5) DEFAULT NULL,
  `identity_num` int(20) DEFAULT NULL,
  `address` text,
  `email_address` varchar(40) DEFAULT NULL,
  `phone_number` int(20) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `facebook_account` varchar(20) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `eq_member` */

/*Table structure for table `eq_slider` */

DROP TABLE IF EXISTS `eq_slider`;

CREATE TABLE `eq_slider` (
  `slider_id` int(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`slider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `eq_slider` */

/*Table structure for table `eq_visitor` */

DROP TABLE IF EXISTS `eq_visitor`;

CREATE TABLE `eq_visitor` (
  `visitor_id` int(5) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(32) DEFAULT NULL,
  `media` varchar(50) DEFAULT NULL,
  `location` varchar(1000) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`visitor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `eq_visitor` */

/*Table structure for table `eq_votes` */

DROP TABLE IF EXISTS `eq_votes`;

CREATE TABLE `eq_votes` (
  `vote_id` int(5) NOT NULL AUTO_INCREMENT,
  `item_id` int(5) DEFAULT NULL,
  `member_id` int(5) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `eq_votes` */

/*Table structure for table `group_priviledge` */

DROP TABLE IF EXISTS `group_priviledge`;

CREATE TABLE `group_priviledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_group` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `id_submenu` int(11) DEFAULT NULL,
  `access_grant` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inx` (`id_user_group`,`id_menu`,`id_submenu`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;

/*Data for the table `group_priviledge` */

insert  into `group_priviledge`(`id`,`id_user_group`,`id_menu`,`id_submenu`,`access_grant`,`create_date`,`change_date`,`change_by`) values (167,7,1,1,1,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(168,7,1,1,2,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(169,7,1,1,4,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(170,7,1,2,1,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(171,7,1,2,2,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(172,7,1,2,4,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(173,7,1,3,1,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(174,7,1,3,2,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(175,7,1,3,4,'2017-05-12 00:35:26','2017-05-12 00:35:26','hilman'),(182,1,26,42,1,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(183,1,26,42,2,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(184,1,26,42,4,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(185,1,1,1,1,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(186,1,1,1,2,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(187,1,1,1,4,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(188,1,1,2,1,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(189,1,1,2,2,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(190,1,1,2,4,'2017-05-12 07:46:51','2017-05-12 07:46:51','hilman'),(197,8,26,43,1,'2017-05-19 04:48:16','2017-05-19 04:48:16','hilman'),(198,8,26,43,2,'2017-05-19 04:48:16','2017-05-19 04:48:16','hilman'),(199,8,26,42,1,'2017-05-19 04:48:16','2017-05-19 04:48:16','hilman'),(200,8,26,42,2,'2017-05-19 04:48:16','2017-05-19 04:48:16','hilman');

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(100) NOT NULL,
  `sort_position` int(4) NOT NULL DEFAULT '0',
  `total_submenu` int(2) NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `access_group` int(11) DEFAULT NULL,
  `image` text,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx` (`sort_position`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`id`,`menu_name`,`sort_position`,`total_submenu`,`content`,`access_group`,`image`,`create_date`,`change_date`,`change_by`) values (1,'Settings',3,3,'settings',4,'fa fa-cube','2017-03-29 13:28:44','2017-05-12 07:45:44','hilman');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `sub_menu` */

DROP TABLE IF EXISTS `sub_menu`;

CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(4) DEFAULT NULL,
  `submenu_name` varchar(100) NOT NULL,
  `sort_position` int(11) NOT NULL,
  `route_submenu` text NOT NULL,
  `content` text,
  `image` text,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inx` (`id_menu`,`sort_position`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

/*Data for the table `sub_menu` */

insert  into `sub_menu`(`id`,`id_menu`,`submenu_name`,`sort_position`,`route_submenu`,`content`,`image`,`create_date`,`change_date`,`change_by`) values (1,1,'User Management',0,'userManagement','settings/userManagement',NULL,'2017-03-29 13:32:17','2017-04-11 15:31:51','hilman'),(2,1,'Group Management',0,'groupManagement','settings/groupManagement',NULL,'2017-04-10 10:37:28','2017-05-18 06:34:31','hilman'),(3,1,'Menu Management',1,'menuManagement','settings/menuManagement',NULL,'2017-04-10 10:37:28','2017-05-18 06:34:33','hilman');

/*Table structure for table `user_group` */

DROP TABLE IF EXISTS `user_group`;

CREATE TABLE `user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `user_group` */

insert  into `user_group`(`id`,`group_name`,`create_date`,`change_date`,`change_by`) values (1,'Administrator','2017-03-29 11:18:57','2017-04-10 11:03:55','hilman'),(7,'Developer','2017-04-19 11:39:11','2017-04-19 11:40:07','hilman'),(8,'PIC','2017-05-19 04:47:56','2017-05-19 04:47:56','hilman');

/*Table structure for table `user_priviledge` */

DROP TABLE IF EXISTS `user_priviledge`;

CREATE TABLE `user_priviledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_submenu` int(11) NOT NULL,
  `access_grant` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`,`id_user`,`id_menu`,`id_submenu`,`change_by`),
  KEY `idx` (`id_user`,`id_menu`,`id_submenu`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=1266 DEFAULT CHARSET=latin1;

/*Data for the table `user_priviledge` */

insert  into `user_priviledge`(`id`,`id_user`,`id_menu`,`id_submenu`,`access_grant`,`create_date`,`change_date`,`change_by`) values (1230,1,26,42,1,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1231,1,26,42,2,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1232,1,26,42,4,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1233,1,26,43,1,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1234,1,26,43,2,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1235,1,26,43,4,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1236,1,1,1,1,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1237,1,1,1,2,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1238,1,1,1,4,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1239,1,1,2,1,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1240,1,1,2,2,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1241,1,1,2,4,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1242,1,1,3,1,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1243,1,1,3,2,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1244,1,1,3,4,'2017-05-18 00:00:57','2017-05-18 00:00:57','hilman'),(1257,8,1,1,1,'2017-06-12 14:36:57','2017-06-12 14:36:57','hilman'),(1258,8,1,1,2,'2017-06-12 14:36:57','2017-06-12 14:36:57','hilman'),(1259,8,1,1,4,'2017-06-12 14:36:57','2017-06-12 14:36:57','hilman'),(1260,8,1,2,1,'2017-06-12 14:36:57','2017-06-12 14:36:57','hilman'),(1261,8,1,2,2,'2017-06-12 14:36:57','2017-06-12 14:36:57','hilman'),(1262,8,1,2,4,'2017-06-12 14:36:57','2017-06-12 14:36:57','hilman'),(1263,8,1,3,1,'2017-06-12 14:36:57','2017-06-12 14:36:57','hilman'),(1264,8,1,3,2,'2017-06-12 14:36:57','2017-06-12 14:36:57','hilman'),(1265,8,1,3,4,'2017-06-12 14:36:57','2017-06-12 14:36:57','hilman');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_group` int(11) NOT NULL,
  `foto` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NULL DEFAULT NULL,
  `change_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`user_group`,`foto`,`status`,`remember_token`,`create_date`,`change_date`,`change_by`) values (1,'hilman','hilman','6fd4829c2ae25048d24bde25a6759a985bf2b1d44d7c0e5ce7dceb324cba7d93eb6c364319b9c3530775e799b619f360730e23b8756103cc276b78a4bc6751c5',7,'20170311084801.png',1,NULL,'2017-03-30 07:50:13','2017-05-12 00:35:46','hilman'),(8,'andrey','andrey','144e97a8a8ba8c4867b1fb2ff2f467242395fef8d583e0d3d5b2be3faeed76968f36c03ab450c9df3979001f2acb4091d7ed7de78adf715098c45164f7341bf1',7,NULL,0,NULL,'2017-06-12 14:36:57','2017-06-12 14:38:10','hilman');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
