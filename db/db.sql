-- MySQL dump 10.13  Distrib 5.5.53, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: osai
-- ------------------------------------------------------
-- Server version	5.5.53-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `log_id` int(11) NOT NULL,
  `action` varchar(20) DEFAULT NULL,
  `result` varchar(250) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `log_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,NULL,NULL,NULL,'2017-06-15 04:19:06'),(2,NULL,NULL,NULL,'2017-06-15 04:28:16'),(3,NULL,NULL,NULL,'2017-06-15 04:28:59'),(4,NULL,NULL,NULL,'2017-06-15 04:29:21');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank_accounts`
--

DROP TABLE IF EXISTS `bank_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` int(11) NOT NULL,
  `account_name` varchar(255) NOT NULL DEFAULT '',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_number` varchar(30) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_accounts`
--

LOCK TABLES `bank_accounts` WRITE;
/*!40000 ALTER TABLE `bank_accounts` DISABLE KEYS */;
INSERT INTO `bank_accounts` VALUES (1,1,'Santoso','BCA','1234567890','2017-11-12 21:30:00','2017-11-12 21:30:00','2017-11-12 21:30:00'),(2,1,'Indra','BNI','991122336677','2017-11-12 21:30:00','2017-11-12 21:30:00','2017-11-12 21:30:00');
/*!40000 ALTER TABLE `bank_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cashier`
--

DROP TABLE IF EXISTS `cashier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cashier` (
  `cashier_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `merchant_id` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `create_by` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cashier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cashier`
--

LOCK TABLES `cashier` WRITE;
/*!40000 ALTER TABLE `cashier` DISABLE KEYS */;
/*!40000 ALTER TABLE `cashier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('1uitqmjsij58peh9q6bonibaffdrrgd1','61.94.176.7',1505357060,'__ci_last_regenerate|i:1505357060;'),('37bm45hmfrcsea8p5k975lh3i2lc670o','202.125.95.18',1506318639,'__ci_last_regenerate|i:1506318639;'),('8nrvdhsl7d66g9c2oqm38ophshcu9f0v','202.125.95.18',1506319394,'__ci_last_regenerate|i:1506319394;'),('ghvnjqjd1brliqlfgnf7bk1icvmis0f6','223.255.229.30',1506498982,'__ci_last_regenerate|i:1506498690;'),('f3vvdsnitms5uj1m48c69vtatc33p8if','223.255.229.30',1506498703,'__ci_last_regenerate|i:1506498703;'),('3cl5a7ckstn2e4cmdogr5ah8dmkosn9q','112.215.235.185',1506508042,'__ci_last_regenerate|i:1506508042;'),('7dnc5pcs59r3hc8vplgdrrsphf5rl4ka','112.215.235.185',1506508049,'__ci_last_regenerate|i:1506508045;'),('cgkbuea8vanjefhsihkq7idasf920v3i','202.62.18.126',1506572571,'__ci_last_regenerate|i:1506572571;'),('9vta48pl30n7c2oln6tl4e5emkgmeqpg','180.251.214.190',1506581772,'__ci_last_regenerate|i:1506581772;'),('o4h8bc80d2mv3m1np8aoi3bffhm309pn','36.69.67.209',1506695653,'__ci_last_regenerate|i:1506695653;'),('1nsssickt1l9nnmgb30f5j93cijfkm2k','36.71.210.119',1506955413,'__ci_last_regenerate|i:1506955413;'),('nd03dln6b6rdusdf12j92s4cfmt4puf0','139.194.145.163',1507120269,'__ci_last_regenerate|i:1507120150;'),('cblcbma5je2qh4n47t7uqhmvtml5u8jk','66.102.6.166',1507214582,'__ci_last_regenerate|i:1507214582;'),('ep3o4m34i8uf8aibafmuka1g2ck4udpv','36.71.210.119',1507251717,'__ci_last_regenerate|i:1507251717;'),('8l9t7jl3ef581rp3h8k1oi4e2086tq0i','114.124.232.208',1507308172,'__ci_last_regenerate|i:1507307936;id|s:1:\"9\";username|s:6:\"fitrah\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:15:\"Fitrah Ramadhan\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"d894f2c6e1d2ba6b271b483676894c84\";'),('eduqu9o31116r3peh676ffdecllhqhvo','114.124.231.225',1507308285,'__ci_last_regenerate|i:1507308246;id|s:1:\"9\";username|s:6:\"fitrah\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:15:\"Fitrah Ramadhan\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"d894f2c6e1d2ba6b271b483676894c84\";'),('f5732462ihldndsi14s56o2l4n2ujv9e','125.166.155.226',1507312492,'__ci_last_regenerate|i:1507312492;'),('lt85fppcmb1evd87e383k984pgf8qng2','64.233.172.165',1507345960,'__ci_last_regenerate|i:1507345960;'),('r67k04mn0eaqai5pd6fn6kum982niqbj','64.233.172.165',1507522390,'__ci_last_regenerate|i:1507522389;'),('4esr5v5n5laqlmkg7b71msatde293i57','64.233.172.162',1507522390,'__ci_last_regenerate|i:1507522389;'),('learvlb5vufmo656gcdujuja325thg8e','202.125.95.18',1507523187,'__ci_last_regenerate|i:1507523187;'),('scjd0aq7jpsvu8b9g14l43p8hqunnaqk','202.125.95.18',1507530849,'__ci_last_regenerate|i:1507530849;'),('0ltq94mrq3eia1jigvf126b4factne38','202.125.95.18',1507532064,'__ci_last_regenerate|i:1507532064;'),('80oa13r9m9746ldpkh6ir1vid95eu3ss','202.125.95.18',1507532586,'__ci_last_regenerate|i:1507532452;'),('15jpj57bao0m6o5hrc0vjf8lfm8mjlom','202.125.95.18',1507533318,'__ci_last_regenerate|i:1507533172;'),('55bhm7ri66qpo5d80l53rdgqvagk3vto','66.102.6.101',1507593903,'__ci_last_regenerate|i:1507593903;'),('odtn2vhcegsiqnod0g8qk3rbcoqoq6lj','110.138.155.149',1507684560,'__ci_last_regenerate|i:1507684323;'),('ubfm56pdn41qernt9gbkqd8h7mq5o050','110.138.155.149',1507684680,'__ci_last_regenerate|i:1507684680;'),('efkl7ro5fp0hl6hhegps0cd7ii1ggv3f','66.102.6.226',1507684766,'__ci_last_regenerate|i:1507684766;'),('qui41473jmiccj5vnthc255jkjvstqrh','110.138.155.149',1507685608,'__ci_last_regenerate|i:1507685608;'),('3olkauisog9pspmv8er177bb4u8q52ep','202.125.95.18',1507704554,'__ci_last_regenerate|i:1507704276;'),('5ca715pkgmphgkg3saemhogct7vunkl6','114.124.168.33',1507721931,'__ci_last_regenerate|i:1507721931;'),('rgp1m20ou05egsgj3bfqn5ekf08q7109','114.124.168.33',1507722804,'__ci_last_regenerate|i:1507722804;'),('tru4em51c5g74rl09nmo1gu6mjpaq7ec','110.138.155.149',1507739772,'__ci_last_regenerate|i:1507739772;'),('hcf79q2euaok38mguceu1kruimlomgdl','66.102.6.229',1507868204,'__ci_last_regenerate|i:1507868204;'),('asu86elgkbjfmapf0dvvcrh0bgon9k10','66.102.6.227',1507937173,'__ci_last_regenerate|i:1507937173;'),('kj9pil2qudojiae7g5c6i828un9o4qkg','110.138.155.149',1507951140,'__ci_last_regenerate|i:1507951107;'),('r7qgfed654mmk5pdjoa9tmnonu905vac','110.138.155.149',1507954397,'__ci_last_regenerate|i:1507954397;'),('ii3l6h8fvi1kn1eg14pjiio58bbvocrv','110.138.155.149',1507962733,'__ci_last_regenerate|i:1507962733;'),('srodk605eeca1eo98rmk22cnqavrg6dd','66.102.6.227',1508126335,'__ci_last_regenerate|i:1508126335;'),('ehgek11f68h9oi0cmgrdeeo92r9u5kr6','66.102.6.226',1508203178,'__ci_last_regenerate|i:1508203178;'),('fhl413gtej62l76u1csat76dbje3dg9r','202.125.95.18',1508310172,'__ci_last_regenerate|i:1508310172;'),('v40j3h0ma9h4nlv41b2dp7rb17esph8v','180.252.205.49',1508544070,'__ci_last_regenerate|i:1508544070;'),('c2q1l059h1dvhpg3evuvd9mb38rcj855','66.102.6.226',1508774530,'__ci_last_regenerate|i:1508774530;'),('h2h4rfpmq4fg1usqe8tbpge6ki1b6hse','103.10.66.65',1508812135,'__ci_last_regenerate|i:1508812134;'),('i4vatptm8bp8ajte6hhgsdapiokd825a','66.102.6.227',1508882708,'__ci_last_regenerate|i:1508882708;'),('a4392j9kbtptkdetbjs12chi2v4g3p4q','203.142.87.234',1508897422,'__ci_last_regenerate|i:1508897422;'),('34b7bgmvs8ipfkqrb1qr6he4r2difkot','203.142.87.234',1508899521,'__ci_last_regenerate|i:1508899521;'),('2bd1fcv7jmadb6m8gde78locc2ovibs4','66.102.6.226',1508984490,'__ci_last_regenerate|i:1508984490;'),('ocjpf2p0qs32va9ehk4ujvcop1reshir','114.124.233.28',1509005273,'__ci_last_regenerate|i:1509005273;'),('3fbm5o3mprb5pjkj87u8amocops3782p','66.102.6.102',1509060755,'__ci_last_regenerate|i:1509060755;'),('74tfb789o12qvl24i0qth0l30q48368o','125.166.21.93',1509317976,'__ci_last_regenerate|i:1509317976;'),('bppkguigjnk6alu2rh1g3gs6v8u1ip5i','125.166.21.93',1509318251,'__ci_last_regenerate|i:1509318251;'),('v53k3up1s01ld62728l11paidmos2kou','112.215.201.128',1509318980,'__ci_last_regenerate|i:1509318980;'),('i9od8anestc0u4ohcmfvg1jf69fdfi2d','66.102.6.98',1509327120,'__ci_last_regenerate|i:1509327120;'),('n4bi1efbsomg7i3stfhb0fah267bn46i','66.102.6.102',1509400564,'__ci_last_regenerate|i:1509400564;'),('qhjq3qv88g3gka1f7n6kaqv9ku7lk451','36.84.129.247',1509404349,'__ci_last_regenerate|i:1509404349;'),('2r6absoji5gol1gdn0h8pr1s844gdvs1','66.102.6.101',1509421458,'__ci_last_regenerate|i:1509421458;'),('b1kfs78u9f45faduark54tc89kj9ad86','202.125.95.18',1509428604,'__ci_last_regenerate|i:1509428604;'),('91glqpfod5sf1rgnvi1jtjecvotrtvmg','66.102.6.102',1509488497,'__ci_last_regenerate|i:1509488497;'),('i1kc10flu7ga6rd3njuvi4gbttvten80','66.102.6.101',1509572523,'__ci_last_regenerate|i:1509572523;'),('e4mvj683ovbvb81hbhbi7v8gruo2bh4e','66.102.6.101',1509641146,'__ci_last_regenerate|i:1509641146;'),('gehcp749nij3vq7blahobqo9dr8j8t44','66.102.6.98',1509693530,'__ci_last_regenerate|i:1509693530;'),('unoss7o993c8e84p8cfe8uh1uk19tpu6','180.244.157.78',1509699532,'__ci_last_regenerate|i:1509699532;'),('0fuv4ve4j3fgrc05gie2p6ejg3q9sspi','180.244.157.78',1509699548,'__ci_last_regenerate|i:1509699548;'),('k631h3o6mnur393g1cbmoapm8m6jhut3','66.102.6.226',1509775634,'__ci_last_regenerate|i:1509775634;'),('4k2e6qpba1t7n11637681frnkb5lcutp','64.233.172.162',1509849690,'__ci_last_regenerate|i:1509849690;'),('kf2643uharp1kg0ji2u1jehvg6nm9c72','125.165.143.4',1509851090,'__ci_last_regenerate|i:1509851090;'),('qpf14n3hin3p5epk7dk8qr5ka95mqd2c','125.165.143.4',1509896492,'__ci_last_regenerate|i:1509896492;'),('vl7kc8ko607ej1bnm0hbkv58a8dktr0i','125.165.143.4',1509897826,'__ci_last_regenerate|i:1509897826;'),('8g5kui8evpp9usvph5dpuitkpukrn891','125.165.143.4',1509898639,'__ci_last_regenerate|i:1509898639;'),('fn0cm9tf146dhhil7vv320n7e3qqp8a5','125.165.143.4',1509927851,'__ci_last_regenerate|i:1509927851;'),('7stc9oro93gji5av47u4ukfbrsnh9jgh','66.102.6.229',1509931439,'__ci_last_regenerate|i:1509931439;'),('1u0m9oirn797ouekmnq156nmuproerga','125.165.143.4',1509933244,'__ci_last_regenerate|i:1509933215;'),('jrahsmo0p1bfvpd1he389278g7rhl00m','125.165.143.4',1509933747,'__ci_last_regenerate|i:1509933747;'),('olk0rcgmn3vbemm959s3o0hcvibpes7t','66.102.6.226',1510006258,'__ci_last_regenerate|i:1510006258;'),('05d3rr9ppmpqf1kjhootnt62s1t17000','36.86.56.97',1510072847,'__ci_last_regenerate|i:1510072847;'),('forgdqjdu7ap9aq99jko1vb266ot9qfj','36.86.56.97',1510072847,'__ci_last_regenerate|i:1510072847;'),('103otr2ipag3papcujoouoacqujcsree','66.102.6.44',1510109662,'__ci_last_regenerate|i:1510109662;'),('g0gg3gvnbjve7b0vg3jpeqt4otkan5od','202.125.95.18',1510123258,'__ci_last_regenerate|i:1510123257;'),('tv92un3tm44cb80jfkij0csht9fprtu6','66.102.6.3',1510184130,'__ci_last_regenerate|i:1510184130;'),('gpgrgasovsu0uuflmsvlvirpeo1pcnq7','180.252.202.55',1510212110,'__ci_last_regenerate|i:1510212110;'),('tgju4iega2psrclg0l55n2drotn3aa7l','36.86.56.18',1510236924,'__ci_last_regenerate|i:1510236924;'),('mkg4mvgcg8camjj63c2go5mmd5tvpn53','36.86.56.18',1510237421,'__ci_last_regenerate|i:1510237421;'),('hsahr8d5vpslkf5r3fgrlm5dvd5li41d','36.86.56.18',1510238206,'__ci_last_regenerate|i:1510238204;'),('cevjrbpmmr814q767vaq4s8s2gt2djqf','66.102.6.4',1510285010,'__ci_last_regenerate|i:1510285010;'),('5ga07kniga73itcfpjvoi5058ap0qu53','66.102.6.44',1510361621,'__ci_last_regenerate|i:1510361621;'),('4je7pcs31v0k4gv3hjvj194l9vs0ke8c','66.102.6.46',1510473242,'__ci_last_regenerate|i:1510473242;'),('ncvcj9he14sb2d2ihqgv30i2nqupgrs5','66.102.6.44',1510530744,'__ci_last_regenerate|i:1510530744;'),('sollpamsuevrqbembignpsm8ckulr6te','36.84.141.27',1510580102,'__ci_last_regenerate|i:1510580102;'),('ptq5ml8l9o27b6hilnktks3ql7hv6t4j','66.102.6.46',1510618685,'__ci_last_regenerate|i:1510618685;'),('44r94qsgo17i1o2k3n3c70sfi24k9at1','66.102.6.46',1510709253,'__ci_last_regenerate|i:1510709253;'),('814i8vou5l9lp8vviag8mj8soi5b4076','66.102.6.42',1510791089,'__ci_last_regenerate|i:1510791089;'),('n050ng7qelu87dlkjuj7qhv8119o9uhn','64.233.172.239',1510890259,'__ci_last_regenerate|i:1510890259;'),('3gb5h9h9qug3uff6000mfe96o2keld1r','66.102.6.158',1511095401,'__ci_last_regenerate|i:1511095401;'),('9dkstv1u7m9qo3n9hjeg05ht5e4teufe','66.102.6.157',1511307888,'__ci_last_regenerate|i:1511307888;'),('ipcp2du891k3oaud5lrqpkvpicldf0a9','66.102.6.159',1511419152,'__ci_last_regenerate|i:1511419152;'),('uhhdbj8h3m6hk6v79h35e8lrnhhkbijv','36.84.135.71',1511455757,'__ci_last_regenerate|i:1511455757;'),('vq5db35nk5cc6t026kqq5gmopdaco0js','180.245.85.166',1511458218,'__ci_last_regenerate|i:1511458218;'),('2ft2g4ei9rgokgvmtvo3lgh13v3lnue1','66.102.6.159',1511482263,'__ci_last_regenerate|i:1511482263;'),('jivp828fo6ichut20s4n1k0j25n9h2eh','66.102.6.158',1511657145,'__ci_last_regenerate|i:1511657145;'),('ln6m26q29nfq8d0cnbq1honbr3qscurn','180.245.85.166',1511705473,'__ci_last_regenerate|i:1511705470;'),('s2g9kqf2h4p73iq8pdch84hsoq8vs17j','66.102.6.158',1511740063,'__ci_last_regenerate|i:1511740063;'),('g5mgpld3quvu47q1rff6sda87v9s076t','66.102.6.42',1511938699,'__ci_last_regenerate|i:1511938699;'),('otbu6l5795rsq4cgd2cgqghmor6i4trn','66.102.6.158',1512002677,'__ci_last_regenerate|i:1512002677;'),('l25u16u508vqjngjk9l7tg2dos2ppdsu','36.70.89.100',1512108392,'__ci_last_regenerate|i:1512108130;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('l611hfhs3u2tccen9j4jnbt3a8bbbejd','36.70.89.100',1512109629,'__ci_last_regenerate|i:1512109329;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('ufo159p837m4k7ju6kj4h1tsj5v3moo4','36.70.89.100',1512109903,'__ci_last_regenerate|i:1512109630;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('mqhcihsidh9dhffan6u6pma0nrdki35h','36.70.89.100',1512110275,'__ci_last_regenerate|i:1512109983;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('vhqsb0vkp7idi10n4efvafp1qvsi6itg','36.70.89.100',1512110327,'__ci_last_regenerate|i:1512110309;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('ai6uk028plsjb5s6fav6c9ss46mvpusk','182.253.212.9',1512115700,'__ci_last_regenerate|i:1512115564;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('7g2upgon3pg018c5uafcnrta3rf497h7','182.253.212.9',1512116371,'__ci_last_regenerate|i:1512116076;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('5aehm6ru0n4a2o77s1if5fdpks773tb4','182.253.212.9',1512116617,'__ci_last_regenerate|i:1512116582;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('i21cb4l9h25aajn0isof3dlmqjveukbk','182.253.212.9',1512118140,'__ci_last_regenerate|i:1512117982;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('smgqjj4cqlo91kl36ipm7q0rpduebkfl','66.102.6.46',1512118232,'__ci_last_regenerate|i:1512118232;'),('0m23cg3194tbftr89ohkcb5gb9elrth5','182.253.212.9',1512118595,'__ci_last_regenerate|i:1512118294;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('v9ssa1l8l259e84ru56c4qdigub31818','182.253.212.9',1512118877,'__ci_last_regenerate|i:1512118598;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('rkfl39i3cn2s86d8ctmn8ggaqpd1hi59','182.253.212.9',1512119210,'__ci_last_regenerate|i:1512118911;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('qhksllshhsr85n6si03rerql9gdlrg4o','182.253.212.9',1512119582,'__ci_last_regenerate|i:1512119480;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('1n2l7eoguov2h5jugndencfk7cr5j7cr','182.253.212.9',1512120544,'__ci_last_regenerate|i:1512120531;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('dnubo2ngjd808smnfajlcj4eft4v2b5a','182.253.212.9',1512121512,'__ci_last_regenerate|i:1512121433;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('7tvru8cp55uvv93abaqekl0p5c4usamb','182.253.212.9',1512122436,'__ci_last_regenerate|i:1512122186;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('1a26isk7sigu62k900lfcm4mn7elhht6','182.253.212.9',1512122802,'__ci_last_regenerate|i:1512122621;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('r526i75rv3vcachdcd3ti7uiqafgkbk2','182.253.212.9',1512123479,'__ci_last_regenerate|i:1512123202;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('o9sc86g2h7j67fiptbuvi8nkoa156tj3','182.253.212.9',1512123861,'__ci_last_regenerate|i:1512123671;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('9c4ou1lquiumj3bq6ui1e87r5bcvlqop','182.253.212.9',1512125124,'__ci_last_regenerate|i:1512124888;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('4bh5jkj243qk6m018esnh60c70i64l7e','182.253.212.9',1512125443,'__ci_last_regenerate|i:1512125217;id|s:1:\"1\";username|s:6:\"hilman\";company_id|s:1:\"0\";merchant_id|N;level_user|s:1:\"1\";name|s:6:\"hilman\";accountType|s:9:\"Developer\";id_group|s:1:\"7\";authLogin|s:32:\"cf081b11e184de45ecce347f758936f9\";'),('pfpkovkf6e0sqnbs1cap46o6c181rnkk','61.5.82.21',1512353186,'__ci_last_regenerate|i:1512353186;'),('cp0ae86nkhk1e0p0lbjob9fifct2es0k','66.102.6.6',1512354537,'__ci_last_regenerate|i:1512354537;'),('i4ig97ka6qodpcvrpfqc6mkfjqjq1vhe','66.102.6.46',1512516423,'__ci_last_regenerate|i:1512516422;'),('2neub8jesm9a9sq4c2ooekj0k39m7vhi','66.102.6.4',1512630942,'__ci_last_regenerate|i:1512630942;');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `city_code` varchar(10) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_news`
--

DROP TABLE IF EXISTS `comment_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_news`
--

LOCK TABLES `comment_news` WRITE;
/*!40000 ALTER TABLE `comment_news` DISABLE KEYS */;
INSERT INTO `comment_news` VALUES (1,1,27,'Good news',NULL,NULL,NULL),(4,1,27,'comment from postman',NULL,NULL,NULL);
/*!40000 ALTER TABLE `comment_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'KFC','021876522','syafeihilman@gmail.com','13860','Jakarta','2017-07-21 14:21:12','2017-07-21 14:23:18','hilman'),(2,'Mac Donals','0210987752','mcd@gmail.com','12345','jakarta','2017-07-25 16:25:27','2017-07-25 16:25:27','hilman'),(3,'All Company','021021021021','all@gmai.com','13890','111111','2017-10-17 10:50:07','2017-10-17 10:50:07','hilman');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) NOT NULL DEFAULT '0',
  `message` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_us`
--

LOCK TABLES `contact_us` WRITE;
/*!40000 ALTER TABLE `contact_us` DISABLE KEYS */;
INSERT INTO `contact_us` VALUES (3,'syafeihilman@gmail.com','Test','Test contact us','2017-10-25 16:52:58','2017-10-25 16:52:58',NULL);
/*!40000 ALTER TABLE `contact_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `addres` varchar(255) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `total_point` int(11) DEFAULT NULL,
  `total_balance` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_customer_UNIQUE` (`customer_id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `phone_number_UNIQUE` (`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_priviledge`
--

DROP TABLE IF EXISTS `group_priviledge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_priviledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_group` int(11) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `id_submenu` int(11) DEFAULT NULL,
  `access_grant` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inx` (`id_user_group`,`id_menu`,`id_submenu`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=453 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_priviledge`
--

LOCK TABLES `group_priviledge` WRITE;
/*!40000 ALTER TABLE `group_priviledge` DISABLE KEYS */;
INSERT INTO `group_priviledge` VALUES (302,1,3,7,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(303,1,3,7,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(304,1,3,7,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(305,1,4,8,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(306,1,4,8,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(307,1,4,8,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(308,1,5,9,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(309,1,5,9,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(310,1,5,9,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(311,1,5,10,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(312,1,5,10,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(313,1,5,10,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(314,1,5,12,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(315,1,5,12,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(316,1,5,12,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(317,1,2,4,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(318,1,2,4,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(319,1,2,4,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(320,1,2,5,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(321,1,2,5,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(322,1,2,5,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(323,1,2,6,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(324,1,2,6,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(325,1,2,6,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(326,1,2,11,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(327,1,2,11,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(328,1,2,11,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(329,1,2,13,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(330,1,2,13,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(331,1,2,13,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(332,1,1,1,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(333,1,1,1,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(334,1,1,1,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(335,1,1,2,1,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(336,1,1,2,2,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(337,1,1,2,4,'2017-07-26 02:19:01','2017-07-26 02:19:01','hilman'),(338,7,3,7,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(339,7,3,7,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(340,7,3,7,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(341,7,4,8,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(342,7,4,8,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(343,7,4,8,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(344,7,5,9,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(345,7,5,9,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(346,7,5,9,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(347,7,5,10,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(348,7,5,10,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(349,7,5,10,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(350,7,5,12,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(351,7,5,12,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(352,7,5,12,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(353,7,7,14,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(354,7,7,14,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(355,7,7,14,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(356,7,2,4,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(357,7,2,4,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(358,7,2,4,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(359,7,2,5,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(360,7,2,5,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(361,7,2,5,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(362,7,2,6,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(363,7,2,6,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(364,7,2,6,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(365,7,2,11,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(366,7,2,11,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(367,7,2,11,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(368,7,2,13,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(369,7,2,13,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(370,7,2,13,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(371,7,1,1,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(372,7,1,1,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(373,7,1,1,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(374,7,1,3,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(375,7,1,3,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(376,7,1,3,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(377,7,1,2,1,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(378,7,1,2,2,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(379,7,1,2,4,'2017-07-26 02:19:47','2017-07-26 02:19:47','hilman'),(401,11,4,8,1,'2017-07-26 02:20:53','2017-07-26 02:20:53','hilman'),(402,11,4,8,2,'2017-07-26 02:20:53','2017-07-26 02:20:53','hilman'),(403,11,4,8,4,'2017-07-26 02:20:53','2017-07-26 02:20:53','hilman'),(419,9,3,7,1,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(420,9,3,7,2,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(421,9,3,7,4,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(422,9,4,8,1,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(423,9,4,8,2,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(424,9,4,8,4,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(425,9,4,17,1,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(426,9,4,17,2,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(427,9,2,11,1,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(428,9,2,11,2,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(429,9,2,11,4,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(430,9,1,1,1,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(431,9,1,1,2,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(432,9,1,1,4,'2017-11-02 10:00:36','2017-11-02 10:00:36','hilman'),(433,10,3,7,1,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(434,10,3,7,2,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(435,10,3,7,4,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(436,10,4,8,1,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(437,10,4,8,2,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(438,10,4,8,4,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(439,10,4,17,1,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(440,10,4,17,2,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(441,10,4,17,4,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(442,10,1,1,1,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(443,10,1,1,2,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(444,10,1,1,4,'2017-11-02 10:01:04','2017-11-02 10:01:04','hilman'),(445,12,3,7,1,'2017-11-02 10:01:12','2017-11-02 10:01:12','hilman'),(446,12,4,8,1,'2017-11-02 10:01:12','2017-11-02 10:01:12','hilman'),(447,12,4,17,1,'2017-11-02 10:01:12','2017-11-02 10:01:12','hilman'),(448,12,2,11,1,'2017-11-02 10:01:12','2017-11-02 10:01:12','hilman'),(449,13,3,7,1,'2017-11-02 10:01:20','2017-11-02 10:01:20','hilman'),(450,13,4,8,1,'2017-11-02 10:01:20','2017-11-02 10:01:20','hilman'),(451,13,4,17,1,'2017-11-02 10:01:20','2017-11-02 10:01:20','hilman'),(452,13,1,1,1,'2017-11-02 10:01:20','2017-11-02 10:01:20','hilman');
/*!40000 ALTER TABLE `group_priviledge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) DEFAULT NULL,
  `voucher_id` varchar(255) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `invoice_number` varchar(255) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `bonus_voucher` int(11) NOT NULL DEFAULT '0',
  `total_price` int(11) DEFAULT NULL,
  `total_price_point` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(255) DEFAULT NULL,
  `status_invoice` varchar(45) NOT NULL DEFAULT '0',
  `order_date` datetime DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `information` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `invoice_number_UNIQUE` (`invoice_number`),
  KEY `mer_com` (`company_id`,`merchant_id`),
  KEY `ord` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=306 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (295,'ORD-0000478','VCR-0000017',1,1,'INV-0000420',3,1,6300,0,71,'1','2017-11-29 13:50:52',1,NULL,'2017-11-29 13:50:52','2017-11-29 13:50:52','71'),(296,'ORD-0000479','VCR-0000011',1,1,'INV-0000421',1,0,500000,0,71,'2','2017-11-29 13:54:31',3,NULL,'2017-11-29 13:54:31','2017-11-29 13:54:31','71'),(297,'ORD-0000480','VCR-0000011',1,1,'INV-0000422',1,0,500000,0,71,'2','2017-11-29 13:56:51',3,NULL,'2017-11-29 13:56:51','2017-11-29 13:56:51','71'),(298,'ORD-0000482','VCR-0000011',1,1,'INV-0000423',1,0,500000,0,71,'2','2017-11-29 13:59:38',3,NULL,'2017-11-29 13:59:38','2017-11-29 13:59:38','71'),(299,'ORD-0000483','VCR-0000011',1,1,'INV-0000424',1,0,500000,0,71,'3','2017-11-29 14:02:27',3,NULL,'2017-11-29 14:02:27','2017-11-29 14:02:27','71'),(300,'ORD-0000484','VCR-0000011',1,1,'INV-0000425',1,0,500000,0,71,'3','2017-11-29 14:03:04',3,NULL,'2017-11-29 14:03:04','2017-11-29 14:03:04','71'),(301,'ORD-0000486','VCR-0000011',1,1,'INV-0000426',1,0,500000,0,71,'2','2017-11-29 14:04:06',3,NULL,'2017-11-29 14:04:06','2017-11-29 14:04:06','71'),(302,'ORD-0000489','VCR-0000011',1,1,'INV-0000427',1,0,500000,0,76,'3','2017-11-30 10:59:13',3,NULL,'2017-11-30 10:59:13','2017-11-30 10:59:13','76'),(303,'ORD-0000491','VCR-0000011',1,1,'INV-0000428',1,0,500000,0,76,'3','2017-11-30 14:42:54',3,NULL,'2017-11-30 14:42:54','2017-11-30 14:42:54','76'),(304,'ORD-0000492','VCR-0000011',1,1,'INV-0000429',1,0,500000,0,76,'2','2017-12-04 13:27:34',3,NULL,'2017-12-04 13:27:34','2017-12-04 13:27:34','76'),(305,'ORD-0000494','VCR-0000003',1,1,'INV-0000430',1,0,180000,0,76,'3','2017-12-05 12:04:06',1,NULL,'2017-12-05 12:04:06','2017-12-05 12:04:06','76');
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_history`
--

DROP TABLE IF EXISTS `invoice_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(100) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `voucher_id` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `bonus_point` int(11) DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `voucher_category` int(11) DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `redeem_date` datetime DEFAULT NULL,
  `used` int(11) NOT NULL DEFAULT '0',
  `qr_code` varchar(255) DEFAULT NULL,
  `cashier_id` int(11) NOT NULL DEFAULT '0',
  `confirmation_number` int(11) DEFAULT NULL,
  `withdraw_status` int(11) NOT NULL DEFAULT '0',
  `voucher_type` int(11) NOT NULL DEFAULT '0' COMMENT '0 payment,1 bonus',
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ord` (`order_id`),
  KEY `vcr` (`voucher_id`),
  KEY `inv` (`invoice_number`),
  KEY `used` (`used`),
  KEY `qr_code` (`qr_code`),
  KEY `cnfrm` (`confirmation_number`)
) ENGINE=InnoDB AUTO_INCREMENT=427 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_history`
--

LOCK TABLES `invoice_history` WRITE;
/*!40000 ALTER TABLE `invoice_history` DISABLE KEYS */;
INSERT INTO `invoice_history` VALUES (414,'ORD-0000478','Semangka','INV-0000420','VCR-0000017',3150,NULL,100,1,7,'2017-12-09',NULL,0,'KTUTJGC5F4',0,NULL,0,0,'2017-11-29 13:50:52','2017-11-29 13:50:52','71'),(415,'ORD-0000478','Semangka','INV-0000420','VCR-0000017',3150,NULL,100,1,7,'2017-12-09',NULL,0,'8DSAVLCLKF',0,NULL,0,0,'2017-11-29 13:50:52','2017-11-29 13:50:52','71'),(416,'ORD-0000478','Semangka','INV-0000420','VCR-0000017',0,0,100,1,7,'2017-12-09',NULL,0,'5NNMJBFSM0',0,NULL,0,1,'2017-11-29 13:50:52','2017-11-29 13:50:52','71'),(417,'ORD-0000479','Rumah Sakit 3','INV-0000421','VCR-0000011',500000,NULL,100,3,4,'2017-12-30',NULL,0,NULL,0,NULL,0,0,'2017-11-29 13:54:31','2017-11-29 13:54:31','71'),(418,'ORD-0000480','Rumah Sakit 3','INV-0000422','VCR-0000011',500000,NULL,100,3,4,'2017-12-30',NULL,0,NULL,0,NULL,0,0,'2017-11-29 13:56:51','2017-11-29 13:56:51','71'),(419,'ORD-0000482','Rumah Sakit 3','INV-0000423','VCR-0000011',500000,NULL,100,3,4,'2017-12-30',NULL,0,NULL,0,NULL,0,0,'2017-11-29 13:59:38','2017-11-29 13:59:38','71'),(420,'ORD-0000483','Rumah Sakit 3','INV-0000424','VCR-0000011',500000,NULL,100,3,4,'2017-12-30',NULL,0,'VQWXKV055P',0,NULL,0,0,'2017-11-29 14:02:27','2017-11-29 14:02:27','71'),(421,'ORD-0000484','Rumah Sakit 3','INV-0000425','VCR-0000011',500000,NULL,100,3,4,'2017-12-30',NULL,0,NULL,0,NULL,0,0,'2017-11-29 14:03:04','2017-11-29 14:03:04','71'),(422,'ORD-0000486','Rumah Sakit 3','INV-0000426','VCR-0000011',500000,NULL,100,3,4,'2017-12-30',NULL,0,NULL,0,NULL,0,0,'2017-11-29 14:04:06','2017-11-29 14:04:06','71'),(423,'ORD-0000489','Rumah Sakit 3','INV-0000427','VCR-0000011',500000,NULL,100,3,4,'2017-12-30','2017-11-30 11:03:49',1,'73RHEBAMH9',15,412163,0,0,'2017-11-30 10:59:13','2017-11-30 10:59:13','76'),(424,'ORD-0000491','Rumah Sakit 3','INV-0000428','VCR-0000011',500000,0,100,3,4,'2017-12-30',NULL,0,'XQWO4IG85P',0,NULL,0,0,'2017-11-30 14:42:54','2017-11-30 14:42:54','76'),(425,'ORD-0000492','Rumah Sakit 3','INV-0000429','VCR-0000011',500000,NULL,100,3,4,'2017-12-30',NULL,0,NULL,0,NULL,0,0,'2017-12-04 13:27:34','2017-12-04 13:27:34','76'),(426,'ORD-0000494','Ayam Gepuk','INV-0000430','VCR-0000003',180000,NULL,0,1,7,'2017-11-07',NULL,0,'CMY2HMCQ50',0,NULL,0,0,'2017-12-05 12:04:06','2017-12-05 12:04:06','76');
/*!40000 ALTER TABLE `invoice_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kurs_point`
--

DROP TABLE IF EXISTS `kurs_point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kurs_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `point` int(4) NOT NULL,
  `money` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inx` (`point`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kurs_point`
--

LOCK TABLES `kurs_point` WRITE;
/*!40000 ALTER TABLE `kurs_point` DISABLE KEYS */;
INSERT INTO `kurs_point` VALUES (1,1,1,NULL,'2017-12-01 06:34:04','');
/*!40000 ALTER TABLE `kurs_point` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_notif_payment`
--

DROP TABLE IF EXISTS `log_notif_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_notif_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notif_data` text,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `inx` (`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_notif_payment`
--

LOCK TABLES `log_notif_payment` WRITE;
/*!40000 ALTER TABLE `log_notif_payment` DISABLE KEYS */;
INSERT INTO `log_notif_payment` VALUES (1,'{\n  \"transaction_time\": \"2017-11-12 02:54:52\",\n  \"transaction_status\": \"capture\",\n  \"transaction_id\": \"83c7be6e-4db4-4923-a2ef-76a9ca196d25\",\n  \"status_message\": \"Veritrans payment notification\",\n  \"status_code\": \"200\",\n  \"signature_key\": \"411eb2b3cddd08fbedf9e4d7a4998783b1dabe9d8a4270683910835fc4e2efe96ac176bd7e745a4fc2bddc34e839cf50fe4cf392dc5ba78fc71e5d6cb5856d28\",\n  \"payment_type\": \"credit_card\",\n  \"order_id\": \"ORD-0000389\",\n  \"masked_card\": \"481111-1114\",\n  \"gross_amount\": \"3150.00\",\n  \"fraud_status\": \"accept\",\n  \"eci\": \"05\",\n  \"channel_response_message\": \"Approved\",\n  \"channel_response_code\": \"00\",\n  \"bank\": \"mandiri\",\n  \"approval_code\": \"1510430096305\"\n}','2017-11-24 00:37:52','2017-11-24 00:37:52',NULL),(2,'{\"transaction_time\":\"2017-11-24 07:38:43\",\"transaction_status\":\"capture\",\"transaction_id\":\"800a4019-f8af-4041-b157-745977ba742e\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"365c370aae4488d4f1aefd63470ec407e21316c64fed93b39f7693bde64eb5f4c3dfae0278f9d53d795edd179d3a9adfa4ac0bc9b801fb107e89e652d45d1299\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000427\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511483926766\"}','2017-11-24 02:06:58','2017-11-24 02:06:58',NULL),(3,'{\"transaction_time\":\"2017-11-24 07:27:28\",\"transaction_status\":\"capture\",\"transaction_id\":\"453b8009-d7f6-4326-9092-ad85f835c451\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"300acdd1dc497154a295f9706de4f26fc6f7e4b3629837c8390b22d96bc93f03657c175cbc8ae44cdb2a0b1fc1aba761af227ee03eb5671e25e7f2462321e567\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000425\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511483252001\"}','2017-11-24 02:06:59','2017-11-24 02:06:59',NULL),(4,'{\"transaction_time\":\"2017-11-24 07:28:57\",\"transaction_status\":\"capture\",\"transaction_id\":\"81e96e33-ca6e-41ad-b678-6ad618b81794\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"9f5852c60866e3d1d0b1494e29eea2d308323211984fc230d7f8a8620a43ae9c235fa9a832f5d47a76855e9cbe1cd78ab434e9c9945e2e6917990561d0c1e2cc\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000426\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511483341028\"}','2017-11-24 02:07:00','2017-11-24 02:07:00',NULL),(5,'{\"transaction_time\":\"2017-11-24 07:13:19\",\"transaction_status\":\"capture\",\"transaction_id\":\"b99cd2c9-996d-436a-bedf-11ddff34e147\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"f06fd0750143c35397c9976044218127a8783ac44cbe9b9ef5c13f8baafbb54fd4642dc56940bfcc8e8e998f33da631a442af05d5f609fa65176fea8c52ee29b\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000424\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"3150.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511482399463\"}','2017-11-24 02:07:01','2017-11-24 02:07:01',NULL),(6,'','2017-11-24 06:46:53','2017-11-24 06:46:53',NULL),(7,'','2017-11-24 06:47:37','2017-11-24 06:47:37',NULL),(8,'{\"transaction_time\":\"2017-11-24 13:51:50\",\"transaction_status\":\"capture\",\"transaction_id\":\"d186787b-3452-4f59-acb5-da97e14aa0a9\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"ea6ea6a41f62a32e16f0188b827f21af712716ea77a333b4a33b4238bb0ac789cac4bd8b7beedb259096df3fe8b385e3fea1b09b6c08f46ca7ca47472b06b60e\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000429\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"3150.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511506310497\"}','2017-11-24 06:51:52','2017-11-24 06:51:52',NULL),(9,'{\"transaction_time\":\"2017-11-24 13:51:50\",\"transaction_status\":\"capture\",\"transaction_id\":\"d186787b-3452-4f59-acb5-da97e14aa0a9\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"ea6ea6a41f62a32e16f0188b827f21af712716ea77a333b4a33b4238bb0ac789cac4bd8b7beedb259096df3fe8b385e3fea1b09b6c08f46ca7ca47472b06b60e\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000429\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"3150.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511506310497\"}','2017-11-24 06:53:31','2017-11-24 06:53:31',NULL),(10,'{\"va_numbers\":[{\"va_number\":\"05135910317\",\"bank\":\"bca\"}],\"transaction_time\":\"2017-11-24 13:58:43\",\"transaction_status\":\"pending\",\"transaction_id\":\"d1ace7b4-3459-438e-938d-7339a041c3ad\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"357b52b39df444d4cc31c8167a1cde49e0164b44a0fb9a23a160fa6fc7a9a621cea8e7c2b1d98b1d45c3509b3642cbd879902affb4ec5244a33f6251364a560e\",\"payment_type\":\"bank_transfer\",\"payment_amounts\":[],\"order_id\":\"ORD-0000430\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\"}','2017-11-24 06:58:45','2017-11-24 06:58:45',NULL),(11,'{\"va_numbers\":[{\"va_number\":\"05135910317\",\"bank\":\"bca\"}],\"transaction_time\":\"2017-11-24 13:58:43\",\"transaction_status\":\"settlement\",\"transaction_id\":\"d1ace7b4-3459-438e-938d-7339a041c3ad\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"6a93f2e557dd92e3398d342f5cc9a8d7b4a161b72626f458b29546cba13ce7be393d4dffc8f76685946e29af5b88821f69da54086312c99725a9ee5551d10a36\",\"payment_type\":\"bank_transfer\",\"payment_amounts\":[],\"order_id\":\"ORD-0000430\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\"}','2017-11-24 06:59:54','2017-11-24 06:59:54',NULL),(12,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"pending\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"18c41a6e0c6e10ea73756d84a4246818077c0ba082865ce9b0f3cbece218c1350b8567a274de6555fa1234de5269993d49ab606f7c647eec9109913f39711214\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:15:13','2017-11-24 07:15:13',NULL),(13,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"pending\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"18c41a6e0c6e10ea73756d84a4246818077c0ba082865ce9b0f3cbece218c1350b8567a274de6555fa1234de5269993d49ab606f7c647eec9109913f39711214\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:15:54','2017-11-24 07:15:54',NULL),(14,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"settlement\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"c1a6265b4731dd43dd78d53ab6a9d09df4b20a1c61ea4c05477a5618e7b498aed984ef2b1ff0347957ae3121208e14c9724b61c0125699c4766d7aefe59a4bdb\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:15:59','2017-11-24 07:15:59',NULL),(15,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"settlement\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"c1a6265b4731dd43dd78d53ab6a9d09df4b20a1c61ea4c05477a5618e7b498aed984ef2b1ff0347957ae3121208e14c9724b61c0125699c4766d7aefe59a4bdb\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:16:52','2017-11-24 07:16:52',NULL),(16,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"settlement\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"c1a6265b4731dd43dd78d53ab6a9d09df4b20a1c61ea4c05477a5618e7b498aed984ef2b1ff0347957ae3121208e14c9724b61c0125699c4766d7aefe59a4bdb\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:21:04','2017-11-24 07:21:04',NULL),(17,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"pending\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"18c41a6e0c6e10ea73756d84a4246818077c0ba082865ce9b0f3cbece218c1350b8567a274de6555fa1234de5269993d49ab606f7c647eec9109913f39711214\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:21:04','2017-11-24 07:21:04',NULL),(18,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"pending\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"18c41a6e0c6e10ea73756d84a4246818077c0ba082865ce9b0f3cbece218c1350b8567a274de6555fa1234de5269993d49ab606f7c647eec9109913f39711214\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:22:28','2017-11-24 07:22:28',NULL),(19,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"settlement\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"c1a6265b4731dd43dd78d53ab6a9d09df4b20a1c61ea4c05477a5618e7b498aed984ef2b1ff0347957ae3121208e14c9724b61c0125699c4766d7aefe59a4bdb\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:25:11','2017-11-24 07:25:11',NULL),(20,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"pending\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"18c41a6e0c6e10ea73756d84a4246818077c0ba082865ce9b0f3cbece218c1350b8567a274de6555fa1234de5269993d49ab606f7c647eec9109913f39711214\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:25:11','2017-11-24 07:25:11',NULL),(21,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"settlement\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"c1a6265b4731dd43dd78d53ab6a9d09df4b20a1c61ea4c05477a5618e7b498aed984ef2b1ff0347957ae3121208e14c9724b61c0125699c4766d7aefe59a4bdb\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:26:39','2017-11-24 07:26:39',NULL),(22,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"pending\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"18c41a6e0c6e10ea73756d84a4246818077c0ba082865ce9b0f3cbece218c1350b8567a274de6555fa1234de5269993d49ab606f7c647eec9109913f39711214\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:27:28','2017-11-24 07:27:28',NULL),(23,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"settlement\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"c1a6265b4731dd43dd78d53ab6a9d09df4b20a1c61ea4c05477a5618e7b498aed984ef2b1ff0347957ae3121208e14c9724b61c0125699c4766d7aefe59a4bdb\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:38:23','2017-11-24 07:38:23',NULL),(24,'{\"transaction_time\":\"2017-11-24 14:15:11\",\"transaction_status\":\"pending\",\"transaction_id\":\"d517ccd7-8eb7-4d93-9ef2-b9e3e458c904\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"18c41a6e0c6e10ea73756d84a4246818077c0ba082865ce9b0f3cbece218c1350b8567a274de6555fa1234de5269993d49ab606f7c647eec9109913f39711214\",\"payment_type\":\"echannel\",\"order_id\":\"ORD-0000431\",\"gross_amount\":\"1200000.00\",\"fraud_status\":\"accept\",\"biller_code\":\"70012\",\"bill_key\":\"122586852231\"}','2017-11-24 07:38:23','2017-11-24 07:38:23',NULL),(25,'{\"transaction_time\":\"2017-11-24 07:27:28\",\"transaction_status\":\"settlement\",\"transaction_id\":\"453b8009-d7f6-4326-9092-ad85f835c451\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"300acdd1dc497154a295f9706de4f26fc6f7e4b3629837c8390b22d96bc93f03657c175cbc8ae44cdb2a0b1fc1aba761af227ee03eb5671e25e7f2462321e567\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000425\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511483252001\"}','2017-11-25 18:56:12','2017-11-25 18:56:12',NULL),(26,'{\"transaction_time\":\"2017-11-24 07:38:43\",\"transaction_status\":\"settlement\",\"transaction_id\":\"800a4019-f8af-4041-b157-745977ba742e\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"365c370aae4488d4f1aefd63470ec407e21316c64fed93b39f7693bde64eb5f4c3dfae0278f9d53d795edd179d3a9adfa4ac0bc9b801fb107e89e652d45d1299\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000427\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511483926766\"}','2017-11-25 18:56:19','2017-11-25 18:56:19',NULL),(27,'{\"transaction_time\":\"2017-11-24 07:28:57\",\"transaction_status\":\"settlement\",\"transaction_id\":\"81e96e33-ca6e-41ad-b678-6ad618b81794\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"9f5852c60866e3d1d0b1494e29eea2d308323211984fc230d7f8a8620a43ae9c235fa9a832f5d47a76855e9cbe1cd78ab434e9c9945e2e6917990561d0c1e2cc\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000426\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511483341028\"}','2017-11-25 18:56:28','2017-11-25 18:56:28',NULL),(28,'{\"transaction_time\":\"2017-11-24 07:13:19\",\"transaction_status\":\"settlement\",\"transaction_id\":\"b99cd2c9-996d-436a-bedf-11ddff34e147\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"f06fd0750143c35397c9976044218127a8783ac44cbe9b9ef5c13f8baafbb54fd4642dc56940bfcc8e8e998f33da631a442af05d5f609fa65176fea8c52ee29b\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000424\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"3150.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511482399463\"}','2017-11-25 18:56:30','2017-11-25 18:56:30',NULL),(29,'{\"transaction_time\":\"2017-11-24 13:51:50\",\"transaction_status\":\"settlement\",\"transaction_id\":\"d186787b-3452-4f59-acb5-da97e14aa0a9\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"ea6ea6a41f62a32e16f0188b827f21af712716ea77a333b4a33b4238bb0ac789cac4bd8b7beedb259096df3fe8b385e3fea1b09b6c08f46ca7ca47472b06b60e\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000429\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"3150.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511506310497\"}','2017-11-25 18:56:36','2017-11-25 18:56:36',NULL),(30,'{\"transaction_time\":\"2017-11-27 21:39:13\",\"transaction_status\":\"capture\",\"transaction_id\":\"a2e489e7-1d43-43ab-85d3-cf6ecd274509\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"b11d5d5994c777f98ee46b66c4f5875d263486644b8b7f3d9d691ac1ec5dfb97ea6f3295ff52a7611405f52d3d83221b77dbe8bad4dd70e8ae4031d4dc7c6d8f\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000433\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511793554092\"}','2017-11-27 14:39:16','2017-11-27 14:39:16',NULL),(31,'{\"va_numbers\":[{\"va_number\":\"05135742822\",\"bank\":\"bca\"}],\"transaction_time\":\"2017-11-27 21:40:55\",\"transaction_status\":\"pending\",\"transaction_id\":\"9fa3cf1a-0c70-4626-87cd-64e737233d41\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"109cfb71d40c9358e47247faa871959d9b00ffe09ac3dbec155796f7e4732a5648964456b09a725594628f25ea9f72a53fc39af28331fe94bc29326d4775da85\",\"payment_type\":\"bank_transfer\",\"payment_amounts\":[],\"order_id\":\"ORD-0000434\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\"}','2017-11-27 14:40:59','2017-11-27 14:40:59',NULL),(32,'{\"va_numbers\":[{\"va_number\":\"05135742822\",\"bank\":\"bca\"}],\"transaction_time\":\"2017-11-27 21:40:55\",\"transaction_status\":\"settlement\",\"transaction_id\":\"9fa3cf1a-0c70-4626-87cd-64e737233d41\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"1af7fac2ee1f0544fbae3a920a12dcf2d66d31f20484d374fbc79654819bcedce82523601f06ff6c100ce57ea67a5697901d39b0fac12293993f553ec5e59b02\",\"payment_type\":\"bank_transfer\",\"payment_amounts\":[],\"order_id\":\"ORD-0000434\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\"}','2017-11-27 14:41:31','2017-11-27 14:41:31',NULL),(33,'{\"transaction_time\":\"2017-11-27 21:48:15\",\"transaction_status\":\"capture\",\"transaction_id\":\"0df02763-420f-4f8a-96e8-269827eb1a6e\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"c5a5b3ad474054bb9b98d61d8f3143df54116738366e3dfd9847e9c848274a99b38d5962a373f58289522a8e00cf348a7223a51912559e7de919d23b24313d5f\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000436\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511794095707\"}','2017-11-27 14:48:17','2017-11-27 14:48:17',NULL),(34,'{\"transaction_time\":\"2017-11-27 21:49:27\",\"transaction_status\":\"capture\",\"transaction_id\":\"e1a3f966-c12e-4bef-95ea-7cdad419ab3e\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"692a41762d37beb49fe5202fb1c670762c86e30b90c2fc41caf86e8978e1e89cbbaaa9962c28480197d8d81a885a10d2721fb74bbe15578f55d583e4138ce6e6\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000437\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511794167603\"}','2017-11-27 14:49:29','2017-11-27 14:49:29',NULL),(35,'{\"transaction_time\":\"2017-11-27 22:46:57\",\"transaction_status\":\"capture\",\"transaction_id\":\"486c68dc-522a-4b96-9d37-f6b851d175e6\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"d2a160c221ec3bda0dc252d7948aac7b288fa802c46e018bcddb723484a8da006491804d6101a1c9f4606db2736fa0bc5ae6366658c3251ee075e655b50c4e32\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000440\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511797617731\"}','2017-11-27 15:46:59','2017-11-27 15:46:59',NULL),(36,'{\"transaction_time\":\"2017-11-27 22:48:38\",\"transaction_status\":\"capture\",\"transaction_id\":\"1ca57f6a-dc85-4cc6-9934-40cdb572735d\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"06477b2ed6aa8c9a126765b2336328337940cd325eedfdc318c8d24a7fd9d2430f6950eeda444fb40d3950557989633fb1d660089a2b90f90328e8b280536c99\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000441\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511797721284\"}','2017-11-27 15:48:43','2017-11-27 15:48:43',NULL),(37,'{\"transaction_time\":\"2017-11-28 05:04:56\",\"transaction_status\":\"capture\",\"transaction_id\":\"905846c2-685b-4090-869d-c8a11f3fa6d1\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"f79fee118aaeefb714b0c2423655f66937b07f50789963ae09ea3f09dc997e26f24949ef38c9ffcc0512090aee5cd5aaa56ee6b2fafa044f82ce5a5b60438fa7\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000442\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511820300145\"}','2017-11-27 22:05:02','2017-11-27 22:05:02',NULL),(38,'{\"transaction_time\":\"2017-11-28 05:07:20\",\"transaction_status\":\"capture\",\"transaction_id\":\"5fd8a577-40a5-4408-ae3b-d3daaea919f3\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"263f737b5efaf52a3e38e52bc1e62805b80f698871adbe951249f07666e72bc7a7d93d94cfc6b23f297271d202e32edc4db1de06d2a017a316083472c98fb649\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000443\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511820443765\"}','2017-11-27 22:07:25','2017-11-27 22:07:25',NULL),(39,'{\"transaction_time\":\"2017-11-28 05:14:14\",\"transaction_status\":\"capture\",\"transaction_id\":\"554d81ce-0dca-467b-8681-4c133fbb7181\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"4e0100edb5ce8ace71437c8a873df72c73852bc9e41de9f391e62fd409845474f343f2a82948067325d425188ffb6b8990cb54ded9c9b13745907d73e3172ee4\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000444\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511820854874\"}','2017-11-27 22:14:16','2017-11-27 22:14:16',NULL),(40,'{\"transaction_time\":\"2017-11-27 21:48:15\",\"transaction_status\":\"settlement\",\"transaction_id\":\"0df02763-420f-4f8a-96e8-269827eb1a6e\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"c5a5b3ad474054bb9b98d61d8f3143df54116738366e3dfd9847e9c848274a99b38d5962a373f58289522a8e00cf348a7223a51912559e7de919d23b24313d5f\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000436\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511794095707\"}','2017-11-28 09:03:37','2017-11-28 09:03:37',NULL),(41,'{\"transaction_time\":\"2017-11-27 22:46:57\",\"transaction_status\":\"settlement\",\"transaction_id\":\"486c68dc-522a-4b96-9d37-f6b851d175e6\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"d2a160c221ec3bda0dc252d7948aac7b288fa802c46e018bcddb723484a8da006491804d6101a1c9f4606db2736fa0bc5ae6366658c3251ee075e655b50c4e32\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000440\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511797617731\"}','2017-11-28 09:03:37','2017-11-28 09:03:37',NULL),(42,'{\"transaction_time\":\"2017-11-27 21:49:27\",\"transaction_status\":\"settlement\",\"transaction_id\":\"e1a3f966-c12e-4bef-95ea-7cdad419ab3e\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"692a41762d37beb49fe5202fb1c670762c86e30b90c2fc41caf86e8978e1e89cbbaaa9962c28480197d8d81a885a10d2721fb74bbe15578f55d583e4138ce6e6\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000437\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511794167603\"}','2017-11-28 09:03:37','2017-11-28 09:03:37',NULL),(43,'{\"transaction_time\":\"2017-11-27 21:39:13\",\"transaction_status\":\"settlement\",\"transaction_id\":\"a2e489e7-1d43-43ab-85d3-cf6ecd274509\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"b11d5d5994c777f98ee46b66c4f5875d263486644b8b7f3d9d691ac1ec5dfb97ea6f3295ff52a7611405f52d3d83221b77dbe8bad4dd70e8ae4031d4dc7c6d8f\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000433\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511793554092\"}','2017-11-28 09:03:37','2017-11-28 09:03:37',NULL),(44,'{\"transaction_time\":\"2017-11-27 22:48:38\",\"transaction_status\":\"settlement\",\"transaction_id\":\"1ca57f6a-dc85-4cc6-9934-40cdb572735d\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"06477b2ed6aa8c9a126765b2336328337940cd325eedfdc318c8d24a7fd9d2430f6950eeda444fb40d3950557989633fb1d660089a2b90f90328e8b280536c99\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000441\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511797721284\"}','2017-11-28 09:03:37','2017-11-28 09:03:37',NULL),(45,'{\"transaction_time\":\"2017-11-29 07:09:50\",\"transaction_status\":\"capture\",\"transaction_id\":\"bcb6b766-9fb9-4451-b545-6ab8459c5f5c\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"82b61c2caeaec37e744f21258539db3db91a8613fd3a13332406671aee450fa91a9804b13fbd9f8c96203d4bfcbf14d490526fe906d68f764c517eef45640531\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000447\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"30000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511914190633\"}','2017-11-29 00:09:53','2017-11-29 00:09:53',NULL),(46,'{\"transaction_time\":\"2017-11-29 07:15:29\",\"transaction_status\":\"capture\",\"transaction_id\":\"165e478b-8a9c-41b5-9415-02f63a85a486\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"ed60ff01b283a03676acd819070372a355cb7d5911f6bbf09bcdba2b8c59a2b4aed093ee7bf9b63b98792fe653c151c86d06ec418dafc337107198e258b0d543\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000448\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"30000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511914529697\"}','2017-11-29 00:15:31','2017-11-29 00:15:31',NULL),(47,'{\"va_numbers\":[{\"va_number\":\"05135217467\",\"bank\":\"bca\"}],\"transaction_time\":\"2017-11-29 13:50:50\",\"transaction_status\":\"pending\",\"transaction_id\":\"b271fe23-df43-4f44-b6d3-06413453aed1\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"4f5b92a0fc518b7a85e59cf65285e483b1f9b1c02d080415eb5323a80580971249c919b45b0c0da4448676248716dd80fb45f5be86540d4deb1750bcc3446d90\",\"payment_type\":\"bank_transfer\",\"payment_amounts\":[],\"order_id\":\"ORD-0000478\",\"gross_amount\":\"6300.00\",\"fraud_status\":\"accept\"}','2017-11-29 06:50:52','2017-11-29 06:50:52',NULL),(48,'{\"va_numbers\":[{\"va_number\":\"05135217467\",\"bank\":\"bca\"}],\"transaction_time\":\"2017-11-29 13:50:50\",\"transaction_status\":\"settlement\",\"transaction_id\":\"b271fe23-df43-4f44-b6d3-06413453aed1\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"3d177af2ab6c518d3db3b82b2aa7da3e0368c4ab5a01924b1c592cd447721ac932d521163b6abfa0e2ecdedf906071aa77f32b203eb2f984924116836237f725\",\"payment_type\":\"bank_transfer\",\"payment_amounts\":[],\"order_id\":\"ORD-0000478\",\"gross_amount\":\"6300.00\",\"fraud_status\":\"accept\"}','2017-11-29 06:52:17','2017-11-29 06:52:17',NULL),(49,'{\"transaction_time\":\"2017-11-29 13:54:29\",\"transaction_status\":\"pending\",\"transaction_id\":\"9027dc0f-00eb-4fc4-9a90-0e1a1c40cb30\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"5b5964483782ccf36845e0fb4cf6e9806e8bee782e8a62c24a17eed525969498c711b7e9ac7d1e2d7be28ec4b1f79c274461addd82d97ae5cee9391844b98b1b\",\"payment_type\":\"bca_klikbca\",\"order_id\":\"ORD-0000479\",\"gross_amount\":\"500000.00\",\"approval_code\":\"9027dc0f00eb4fc49a\"}','2017-11-29 06:54:31','2017-11-29 06:54:31',NULL),(50,'{\"transaction_time\":\"2017-11-29 13:54:29\",\"transaction_status\":\"pending\",\"transaction_id\":\"9027dc0f-00eb-4fc4-9a90-0e1a1c40cb30\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"5b5964483782ccf36845e0fb4cf6e9806e8bee782e8a62c24a17eed525969498c711b7e9ac7d1e2d7be28ec4b1f79c274461addd82d97ae5cee9391844b98b1b\",\"payment_type\":\"bca_klikbca\",\"order_id\":\"ORD-0000479\",\"gross_amount\":\"500000.00\",\"approval_code\":\"9027dc0f00eb4fc49a\"}','2017-11-29 06:56:36','2017-11-29 06:56:36',NULL),(51,'{\"transaction_time\":\"2017-11-29 13:56:49\",\"transaction_status\":\"pending\",\"transaction_id\":\"d76c9cb4-9eae-4ea4-9c7f-52ed841fb985\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"b4a68e405b503eb54a8a009ee5dbd485d08709750fb2d1f6ed0df668ed57fe12128a6769ffe257b5817ae39d3f5bc3738d66002a7ebffb78fbc81fad7f919eca\",\"payment_type\":\"bca_klikbca\",\"order_id\":\"ORD-0000480\",\"gross_amount\":\"500000.00\",\"approval_code\":\"d76c9cb49eae4ea49c\"}','2017-11-29 06:56:51','2017-11-29 06:56:51',NULL),(52,'{\"transaction_time\":\"2017-11-29 13:56:49\",\"transaction_status\":\"pending\",\"transaction_id\":\"d76c9cb4-9eae-4ea4-9c7f-52ed841fb985\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"b4a68e405b503eb54a8a009ee5dbd485d08709750fb2d1f6ed0df668ed57fe12128a6769ffe257b5817ae39d3f5bc3738d66002a7ebffb78fbc81fad7f919eca\",\"payment_type\":\"bca_klikbca\",\"order_id\":\"ORD-0000480\",\"gross_amount\":\"500000.00\",\"approval_code\":\"d76c9cb49eae4ea49c\"}','2017-11-29 06:58:47','2017-11-29 06:58:47',NULL),(53,'{\"transaction_time\":\"2017-11-29 13:59:37\",\"transaction_status\":\"pending\",\"transaction_id\":\"938f42b8-48cd-4c69-b859-0d479d289236\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"e19cd7a06ffb49bdd8f40055b255e6a51b27f4be8a5863707600a23060f1d15df8ed08d854a2859b649e860d93b0436523ea5b3c2c955b999ed577f4eb8213a4\",\"payment_type\":\"bca_klikpay\",\"order_id\":\"ORD-0000482\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\"}','2017-11-29 06:59:38','2017-11-29 06:59:38',NULL),(54,'{\"transaction_time\":\"2017-11-29 14:02:24\",\"transaction_status\":\"settlement\",\"transaction_id\":\"ae575c17-dbac-43d2-a4c9-6582088e039e\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"f2adf68c36f45fa9a915b05b3d7a2a55a0ed653352ab36d16437aa4dc6aa23dcc89138cd3574f1ede351d750acafffe32dedcd88d6f99e489af08a60127ddc6d\",\"payment_type\":\"mandiri_clickpay\",\"order_id\":\"ORD-0000483\",\"masked_card\":\"411111-1111\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\",\"approval_code\":\"1511938946253\"}','2017-11-29 07:02:27','2017-11-29 07:02:27',NULL),(55,'{\"transaction_time\":\"2017-11-29 14:03:02\",\"transaction_status\":\"pending\",\"transaction_id\":\"d661486b-391b-4164-b8dc-b178b55a4316\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"6056c390c287fa73f6d092c112ace6f34af8aef70389f07f04219035d21e72b4f84bdad915de34048d0e743a2cd1986e51f6a724257c9056ddb53dc37d693be8\",\"payment_type\":\"cimb_clicks\",\"order_id\":\"ORD-0000484\",\"gross_amount\":\"500000.00\"}','2017-11-29 07:03:04','2017-11-29 07:03:04',NULL),(56,'{\"transaction_time\":\"2017-11-29 14:04:05\",\"transaction_status\":\"pending\",\"transaction_id\":\"a5055839-cd1b-44c3-866b-0bbdd3a6ae19\",\"store\":\"kioson\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"82d2ee0997dbeed7d8d85c8a00b26b8524dcea057e821de0a005d524dd877bc7cc98e6712f53ce345ea493d2902ac05e8f11d60277ef5757d145608a62fec968\",\"payment_type\":\"cstore\",\"payment_code\":\"406229493933\",\"order_id\":\"ORD-0000486\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\"}','2017-11-29 07:04:06','2017-11-29 07:04:06',NULL),(57,'{\"transaction_time\":\"2017-11-29 14:03:02\",\"transaction_status\":\"pending\",\"transaction_id\":\"d661486b-391b-4164-b8dc-b178b55a4316\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"6056c390c287fa73f6d092c112ace6f34af8aef70389f07f04219035d21e72b4f84bdad915de34048d0e743a2cd1986e51f6a724257c9056ddb53dc37d693be8\",\"payment_type\":\"cimb_clicks\",\"order_id\":\"ORD-0000484\",\"gross_amount\":\"500000.00\"}','2017-11-29 07:04:46','2017-11-29 07:04:46',NULL),(58,'{\"transaction_time\":\"2017-11-29 13:59:37\",\"transaction_status\":\"expire\",\"transaction_id\":\"938f42b8-48cd-4c69-b859-0d479d289236\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"202\",\"signature_key\":\"5394669095d42c88ae88f0d316911c2e636ac15f20cbb9f6731e1cab41899e0357693227b85c5c10bc1476c0b23f1373c5b9917a95e5677518367813157e7e1d\",\"payment_type\":\"bca_klikpay\",\"order_id\":\"ORD-0000482\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\"}','2017-11-29 09:00:13','2017-11-29 09:00:13',NULL),(59,'{\"transaction_time\":\"2017-11-28 05:07:20\",\"transaction_status\":\"settlement\",\"transaction_id\":\"5fd8a577-40a5-4408-ae3b-d3daaea919f3\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"263f737b5efaf52a3e38e52bc1e62805b80f698871adbe951249f07666e72bc7a7d93d94cfc6b23f297271d202e32edc4db1de06d2a017a316083472c98fb649\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000443\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511820443765\"}','2017-11-29 09:04:08','2017-11-29 09:04:08',NULL),(60,'{\"transaction_time\":\"2017-11-28 05:14:14\",\"transaction_status\":\"settlement\",\"transaction_id\":\"554d81ce-0dca-467b-8681-4c133fbb7181\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"4e0100edb5ce8ace71437c8a873df72c73852bc9e41de9f391e62fd409845474f343f2a82948067325d425188ffb6b8990cb54ded9c9b13745907d73e3172ee4\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000444\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511820854874\"}','2017-11-29 09:04:08','2017-11-29 09:04:08',NULL),(61,'{\"transaction_time\":\"2017-11-28 05:04:56\",\"transaction_status\":\"settlement\",\"transaction_id\":\"905846c2-685b-4090-869d-c8a11f3fa6d1\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"f79fee118aaeefb714b0c2423655f66937b07f50789963ae09ea3f09dc997e26f24949ef38c9ffcc0512090aee5cd5aaa56ee6b2fafa044f82ce5a5b60438fa7\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000442\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511820300145\"}','2017-11-29 09:04:08','2017-11-29 09:04:08',NULL),(62,'{\"transaction_time\":\"2017-11-28 05:14:14\",\"transaction_status\":\"settlement\",\"transaction_id\":\"554d81ce-0dca-467b-8681-4c133fbb7181\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"4e0100edb5ce8ace71437c8a873df72c73852bc9e41de9f391e62fd409845474f343f2a82948067325d425188ffb6b8990cb54ded9c9b13745907d73e3172ee4\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000444\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511820854874\"}','2017-11-29 09:04:48','2017-11-29 09:04:48',NULL),(63,'{\"transaction_time\":\"2017-11-28 05:04:56\",\"transaction_status\":\"settlement\",\"transaction_id\":\"905846c2-685b-4090-869d-c8a11f3fa6d1\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"f79fee118aaeefb714b0c2423655f66937b07f50789963ae09ea3f09dc997e26f24949ef38c9ffcc0512090aee5cd5aaa56ee6b2fafa044f82ce5a5b60438fa7\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000442\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511820300145\"}','2017-11-29 09:05:09','2017-11-29 09:05:09',NULL),(64,'{\"transaction_time\":\"2017-11-28 05:07:20\",\"transaction_status\":\"settlement\",\"transaction_id\":\"5fd8a577-40a5-4408-ae3b-d3daaea919f3\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"263f737b5efaf52a3e38e52bc1e62805b80f698871adbe951249f07666e72bc7a7d93d94cfc6b23f297271d202e32edc4db1de06d2a017a316083472c98fb649\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000443\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"15000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511820443765\"}','2017-11-29 09:06:26','2017-11-29 09:06:26',NULL),(65,'{\"transaction_time\":\"2017-11-29 14:03:02\",\"transaction_status\":\"expire\",\"transaction_id\":\"d661486b-391b-4164-b8dc-b178b55a4316\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"202\",\"signature_key\":\"c78789afc33280ad145cd6ce6f21ace3a52cdba496bb85b609e824615a224ed47b844a10bbc418dd5f0c3408c562589a194f528aabf5aedbc221d0e250daa09e\",\"payment_type\":\"cimb_clicks\",\"order_id\":\"ORD-0000484\",\"gross_amount\":\"500000.00\"}','2017-11-29 09:07:02','2017-11-29 09:07:02',NULL),(66,'{\"transaction_time\":\"2017-11-29 14:03:02\",\"transaction_status\":\"expire\",\"transaction_id\":\"d661486b-391b-4164-b8dc-b178b55a4316\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"202\",\"signature_key\":\"c78789afc33280ad145cd6ce6f21ace3a52cdba496bb85b609e824615a224ed47b844a10bbc418dd5f0c3408c562589a194f528aabf5aedbc221d0e250daa09e\",\"payment_type\":\"cimb_clicks\",\"order_id\":\"ORD-0000484\",\"gross_amount\":\"500000.00\"}','2017-11-29 09:07:31','2017-11-29 09:07:31',NULL),(67,'{\"transaction_time\":\"2017-11-29 14:04:05\",\"transaction_status\":\"expire\",\"transaction_id\":\"a5055839-cd1b-44c3-866b-0bbdd3a6ae19\",\"store\":\"kioson\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"202\",\"signature_key\":\"94df74cbe088d814b192e66d0e17dba321ce3ece943e8eff5053ad8f053b944428f6e492df11fe61c5b6a19801a326948926f004c30bd4c780bda37c7388059d\",\"payment_type\":\"cstore\",\"payment_code\":\"406229493933\",\"order_id\":\"ORD-0000486\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\"}','2017-11-29 09:15:14','2017-11-29 09:15:14',NULL),(68,'{\"transaction_time\":\"2017-11-30 10:59:11\",\"transaction_status\":\"capture\",\"transaction_id\":\"9f47cf81-603c-4e98-964f-212199e22e40\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"022c76e8ad27ee891c32b42201b979ad43a59c6452bbd28446b92d637dbc824185c5b3ae0018ea766f9107e08be6c9ba2b9ca077d8dfa23ec926f26cb1a707a1\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000489\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1512014351744\"}','2017-11-30 03:59:13','2017-11-30 03:59:13',NULL),(69,'{\"transaction_time\":\"2017-11-29 07:15:29\",\"transaction_status\":\"settlement\",\"transaction_id\":\"165e478b-8a9c-41b5-9415-02f63a85a486\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"ed60ff01b283a03676acd819070372a355cb7d5911f6bbf09bcdba2b8c59a2b4aed093ee7bf9b63b98792fe653c151c86d06ec418dafc337107198e258b0d543\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000448\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"30000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511914529697\"}','2017-11-30 09:04:41','2017-11-30 09:04:41',NULL),(70,'{\"transaction_time\":\"2017-11-29 07:09:50\",\"transaction_status\":\"settlement\",\"transaction_id\":\"bcb6b766-9fb9-4451-b545-6ab8459c5f5c\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"82b61c2caeaec37e744f21258539db3db91a8613fd3a13332406671aee450fa91a9804b13fbd9f8c96203d4bfcbf14d490526fe906d68f764c517eef45640531\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000447\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"30000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511914190633\"}','2017-11-30 09:04:41','2017-11-30 09:04:41',NULL),(71,'{\"transaction_time\":\"2017-11-29 07:15:29\",\"transaction_status\":\"settlement\",\"transaction_id\":\"165e478b-8a9c-41b5-9415-02f63a85a486\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"ed60ff01b283a03676acd819070372a355cb7d5911f6bbf09bcdba2b8c59a2b4aed093ee7bf9b63b98792fe653c151c86d06ec418dafc337107198e258b0d543\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000448\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"30000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511914529697\"}','2017-11-30 09:05:35','2017-11-30 09:05:35',NULL),(72,'{\"transaction_time\":\"2017-11-29 07:09:50\",\"transaction_status\":\"settlement\",\"transaction_id\":\"bcb6b766-9fb9-4451-b545-6ab8459c5f5c\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"82b61c2caeaec37e744f21258539db3db91a8613fd3a13332406671aee450fa91a9804b13fbd9f8c96203d4bfcbf14d490526fe906d68f764c517eef45640531\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000447\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"30000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1511914190633\"}','2017-11-30 09:06:31','2017-11-30 09:06:31',NULL),(73,'{\"transaction_time\":\"2017-11-30 10:59:11\",\"transaction_status\":\"settlement\",\"transaction_id\":\"9f47cf81-603c-4e98-964f-212199e22e40\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"022c76e8ad27ee891c32b42201b979ad43a59c6452bbd28446b92d637dbc824185c5b3ae0018ea766f9107e08be6c9ba2b9ca077d8dfa23ec926f26cb1a707a1\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000489\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1512014351744\"}','2017-12-01 09:03:49','2017-12-01 09:03:49',NULL),(74,'{\"transaction_time\":\"2017-11-30 14:42:46\",\"transaction_status\":\"settlement\",\"transaction_id\":\"e619d4fb-45d8-46c9-bdb5-3529f3900d88\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"85a545e57f9877a475b200b40f676bc21dbb16cc493106223f9b11dd7fa6f3900e2f2203b1c10266e50486a7048200c9b6fbc54debf7c5920a4e5be2b7758d3e\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000491\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1512027767759\"}','2017-12-01 09:03:50','2017-12-01 09:03:50',NULL),(75,'{\"transaction_time\":\"2017-12-04 13:27:31\",\"transaction_status\":\"pending\",\"transaction_id\":\"2397a4d8-720d-440d-b8a9-23a75b1f37b6\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"201\",\"signature_key\":\"3de9340b832db6aaa03592cf91b8b007524e806edd2f1c9dd9802a82ddb3f3e91341e6266f301b8afac5b7991eef49e1b58f9e0ca14fe97206158f523b51338d\",\"payment_type\":\"bri_epay\",\"order_id\":\"ORD-0000492\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\"}','2017-12-04 06:27:34','2017-12-04 06:27:34',NULL),(76,'{\"transaction_time\":\"2017-12-04 13:27:31\",\"transaction_status\":\"expire\",\"transaction_id\":\"2397a4d8-720d-440d-b8a9-23a75b1f37b6\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"202\",\"signature_key\":\"b11f97e06cadba5797249a25d7e6b146a49f97aa50ca3bd9d343c2648586521c700e60bd6dcf6895ed5f68db3da646d6996d7e038954d893037b5c7207a12f49\",\"payment_type\":\"bri_epay\",\"order_id\":\"ORD-0000492\",\"gross_amount\":\"500000.00\",\"fraud_status\":\"accept\"}','2017-12-04 08:30:02','2017-12-04 08:30:02',NULL),(77,'{\"transaction_time\":\"2017-12-05 12:04:03\",\"transaction_status\":\"capture\",\"transaction_id\":\"7ea6003a-b929-48ef-9136-fe2d321a78c4\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"af9e3d74cb64ab8bcd2dc3ea6a695da5dd6e80be9e796733f893dcc2aaf16dbcd9ccf4c570aefd9790d4a872ebfe962fa32acac79eff9cac3f76915dd1c8283a\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000494\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"180000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1512450243781\"}','2017-12-05 05:04:06','2017-12-05 05:04:06',NULL),(78,'{\"transaction_time\":\"2017-12-05 12:04:03\",\"transaction_status\":\"settlement\",\"transaction_id\":\"7ea6003a-b929-48ef-9136-fe2d321a78c4\",\"status_message\":\"Veritrans payment notification\",\"status_code\":\"200\",\"signature_key\":\"af9e3d74cb64ab8bcd2dc3ea6a695da5dd6e80be9e796733f893dcc2aaf16dbcd9ccf4c570aefd9790d4a872ebfe962fa32acac79eff9cac3f76915dd1c8283a\",\"payment_type\":\"credit_card\",\"order_id\":\"ORD-0000494\",\"masked_card\":\"481111-1114\",\"gross_amount\":\"180000.00\",\"fraud_status\":\"accept\",\"eci\":\"05\",\"channel_response_message\":\"Approved\",\"channel_response_code\":\"00\",\"bank\":\"mandiri\",\"approval_code\":\"1512450243781\"}','2017-12-06 09:05:02','2017-12-06 09:05:02',NULL);
/*!40000 ALTER TABLE `log_notif_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(100) NOT NULL,
  `sort_position` int(4) NOT NULL DEFAULT '0',
  `total_submenu` int(2) NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `access_group` int(11) DEFAULT NULL,
  `image` text,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx` (`sort_position`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,'Settings',7,3,'settings',4,'fa fa-cube','2017-03-29 13:28:44','2017-10-14 05:37:20','hilman'),(2,'Parameter',5,5,'parameter',4,'fa fa-cube','2017-07-19 07:10:07','2017-07-21 06:58:06','hilman'),(3,'Product Management',1,1,'product',4,'fa fa-cube','2017-07-19 07:50:29','2017-07-19 07:52:03','hilman'),(4,'Finance',2,0,'finance',4,'fa fa-cube','2017-07-19 07:51:57','2017-12-01 06:30:51','hilman'),(5,'Users Management',3,3,'usermanagement',4,'fa fa-cube','2017-07-19 08:01:27','2017-07-21 06:58:04','hilman'),(7,'Content',4,2,'content',4,'fa fa-cube','2017-07-21 06:57:20','2017-10-06 16:31:32','hilman'),(8,'Member Management',6,1,'member',4,'fa fa-cube','2017-10-14 05:36:40','2017-10-14 05:37:20','hilman');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `merchant`
--

DROP TABLE IF EXISTS `merchant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchant_id` varchar(10) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `zip_code` varchar(10) DEFAULT NULL,
  `balance` int(11) NOT NULL DEFAULT '0',
  `latit` decimal(10,8) DEFAULT NULL,
  `longit` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `image` varchar(255) NOT NULL DEFAULT '0',
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mercant_id_UNIQUE` (`merchant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `merchant`
--

LOCK TABLES `merchant` WRITE;
/*!40000 ALTER TABLE `merchant` DISABLE KEYS */;
INSERT INTO `merchant` VALUES (1,'MCR0000002','KFC Jakarta Pusat',1,'jakarta pusat','021098765',NULL,'kpfjakartapusat@gmail.com','012345',0,40.78086868,-73.88918079,'20171030025353.png','2017-07-21 16:32:08','2017-10-30 15:08:21','hilman'),(2,'MCR0000001','Mcd Depok',2,'Depok','02123456',NULL,'mcd_depok@gmail.com','12333',0,1.00000000,-1.00000000,'20171030025410.png','2017-07-25 16:32:28','2017-10-30 14:54:10','hilman'),(5,'MCR0000003','All Merchant',3,'123','021021',NULL,'all@gmai.com','13860',0,-1.00000000,1.00000000,'0','2017-10-17 11:03:19','2017-10-17 11:03:19','hilman');
/*!40000 ALTER TABLE `merchant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `new_table`
--

DROP TABLE IF EXISTS `new_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `new_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_id` varchar(100) NOT NULL,
  `order_date` date NOT NULL,
  `total_order` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `change_date` datetime NOT NULL,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `new_table`
--

LOCK TABLES `new_table` WRITE;
/*!40000 ALTER TABLE `new_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `new_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `content` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Osai News','Osai Nes Description                         ','Osai News Content                            ','20170726103600.jpg',1,'2017-07-26 10:28:15','2017-07-26 10:36:00','hilman'),(3,'Osai News 2','Osai News 2','Osai News 2','20170809031027.jpeg',1,'2017-08-09 15:10:27','2017-08-09 15:10:27','hilman');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_transaction`
--

DROP TABLE IF EXISTS `payment_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_code` varchar(45) NOT NULL DEFAULT '0',
  `status_message` varchar(100) DEFAULT '',
  `transaction_id` varchar(255) DEFAULT '',
  `masked_card` varchar(100) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT '',
  `payment_type` varchar(100) DEFAULT '',
  `transaction_time` datetime DEFAULT NULL,
  `transaction_status` varchar(20) DEFAULT '',
  `fraud_status` varchar(20) DEFAULT '',
  `approval_code` varchar(255) DEFAULT NULL,
  `signature_key` text,
  `bank` varchar(100) DEFAULT NULL,
  `eci` int(11) DEFAULT NULL,
  `gross_amount` decimal(19,4) DEFAULT NULL,
  `va_number` varchar(100) DEFAULT NULL,
  `biller_code` int(11) DEFAULT NULL,
  `biller_key` varchar(255) DEFAULT NULL,
  `store` varchar(100) DEFAULT NULL,
  `error_messages` varchar(255) DEFAULT NULL,
  `payment_code` varchar(255) DEFAULT NULL,
  `channel_response_message` varchar(255) DEFAULT NULL,
  `channel_response_code` varchar(11) DEFAULT NULL,
  `bill_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ord` (`order_id`),
  KEY `tra_fr` (`transaction_status`,`fraud_status`)
) ENGINE=InnoDB AUTO_INCREMENT=294 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_transaction`
--

LOCK TABLES `payment_transaction` WRITE;
/*!40000 ALTER TABLE `payment_transaction` DISABLE KEYS */;
INSERT INTO `payment_transaction` VALUES (277,'200','Veritrans payment notification','b271fe23-df43-4f44-b6d3-06413453aed1',NULL,'ORD-0000478','bank_transfer','2017-11-29 13:50:50','settlement','accept',NULL,'3d177af2ab6c518d3db3b82b2aa7da3e0368c4ab5a01924b1c592cd447721ac932d521163b6abfa0e2ecdedf906071aa77f32b203eb2f984924116836237f725','bca',NULL,6300.0000,'05135217467',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(278,'407','Success, transaction is found','9027dc0f-00eb-4fc4-9a90-0e1a1c40cb30',NULL,'ORD-0000479','bca_klikbca','2017-11-29 13:54:29','expire','','9027dc0f00eb4fc49a','84c2bed579caa37d19e1ee5f40f2742b8a5cdf84413e1dfdfe5f4650df79f95fa3ba0dec0448ad226ff867e4405a2772fe6e4d4a2bcfe8d7cc6c628c25a53d50',NULL,NULL,500000.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(279,'407','Success, transaction is found','d76c9cb4-9eae-4ea4-9c7f-52ed841fb985',NULL,'ORD-0000480','bca_klikbca','2017-11-29 13:56:49','expire','','d76c9cb49eae4ea49c','9ed1f33967d34900e3d8004e0ade9e3268d42f02d8f367ec71fd39181913ebadcfc10a03e3c03a3bd2e5fa46b3edee0d0b4180c0b50f14151628f425b1419f81',NULL,NULL,500000.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(280,'200','Veritrans payment notification','d517ccd7-8eb7-4d93-9ef2-b9e3e458c904',NULL,'ORD-0000431','echannel','2017-11-24 14:15:11','settlement','accept',NULL,'c1a6265b4731dd43dd78d53ab6a9d09df4b20a1c61ea4c05477a5618e7b498aed984ef2b1ff0347957ae3121208e14c9724b61c0125699c4766d7aefe59a4bdb',NULL,NULL,1200000.0000,NULL,70012,NULL,NULL,NULL,NULL,NULL,NULL,'122586852231'),(281,'202','Veritrans payment notification','938f42b8-48cd-4c69-b859-0d479d289236',NULL,'ORD-0000482','bca_klikpay','2017-11-29 13:59:37','expire','accept',NULL,'5394669095d42c88ae88f0d316911c2e636ac15f20cbb9f6731e1cab41899e0357693227b85c5c10bc1476c0b23f1373c5b9917a95e5677518367813157e7e1d',NULL,NULL,500000.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(282,'200','Veritrans payment notification','ae575c17-dbac-43d2-a4c9-6582088e039e','411111-1111','ORD-0000483','mandiri_clickpay','2017-11-29 14:02:24','settlement','accept','1511938946253','f2adf68c36f45fa9a915b05b3d7a2a55a0ed653352ab36d16437aa4dc6aa23dcc89138cd3574f1ede351d750acafffe32dedcd88d6f99e489af08a60127ddc6d',NULL,NULL,500000.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(283,'202','Veritrans payment notification','d661486b-391b-4164-b8dc-b178b55a4316',NULL,'ORD-0000484','cimb_clicks','2017-11-29 14:03:02','expire','',NULL,'c78789afc33280ad145cd6ce6f21ace3a52cdba496bb85b609e824615a224ed47b844a10bbc418dd5f0c3408c562589a194f528aabf5aedbc221d0e250daa09e',NULL,NULL,500000.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(284,'202','Veritrans payment notification','a5055839-cd1b-44c3-866b-0bbdd3a6ae19',NULL,'ORD-0000486','cstore','2017-11-29 14:04:05','expire','accept',NULL,'94df74cbe088d814b192e66d0e17dba321ce3ece943e8eff5053ad8f053b944428f6e492df11fe61c5b6a19801a326948926f004c30bd4c780bda37c7388059d',NULL,NULL,500000.0000,NULL,NULL,NULL,'kioson',NULL,'406229493933',NULL,NULL,NULL),(285,'200','Veritrans payment notification','5fd8a577-40a5-4408-ae3b-d3daaea919f3','481111-1114','ORD-0000443','credit_card','2017-11-28 05:07:20','settlement','accept','1511820443765','263f737b5efaf52a3e38e52bc1e62805b80f698871adbe951249f07666e72bc7a7d93d94cfc6b23f297271d202e32edc4db1de06d2a017a316083472c98fb649','mandiri',5,15000.0000,NULL,NULL,NULL,NULL,NULL,NULL,'Approved','00',NULL),(286,'200','Veritrans payment notification','554d81ce-0dca-467b-8681-4c133fbb7181','481111-1114','ORD-0000444','credit_card','2017-11-28 05:14:14','settlement','accept','1511820854874','4e0100edb5ce8ace71437c8a873df72c73852bc9e41de9f391e62fd409845474f343f2a82948067325d425188ffb6b8990cb54ded9c9b13745907d73e3172ee4','mandiri',5,15000.0000,NULL,NULL,NULL,NULL,NULL,NULL,'Approved','00',NULL),(287,'200','Veritrans payment notification','905846c2-685b-4090-869d-c8a11f3fa6d1','481111-1114','ORD-0000442','credit_card','2017-11-28 05:04:56','settlement','accept','1511820300145','f79fee118aaeefb714b0c2423655f66937b07f50789963ae09ea3f09dc997e26f24949ef38c9ffcc0512090aee5cd5aaa56ee6b2fafa044f82ce5a5b60438fa7','mandiri',5,15000.0000,NULL,NULL,NULL,NULL,NULL,NULL,'Approved','00',NULL),(288,'200','Veritrans payment notification','9f47cf81-603c-4e98-964f-212199e22e40','481111-1114','ORD-0000489','credit_card','2017-11-30 10:59:11','settlement','accept','1512014351744','022c76e8ad27ee891c32b42201b979ad43a59c6452bbd28446b92d637dbc824185c5b3ae0018ea766f9107e08be6c9ba2b9ca077d8dfa23ec926f26cb1a707a1','mandiri',5,500000.0000,NULL,NULL,NULL,NULL,NULL,NULL,'Approved','00',NULL),(289,'200','Veritrans payment notification','e619d4fb-45d8-46c9-bdb5-3529f3900d88','481111-1114','ORD-0000491','credit_card','2017-11-30 14:42:46','settlement','accept','1512027767759','85a545e57f9877a475b200b40f676bc21dbb16cc493106223f9b11dd7fa6f3900e2f2203b1c10266e50486a7048200c9b6fbc54debf7c5920a4e5be2b7758d3e','mandiri',5,500000.0000,NULL,NULL,NULL,NULL,NULL,NULL,'Approved','00',NULL),(290,'200','Veritrans payment notification','165e478b-8a9c-41b5-9415-02f63a85a486','481111-1114','ORD-0000448','credit_card','2017-11-29 07:15:29','settlement','accept','1511914529697','ed60ff01b283a03676acd819070372a355cb7d5911f6bbf09bcdba2b8c59a2b4aed093ee7bf9b63b98792fe653c151c86d06ec418dafc337107198e258b0d543','mandiri',5,30000.0000,NULL,NULL,NULL,NULL,NULL,NULL,'Approved','00',NULL),(291,'200','Veritrans payment notification','bcb6b766-9fb9-4451-b545-6ab8459c5f5c','481111-1114','ORD-0000447','credit_card','2017-11-29 07:09:50','settlement','accept','1511914190633','82b61c2caeaec37e744f21258539db3db91a8613fd3a13332406671aee450fa91a9804b13fbd9f8c96203d4bfcbf14d490526fe906d68f764c517eef45640531','mandiri',5,30000.0000,NULL,NULL,NULL,NULL,NULL,NULL,'Approved','00',NULL),(292,'202','Veritrans payment notification','2397a4d8-720d-440d-b8a9-23a75b1f37b6',NULL,'ORD-0000492','bri_epay','2017-12-04 13:27:31','expire','accept',NULL,'b11f97e06cadba5797249a25d7e6b146a49f97aa50ca3bd9d343c2648586521c700e60bd6dcf6895ed5f68db3da646d6996d7e038954d893037b5c7207a12f49',NULL,NULL,500000.0000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(293,'200','Veritrans payment notification','7ea6003a-b929-48ef-9136-fe2d321a78c4','481111-1114','ORD-0000494','credit_card','2017-12-05 12:04:03','settlement','accept','1512450243781','af9e3d74cb64ab8bcd2dc3ea6a695da5dd6e80be9e796733f893dcc2aaf16dbcd9ccf4c570aefd9790d4a872ebfe962fa32acac79eff9cac3f76915dd1c8283a','mandiri',5,180000.0000,NULL,NULL,NULL,NULL,NULL,NULL,'Approved','00',NULL);
/*!40000 ALTER TABLE `payment_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `point_history`
--

DROP TABLE IF EXISTS `point_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `point_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `mutation` int(11) DEFAULT NULL,
  `sent_by` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `invoice_number` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `point_history`
--

LOCK TABLES `point_history` WRITE;
/*!40000 ALTER TABLE `point_history` DISABLE KEYS */;
INSERT INTO `point_history` VALUES (220,71,100,1,NULL,1,'INV-0000420','2017-11-29 13:52:17','2017-11-29 13:52:17','71'),(221,71,100,1,NULL,1,'INV-0000420','2017-11-29 13:52:17','2017-11-29 13:52:17','71'),(222,71,100,1,NULL,1,'INV-0000424','2017-11-29 14:02:27','2017-11-29 14:02:27','71'),(223,76,100,1,NULL,1,'INV-0000427','2017-11-30 10:59:13','2017-11-30 10:59:13','76'),(224,76,100,1,NULL,1,'INV-0000428','2017-12-01 13:02:49','2017-12-01 13:02:49','76'),(225,77,200,1,72,1,NULL,'2017-12-01 17:50:25','2017-12-01 17:50:25',NULL),(226,72,200,0,77,1,NULL,'2017-12-01 17:50:25','2017-12-01 17:50:25',NULL);
/*!40000 ALTER TABLE `point_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_invoice_number`
--

DROP TABLE IF EXISTS `seq_invoice_number`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_invoice_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_user_UNIQUE` (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=431 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_invoice_number`
--

LOCK TABLES `seq_invoice_number` WRITE;
/*!40000 ALTER TABLE `seq_invoice_number` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_invoice_number` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_no_merchant`
--

DROP TABLE IF EXISTS `seq_no_merchant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_no_merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_no_merchant`
--

LOCK TABLES `seq_no_merchant` WRITE;
/*!40000 ALTER TABLE `seq_no_merchant` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_no_merchant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_no_voucher`
--

DROP TABLE IF EXISTS `seq_no_voucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_no_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=251 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_no_voucher`
--

LOCK TABLES `seq_no_voucher` WRITE;
/*!40000 ALTER TABLE `seq_no_voucher` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_no_voucher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seq_order_id`
--

DROP TABLE IF EXISTS `seq_order_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seq_order_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=496 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seq_order_id`
--

LOCK TABLES `seq_order_id` WRITE;
/*!40000 ALTER TABLE `seq_order_id` DISABLE KEYS */;
/*!40000 ALTER TABLE `seq_order_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `state_code` varchar(10) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statistic_data`
--

DROP TABLE IF EXISTS `statistic_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statistic_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `voucher_id` int(11) DEFAULT NULL,
  `total_view` int(11) DEFAULT NULL,
  `total_search` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statistic_data`
--

LOCK TABLES `statistic_data` WRITE;
/*!40000 ALTER TABLE `statistic_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `statistic_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_menu`
--

DROP TABLE IF EXISTS `sub_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(4) DEFAULT NULL,
  `submenu_name` varchar(100) NOT NULL,
  `sort_position` int(11) NOT NULL,
  `route_submenu` text NOT NULL,
  `content` text,
  `image` text,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inx` (`id_menu`,`sort_position`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_menu`
--

LOCK TABLES `sub_menu` WRITE;
/*!40000 ALTER TABLE `sub_menu` DISABLE KEYS */;
INSERT INTO `sub_menu` VALUES (1,1,'User Management',0,'userManagement','settings/userManagement',NULL,'2017-03-29 13:32:17','2017-07-19 07:52:17','hilman'),(2,1,'Group Management',1,'groupManagement','settings/groupManagement',NULL,'2017-04-10 10:37:28','2017-07-19 07:52:17','hilman'),(3,1,'Menu Management',0,'menuManagement','settings/menuManagement',NULL,'2017-04-10 10:37:28','2017-06-16 19:42:34','hilman'),(4,2,'City',0,'city','parameter/city',NULL,'2017-07-19 07:10:56','2017-07-19 07:10:56','hilman'),(5,2,'Company',0,'company','parameter/company',NULL,'2017-07-19 07:12:07','2017-07-19 07:12:07','hilman'),(6,2,'State',0,'state','parameter/state',NULL,'2017-07-19 07:12:36','2017-07-19 07:12:36','hilman'),(7,3,'Voucher',0,'voucher','product/voucher',NULL,'2017-07-19 07:50:49','2017-07-19 07:50:49','hilman'),(8,4,'Invoice History',0,'invoiceHistory','finance/invoiceHistory',NULL,'2017-07-19 07:53:02','2017-07-19 07:53:02','hilman'),(9,5,'Mercant Users',0,'mercantUser','usermanagement/mercantUser',NULL,'2017-07-19 08:02:50','2017-07-19 08:02:50','hilman'),(10,5,'Cashier',0,'cashier','usermanagement/cashier',NULL,'2017-07-21 01:56:04','2017-07-21 01:56:04','hilman'),(11,2,'Merchant',0,'merchant','parameter/merchant',NULL,'2017-07-21 01:57:34','2017-07-21 01:57:34','hilman'),(12,5,'Users',0,'users','usermanagement/users',NULL,'2017-07-21 02:16:42','2017-07-21 02:16:42','hilman'),(13,2,'Category Voucher',0,'categoryVoucher','parameter/categoryVoucher',NULL,'2017-07-21 04:07:16','2017-07-21 04:07:16','hilman'),(14,7,'News',0,'news','content/news',NULL,'2017-07-21 06:57:32','2017-07-21 06:57:32','hilman'),(15,7,'Slider',0,'slider','content/slider',NULL,'2017-10-06 16:31:32','2017-10-06 16:31:32','hilman'),(16,8,'Member List',0,'memberList','member/memberList',NULL,'2017-10-14 05:37:04','2017-10-14 05:37:04','hilman'),(17,4,'Withdraw',0,'withdraw','finance/withdraw',NULL,'2017-10-23 15:25:31','2017-10-23 15:25:31','hilman'),(20,4,'Kurs Point',0,'kursPoint','finance/kursPoint',NULL,'2017-12-01 06:30:51','2017-12-01 06:30:51','hilman');
/*!40000 ALTER TABLE `sub_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_order`
--

DROP TABLE IF EXISTS `tbl_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) DEFAULT NULL,
  `voucher_id` varchar(255) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `point` int(11) NOT NULL DEFAULT '0',
  `customer_id` int(255) DEFAULT NULL,
  `status_order` varchar(45) NOT NULL DEFAULT '0',
  `order_date` datetime DEFAULT NULL,
  `information` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=399 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_order`
--

LOCK TABLES `tbl_order` WRITE;
/*!40000 ALTER TABLE `tbl_order` DISABLE KEYS */;
INSERT INTO `tbl_order` VALUES (379,'ORD-0000476','VCR-0000017',1,1,1,3150,0,71,'0','2017-11-29 13:45:52',NULL,'2017-11-29 13:45:52','2017-11-29 13:45:52','71'),(380,'ORD-0000477','VCR-0000017',1,1,2,6300,0,71,'0','2017-11-29 13:47:14',NULL,'2017-11-29 13:47:14','2017-11-29 13:47:14','71'),(381,'ORD-0000478','VCR-0000017',1,1,2,6300,0,71,'0','2017-11-29 13:47:35',NULL,'2017-11-29 13:47:35','2017-11-29 13:47:35','71'),(382,'ORD-0000479','VCR-0000011',1,1,1,500000,0,71,'0','2017-11-29 13:53:08',NULL,'2017-11-29 13:53:08','2017-11-29 13:53:08','71'),(383,'ORD-0000480','VCR-0000011',1,1,1,500000,0,71,'0','2017-11-29 13:55:01',NULL,'2017-11-29 13:55:01','2017-11-29 13:55:01','71'),(384,'ORD-0000481','VCR-0000011',1,1,1,500000,0,71,'0','2017-11-29 13:58:16',NULL,'2017-11-29 13:58:16','2017-11-29 13:58:16','71'),(385,'ORD-0000482','VCR-0000011',1,1,1,500000,0,71,'0','2017-11-29 13:59:18',NULL,'2017-11-29 13:59:18','2017-11-29 13:59:18','71'),(386,'ORD-0000483','VCR-0000011',1,1,1,500000,0,71,'0','2017-11-29 14:00:10',NULL,'2017-11-29 14:00:10','2017-11-29 14:00:10','71'),(387,'ORD-0000484','VCR-0000011',1,1,1,500000,0,71,'0','2017-11-29 14:02:37',NULL,'2017-11-29 14:02:37','2017-11-29 14:02:37','71'),(388,'ORD-0000485','VCR-0000011',1,1,1,500000,0,71,'0','2017-11-29 14:03:07',NULL,'2017-11-29 14:03:07','2017-11-29 14:03:07','71'),(389,'ORD-0000486','VCR-0000011',1,1,1,500000,0,71,'0','2017-11-29 14:03:51',NULL,'2017-11-29 14:03:51','2017-11-29 14:03:51','71'),(390,'ORD-0000487','VCR-0000250',1,1,1,15000,100,76,'0','2017-11-30 10:54:26',NULL,'2017-11-30 10:54:26','2017-11-30 10:54:26','76'),(391,'ORD-0000488','VCR-0000011',1,1,1,500000,0,80,'0','2017-11-30 10:56:36',NULL,'2017-11-30 10:56:36','2017-11-30 10:56:36','80'),(392,'ORD-0000489','VCR-0000011',1,1,1,500000,0,76,'0','2017-11-30 10:56:43',NULL,'2017-11-30 10:56:43','2017-11-30 10:56:43','76'),(393,'ORD-0000490','VCR-0000011',1,1,1,500000,0,76,'0','2017-11-30 11:15:14',NULL,'2017-11-30 11:15:14','2017-11-30 11:15:14','76'),(394,'ORD-0000491','VCR-0000011',1,1,1,500000,0,76,'0','2017-11-30 14:41:51',NULL,'2017-11-30 14:41:51','2017-11-30 14:41:51','76'),(395,'ORD-0000492','VCR-0000011',1,1,1,500000,0,76,'0','2017-12-04 13:23:43',NULL,'2017-12-04 13:23:43','2017-12-04 13:23:43','76'),(396,'ORD-0000493','VCR-0000011',1,1,1,500000,0,76,'0','2017-12-04 13:27:38',NULL,'2017-12-04 13:27:38','2017-12-04 13:27:38','76'),(397,'ORD-0000494','VCR-0000003',1,1,1,180000,0,76,'0','2017-12-05 12:02:57',NULL,'2017-12-05 12:02:57','2017-12-05 12:02:57','76'),(398,'ORD-0000495','VCR-0000011',1,1,2,1000000,0,76,'0','2017-12-06 20:03:20',NULL,'2017-12-06 20:03:20','2017-12-06 20:03:20','76');
/*!40000 ALTER TABLE `tbl_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ref`
--

DROP TABLE IF EXISTS `tbl_ref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_ref` int(4) NOT NULL,
  `id_ref` int(11) DEFAULT NULL,
  `content` varchar(255) DEFAULT '',
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `inx` (`group_ref`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ref`
--

LOCK TABLES `tbl_ref` WRITE;
/*!40000 ALTER TABLE `tbl_ref` DISABLE KEYS */;
INSERT INTO `tbl_ref` VALUES (1,1,1,'Money',NULL,'2017-10-05 03:45:51',''),(2,1,2,'Point',NULL,'2017-10-05 03:46:20',''),(3,1,3,'Money / Point',NULL,'2017-10-05 03:46:40',''),(4,1,4,'Money + Point',NULL,'2017-10-06 17:43:58',''),(5,2,1,'Bonus',NULL,'2017-10-14 05:47:45',''),(6,2,2,'Transfer',NULL,'2017-10-14 05:48:02',''),(7,3,1,'Requesting',NULL,'2017-10-23 15:52:55',''),(8,3,2,'Approved',NULL,'2017-10-23 15:53:11',''),(9,3,3,'Rejected',NULL,'2017-10-23 15:53:29',''),(10,4,0,'Female',NULL,'2017-10-24 22:33:58',''),(11,4,1,'Male',NULL,'2017-10-24 22:34:08','');
/*!40000 ALTER TABLE `tbl_ref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_group`
--

DROP TABLE IF EXISTS `user_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `level_user` int(11) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  `id_mercant` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_group`
--

LOCK TABLES `user_group` WRITE;
/*!40000 ALTER TABLE `user_group` DISABLE KEYS */;
INSERT INTO `user_group` VALUES (1,'Super Administrator',1,NULL,NULL,'2017-03-29 11:18:57','2017-07-21 02:06:07','hilman'),(7,'Developer',1,NULL,NULL,'2017-04-19 11:39:11','2017-04-19 11:40:07','hilman'),(9,'Administrator Company',1,NULL,NULL,'2017-07-21 02:06:29','2017-07-21 02:06:29','hilman'),(10,'Administrator Mercant',2,NULL,NULL,'2017-07-21 02:06:41','2017-07-21 02:06:41','hilman'),(11,'Cashier',3,NULL,NULL,'2017-07-21 02:11:39','2017-07-21 02:11:39','hilman'),(12,'Manager Company',2,NULL,NULL,'2017-07-21 02:11:59','2017-07-21 02:13:06','hilman'),(13,'Manager Mercant',3,NULL,NULL,'2017-07-21 02:13:16','2017-07-21 02:13:16','hilman'),(14,'Staff',NULL,NULL,NULL,'2017-09-27 07:42:25','2017-09-27 07:42:25','hilman');
/*!40000 ALTER TABLE `user_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_priviledge`
--

DROP TABLE IF EXISTS `user_priviledge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_priviledge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_submenu` int(11) NOT NULL,
  `access_grant` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` varchar(100) NOT NULL,
  PRIMARY KEY (`id`,`id_user`,`id_menu`,`id_submenu`,`change_by`),
  KEY `idx` (`id_user`,`id_menu`,`id_submenu`,`change_by`)
) ENGINE=InnoDB AUTO_INCREMENT=2278 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_priviledge`
--

LOCK TABLES `user_priviledge` WRITE;
/*!40000 ALTER TABLE `user_priviledge` DISABLE KEYS */;
INSERT INTO `user_priviledge` VALUES (1671,9,1,1,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1672,9,1,1,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1673,9,1,1,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1674,9,1,2,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1675,9,1,2,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1676,9,1,2,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1677,9,1,3,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1678,9,1,3,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1679,9,1,3,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1680,9,2,4,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1681,9,2,4,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1682,9,2,4,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1683,9,2,5,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1684,9,2,5,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1685,9,2,5,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1686,9,2,6,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1687,9,2,6,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1688,9,2,6,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1689,9,2,11,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1690,9,2,11,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1691,9,2,11,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1692,9,2,13,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1693,9,2,13,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1694,9,2,13,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1695,9,3,7,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1696,9,3,7,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1697,9,3,7,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1698,9,4,8,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1699,9,4,8,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1700,9,4,8,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1701,9,5,9,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1702,9,5,9,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1703,9,5,9,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1704,9,5,10,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1705,9,5,10,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1706,9,5,10,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1707,9,5,12,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1708,9,5,12,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1709,9,5,12,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1710,9,7,14,1,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1711,9,7,14,2,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1712,9,7,14,4,'2017-07-26 02:22:42','2017-07-26 02:22:42','hilman'),(1755,10,1,1,1,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1756,10,1,1,2,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1757,10,1,1,4,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1758,10,2,11,1,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1759,10,2,11,2,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1760,10,2,11,4,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1761,10,3,7,1,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1762,10,3,7,2,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1763,10,3,7,4,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1764,10,4,8,1,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1765,10,4,8,2,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1766,10,4,8,4,'2017-07-26 02:22:54','2017-07-26 02:22:54','hilman'),(1776,11,1,1,1,'2017-07-26 02:23:09','2017-07-26 02:23:09','hilman'),(1777,11,3,7,1,'2017-07-26 02:23:09','2017-07-26 02:23:09','hilman'),(1778,11,4,8,1,'2017-07-26 02:23:09','2017-07-26 02:23:09','hilman'),(1842,16,1,1,1,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1843,16,1,1,2,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1844,16,1,1,4,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1845,16,2,11,1,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1846,16,2,11,2,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1847,16,2,11,4,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1848,16,3,7,1,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1849,16,3,7,2,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1850,16,3,7,4,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1851,16,4,8,1,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1852,16,4,8,2,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1853,16,4,8,4,'2017-07-26 02:23:43','2017-07-26 02:23:43','hilman'),(1866,17,1,1,1,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1867,17,1,1,2,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1868,17,1,1,4,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1869,17,3,7,1,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1870,17,3,7,2,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1871,17,3,7,4,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1872,17,4,8,1,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1873,17,4,8,2,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1874,17,4,8,4,'2017-07-26 02:23:54','2017-07-26 02:23:54','hilman'),(1884,18,4,8,1,'2017-07-26 02:24:06','2017-07-26 02:24:06','hilman'),(1885,18,4,8,2,'2017-07-26 02:24:06','2017-07-26 02:24:06','hilman'),(1886,18,4,8,4,'2017-07-26 02:24:06','2017-07-26 02:24:06','hilman'),(1899,19,2,11,1,'2017-07-26 02:29:43','2017-07-26 02:29:43','hilman'),(1900,19,3,7,1,'2017-07-26 02:29:43','2017-07-26 02:29:43','hilman'),(1901,19,4,8,1,'2017-07-26 02:29:43','2017-07-26 02:29:43','hilman'),(1938,36,3,7,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1939,36,3,7,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1940,36,3,7,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1941,36,4,8,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1942,36,4,8,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1943,36,4,8,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1944,36,5,9,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1945,36,5,9,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1946,36,5,9,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1947,36,5,10,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1948,36,5,10,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1949,36,5,10,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1950,36,5,12,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1951,36,5,12,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1952,36,5,12,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1953,36,7,14,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1954,36,7,14,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1955,36,7,14,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1956,36,2,4,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1957,36,2,4,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1958,36,2,4,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1959,36,2,5,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1960,36,2,5,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1961,36,2,5,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1962,36,2,6,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1963,36,2,6,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1964,36,2,6,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1965,36,2,11,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1966,36,2,11,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1967,36,2,11,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1968,36,2,13,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1969,36,2,13,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1970,36,2,13,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1971,36,1,1,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1972,36,1,1,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1973,36,1,1,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1974,36,1,3,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1975,36,1,3,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1976,36,1,3,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1977,36,1,2,1,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1978,36,1,2,2,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(1979,36,1,2,4,'2017-09-14 01:13:55','2017-09-14 01:13:55','admin'),(2178,12,3,7,1,'2017-11-17 07:16:02','2017-11-17 07:16:02','admin_kfc'),(2179,12,3,7,2,'2017-11-17 07:16:02','2017-11-17 07:16:02','admin_kfc'),(2180,12,3,7,4,'2017-11-17 07:16:02','2017-11-17 07:16:02','admin_kfc'),(2181,12,4,8,1,'2017-11-17 07:16:02','2017-11-17 07:16:02','admin_kfc'),(2182,12,4,8,2,'2017-11-17 07:16:02','2017-11-17 07:16:02','admin_kfc'),(2183,12,4,8,4,'2017-11-17 07:16:02','2017-11-17 07:16:02','admin_kfc'),(2184,12,4,17,1,'2017-11-17 07:16:02','2017-11-17 07:16:02','admin_kfc'),(2185,12,1,1,1,'2017-11-17 07:16:02','2017-11-17 07:16:02','admin_kfc'),(2186,12,1,1,2,'2017-11-17 07:16:02','2017-11-17 07:16:02','admin_kfc'),(2187,12,1,1,4,'2017-11-17 07:16:02','2017-11-17 07:16:02','admin_kfc'),(2233,1,3,7,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2234,1,3,7,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2235,1,3,7,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2236,1,4,8,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2237,1,4,8,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2238,1,4,8,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2239,1,4,17,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2240,1,4,17,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2241,1,4,17,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2242,1,4,20,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2243,1,4,20,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2244,1,4,20,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2245,1,7,14,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2246,1,7,14,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2247,1,7,14,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2248,1,7,15,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2249,1,7,15,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2250,1,7,15,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2251,1,2,4,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2252,1,2,4,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2253,1,2,4,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2254,1,2,5,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2255,1,2,5,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2256,1,2,5,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2257,1,2,6,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2258,1,2,6,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2259,1,2,6,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2260,1,2,11,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2261,1,2,11,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2262,1,2,11,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2263,1,2,13,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2264,1,2,13,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2265,1,2,13,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2266,1,8,16,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2267,1,8,16,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2268,1,8,16,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2269,1,1,1,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2270,1,1,1,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2271,1,1,1,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2272,1,1,3,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2273,1,1,3,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2274,1,1,3,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2275,1,1,2,1,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2276,1,1,2,2,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman'),(2277,1,1,2,4,'2017-12-01 06:31:40','2017-12-01 06:31:40','hilman');
/*!40000 ALTER TABLE `user_priviledge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `digital_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `birthdate` date DEFAULT NULL,
  `user_group` int(11) NOT NULL DEFAULT '0',
  `foto` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `level_user` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` int(11) DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fcm_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `saldo` int(11) NOT NULL DEFAULT '0',
  `point` int(11) NOT NULL DEFAULT '0',
  `gender` int(11) DEFAULT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `change_date` timestamp NULL DEFAULT NULL,
  `change_by` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stat_notif` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `idc_fcm` (`fcm_token`),
  KEY `login` (`name`),
  KEY `login2` (`email`),
  KEY `login3` (`username`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'71710250553541','hilman','hilman','hilman','$2y$10$6Oe/fGSWzRyNKNhDO1wx0exeugHPcLluZncIexGjzs8CusVPWLmH2',NULL,7,'20170311084801.png',1,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,'',0,0,0,'2017-03-30 07:50:13','2017-08-09 00:41:30','hilman',0),(9,'71710250553549','Fitrah Ramadhan','fitrah','fitrah','$2y$10$40Ocs93gV3m2CR2hfXPlJeGeUPnB9KfkFudByskPXAqcdpF0VxcNK',NULL,7,NULL,1,1,0,NULL,NULL,NULL,'',NULL,NULL,NULL,'111',0,100,0,'2017-06-15 06:58:18','2017-10-06 16:42:05','hilman',0),(10,'717102505535410','Admin Company KFC','admin_company','admin_company','$2y$10$6Oe/fGSWzRyNKNhDO1wx0exeugHPcLluZncIexGjzs8CusVPWLmH2',NULL,9,NULL,1,2,1,NULL,NULL,NULL,'',NULL,NULL,NULL,'111',0,0,0,'2017-07-21 02:19:26','2017-07-26 02:22:54','hilman',0),(11,'717102505535411','Manager Merchant KFC','manager_merchant','manager_merchant','$2y$10$6oeIaSKVatfIjTKAeAfFV.1V3Nj3UvjvqdGArQoSRPMuSsJKXKpua',NULL,13,NULL,1,3,1,NULL,NULL,1,'',NULL,NULL,NULL,'111',0,0,0,'2017-07-21 09:44:38','2017-07-26 02:23:09','hilman',0),(12,'717102505535412','Admin Merchant KFC','admin_kfc','admin_kfc','$2y$10$6Oe/fGSWzRyNKNhDO1wx0exeugHPcLluZncIexGjzs8CusVPWLmH2',NULL,10,NULL,1,3,1,NULL,NULL,1,'',NULL,NULL,NULL,'111',0,0,0,'2017-07-25 09:07:52','2017-07-26 02:23:23','hilman',0),(15,'717102505535415','Fitrah Cashier KFC','cashier','cashier','$2y$10$h5h7hambUqxMRqpyLJtH4OH2oF1GzMec4Z4jmpS2r1M/fNQVja2si',NULL,11,NULL,1,3,1,NULL,NULL,1,'',NULL,NULL,NULL,'111',0,0,0,'2017-07-25 09:20:42','2017-07-25 09:24:27','hilman',0),(16,'717102505535416','Admin Company MCD','admin_mcd','admin_mcd','$2y$10$6Oe/fGSWzRyNKNhDO1wx0exeugHPcLluZncIexGjzs8CusVPWLmH2',NULL,9,NULL,1,2,2,NULL,NULL,NULL,'',NULL,NULL,NULL,'111',0,0,0,'2017-07-25 09:25:52','2017-07-26 02:23:43','hilman',0),(17,'717102505535417','Admin Merchant MCD','admin_merchant_mcd','admin_merchant_mcd','$2y$10$6Oe/fGSWzRyNKNhDO1wx0exeugHPcLluZncIexGjzs8CusVPWLmH2',NULL,10,NULL,1,3,2,NULL,NULL,2,'',NULL,NULL,NULL,'111',0,0,0,'2017-07-25 09:32:54','2017-09-07 22:50:12','hilman',0),(18,'717102505535418','Yunan Cashier MCD','cashier_mcd','cashier_mcd','$2y$10$6Oe/fGSWzRyNKNhDO1wx0exeugHPcLluZncIexGjzs8CusVPWLmH2',NULL,11,NULL,1,3,2,NULL,NULL,2,'',NULL,NULL,NULL,'111',0,0,0,'2017-07-25 09:37:34','2017-07-26 02:24:06','hilman',0),(19,'717102505535419','Manager Company KFC','manager_company','manager_company','$2y$10$6Oe/fGSWzRyNKNhDO1wx0exeugHPcLluZncIexGjzs8CusVPWLmH2',NULL,12,NULL,1,2,1,NULL,NULL,NULL,'',NULL,NULL,NULL,'111',0,0,0,'2017-07-26 02:26:21','2017-07-26 02:29:43','hilman',0),(36,'717102505535436','admin','admin','admin','$2y$10$6Oe/fGSWzRyNKNhDO1wx0exeugHPcLluZncIexGjzs8CusVPWLmH2',NULL,1,NULL,1,1,0,NULL,NULL,NULL,'0',NULL,NULL,NULL,'111',0,0,0,'2017-09-14 01:13:18','2017-09-14 01:13:18','hilman',0),(70,'20171107225525','Test app','testaja@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'106741432149771883178',NULL,NULL,NULL,'',0,0,NULL,NULL,NULL,NULL,0),(71,'20171107230542','Fitrah Ramadhan','fitrah.ramadhan26@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'0','1499722143415205',NULL,NULL,'fYQBmKNrDGI:APA91bEtgjy4yVVd2jiM7Lwi-Yco4hk-T5ivHLtKCnd500jrt-zn9de95rBOAfLnXpjCKnp7I_gyeQrpY7ekApRD3Yf8ejM03mUR4jvTslPetlGxMn2dLMkMukBL8A6zDNhvwPBARO6B',0,4500,NULL,NULL,NULL,NULL,0),(72,'20171107231124','Hilman Syafei','imanbellamy@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'0','10203889993646211',NULL,NULL,'dCMysloaXX4:APA91bFxoehECOch8CsiVLQ1MpIKKkGOOjxnJ4K5u2Y1LCBAiVmN-JnrrRmCDhCw3-g1wGoHfH2pW1hhjiBDyBFNf4v1CjMXvbob2tNudjoIjjcTpIZLzNr9xM6cPlxDSOcKWQAyP7Ya',0,39800,NULL,NULL,NULL,NULL,0),(73,'20171107235420','hilman syafei','syafeihilman88@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'118393590578263811355',NULL,NULL,NULL,'',0,300000,NULL,NULL,NULL,NULL,0),(74,'20171108104026','Fitrah Ramadhan','fitrahdev@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'112310124856796951474',NULL,NULL,NULL,'fYQBmKNrDGI:APA91bEtgjy4yVVd2jiM7Lwi-Yco4hk-T5ivHLtKCnd500jrt-zn9de95rBOAfLnXpjCKnp7I_gyeQrpY7ekApRD3Yf8ejM03mUR4jvTslPetlGxMn2dLMkMukBL8A6zDNhvwPBARO6B',0,38424,NULL,NULL,NULL,NULL,0),(75,'20171108104047','Putri nur Hasanah','puputahmad6900@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'106042636992072479768',NULL,NULL,NULL,'',0,959876,NULL,NULL,NULL,NULL,0),(76,'20171109195336','Faustinus Wirasadi','faustinus.wirasadi@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'0','10214895785135243',NULL,NULL,'dir7b-mX_5M:APA91bHBwtOTWz1sbjqgiKyU3wH4zafqQi71L40_CRq99A04QlN3lPuS60j4PXAuNIlL7OxL_HPzysl1RqZ0XIbSEMd416AzE1lR91jhx8qnmlec8QceJsRnGUE4OMai2AGgvDn-DFaS',0,40900,NULL,NULL,NULL,NULL,0),(77,'20171109204258','Aghita Cisar','raghitacisar@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'112832185374524146004',NULL,NULL,NULL,'d1jG7MlBCU8:APA91bFOehjSEZxbqExYGDRz9nZY57mWLRwyMFfDyI08CfnVcL485DtGYkU3tXQC4KEJPO2t7PUgKrgmnqAasyPnsIs65okgsBgCmlFeNnkUmpLSDU0hL-h4ZKoi1C2XHyn6EJpLobWz',0,49300,NULL,NULL,NULL,NULL,0),(78,'20171110082706','Boim Boim','boimkitjem@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'101923500062462908142',NULL,NULL,NULL,'',0,300,NULL,NULL,NULL,NULL,0),(79,'20171110171351','hilman andro','hilmanandro@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'114856742964784884792',NULL,NULL,NULL,'ecNDlhsLd30:APA91bEvDRl1Zbsnc79BUvcLVAJTna4zrCXr7TEOxHcsGLOLJ_scEdlryJl8Qt_bgvE7-nFbksoEeXeRvCrqQaUROvrBLCeg8zTYGuBnyb3Z20KIKOer0-q7Ny-H2XSc0FCxD85kFhWf',0,195400,NULL,NULL,NULL,NULL,0),(80,'20171114093138','Rahayu Kartini','rahayu.kartini@gmail.com',NULL,'',NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,'104724941644380308937',NULL,NULL,NULL,'dWa43ZkUkRA:APA91bG9YBwdUptMvkw3Mu4-LlgCjzp30JCpk4w-YJ7UwzVHQSiUB8oc6cBAdk7pmXcNbvOp3XSz78GXM4-zEaZMa1RRZwODH8WU-BvQGH5KHi-jFUfE1CktdPWoCMtiigFRL-6WCOzP',0,0,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voucher`
--

DROP TABLE IF EXISTS `voucher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_id` varchar(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `point_get` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `expired_date` datetime DEFAULT NULL,
  `merchant_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `bonus_point` int(11) NOT NULL DEFAULT '0',
  `discount` int(11) NOT NULL DEFAULT '0',
  `discount_plus` int(11) NOT NULL DEFAULT '0',
  `payment_type` int(11) DEFAULT NULL,
  `voucher_category` int(11) DEFAULT NULL,
  `term_condition` text,
  `information` text,
  `stock` int(11) NOT NULL DEFAULT '0',
  `sold` int(11) NOT NULL DEFAULT '0',
  `count_buy` int(11) NOT NULL DEFAULT '0',
  `get_buy` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `stat_slider` int(11) NOT NULL DEFAULT '0',
  `count_view` int(11) NOT NULL DEFAULT '0',
  `how_to_use` text,
  `benefit` text,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `voucher_id_UNIQUE` (`voucher_id`),
  KEY `mer_com` (`merchant_id`,`company_id`),
  KEY `expired` (`expired_date`),
  KEY `sold` (`sold`,`expired_date`),
  KEY `stock` (`stock`,`expired_date`),
  KEY `point` (`point`,`expired_date`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voucher`
--

LOCK TABLES `voucher` WRITE;
/*!40000 ALTER TABLE `voucher` DISABLE KEYS */;
INSERT INTO `voucher` VALUES (4,'VCR-0000003',NULL,200000,0,0,'Ayam Gepuk','2017-11-07 22:03:03',1,1,0,10,0,1,7,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',8,17,0,0,'20170721020455.jpg',1,89,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-07-21 13:38:55','2017-10-30 14:31:12','hilman'),(5,'VCR-0000004',NULL,0,2000,0,'Nasi Cobek','2017-11-26 00:00:00',1,1,0,5,0,2,7,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',71,29,0,0,'20170809082245.jpeg',1,66,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-08-09 08:12:09','2017-10-07 00:41:52','hilman'),(6,'VCR-0000005',NULL,40000,100,0,'Nasi Goreng','2017-11-27 00:00:00',1,1,100,15,0,3,7,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',95,25,0,0,'20170809082410.jpg',0,31,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-08-09 08:24:10','2017-08-24 11:07:59','hilman'),(7,'VCR-0000001',NULL,1000000,10000,0,'Hotel 1','2017-11-27 00:00:00',1,1,200,0,0,3,2,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',89,31,0,0,'20170908055650.jpg',0,24,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-09-08 05:54:32','2017-09-08 05:57:03','hilman'),(8,'VCR-0000002',NULL,1200000,1000,0,'Hotel 2','2017-11-26 00:00:00',1,1,100,0,0,3,2,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',7,3,0,0,'20170908055809.jpg',0,4,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-09-08 05:58:09','2017-10-06 23:46:17','hilman'),(14,'VCR-0000006',NULL,500000,0,0,'Hotel 2','2017-12-30 00:00:00',1,1,200,0,0,1,2,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',13,7,0,0,'20171002091207.jpeg',0,7,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:09:36','2017-10-06 23:44:24','fitrah'),(15,'VCR-0000007',NULL,100000,0,0,'Klinik 2','2017-12-30 00:00:00',1,1,100,0,0,1,3,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',10,0,0,0,'20171002091822.jpg',1,5,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:18:22','2017-10-02 21:18:22','admin_company'),(16,'VCR-0000008',NULL,200000,100,0,'Klinik 3','2017-12-30 00:00:00',1,1,50,0,0,3,3,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',9,1,0,0,'20171002091926.jpg',0,6,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:19:26','2017-10-06 23:44:45','fitrah'),(17,'VCR-0000009',NULL,2000000,0,0,'Rumah Sakit 1','2017-12-30 00:00:00',1,1,300,0,0,1,4,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',15,0,0,0,'20171006110331.jpg',0,2,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:21:19','2017-10-06 23:44:28','hilman'),(18,'VCR-0000010',NULL,0,10000,0,'Rumah Sakit 2','2017-12-30 00:00:00',1,1,10,0,0,2,4,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',10,0,0,0,'20171006112820.jpg',0,1,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:21:51','2017-10-06 23:45:35','hilman'),(19,'VCR-0000011',NULL,500000,1000,0,'Rumah Sakit 3','2017-12-30 00:00:00',1,1,100,0,0,3,4,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',482,28,0,0,'20171006105745.JPG',0,28,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:22:26','2017-11-29 07:25:18','hilman'),(20,'VCR-0000012',NULL,1500000,0,0,'Rumah Sakit 4','2017-12-30 00:00:00',1,1,120,0,0,1,4,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',10,0,0,0,'20171002092302.jpg',0,2,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:23:02','2017-10-06 23:45:53','hilman'),(21,'VCR-0000013',NULL,10000,0,0,'Obat 1','2017-12-30 00:00:00',1,1,10,0,0,1,5,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',200,0,0,0,'20171002092411.jpg',1,1,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:24:11','2017-10-02 21:24:11','admin_company'),(22,'VCR-0000014',NULL,0,500,0,'Obat 2','2017-11-07 00:00:00',1,1,10,0,0,2,5,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',99,1,0,0,'20171002092438.jpg',0,3,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:24:38','2017-10-06 23:46:02','hilman'),(23,'VCR-0000015',NULL,300000,0,0,'Fitnes 1','2017-11-07 00:00:00',1,1,100,0,0,1,6,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',13,2,0,0,'20171002092736.jpg',0,4,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:27:36','2017-10-02 21:27:36','admin_company'),(24,'VCR-0000016',NULL,0,2000,0,'Fitnes 2','2017-12-30 00:00:00',1,1,30,0,0,2,6,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',10,0,0,0,'20171002092803.jpeg',0,1,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-02 21:28:03','2017-10-06 23:43:52','hilman'),(29,'VCR-0000018',NULL,20000,0,20000,'Point','2018-03-31 00:00:00',5,3,0,0,0,1,1,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',996,4,0,0,'20171030013133.jpeg',0,12,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-17 11:20:19','2017-11-01 14:52:44','hilman'),(30,'VCR-0000019',NULL,10000,0,10000,'Point','2017-11-28 00:00:00',5,3,0,0,0,1,1,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',997,3,0,0,'20171017113221.png',0,6,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-17 11:32:21','2017-11-02 09:21:14','hilman'),(31,'VCR-0000020',NULL,5000,0,5000,'Point','2018-06-29 03:03:02',5,3,0,0,0,1,1,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',999,1,0,0,'20171030013636.jpeg',0,9,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-10-17 11:34:29','2017-11-01 14:53:10','hilman'),(32,'VCR-0000017',NULL,15000,0,0,'Semangka','2017-11-29 14:20:00',1,1,100,70,30,1,7,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<p>Bagi Anda yang gemar menyantap menu makanan Italia, Pizza e Birra kini hadir dengan voucher value yang berlaku untuk semua menu makanan dan minuman. Menu makanan yang disajikan seperti berbagai macam pilihan pizza, pasta, salad, aneka beer dan masih banyak lainnya.</p>\r\n',25,75,2,1,'20171102114506.jpg',0,120,'<ul>\r\n	<li>Waktu penukaran :</li>\r\n	<li>Senin - Minggu : 10.00 - 22.00 WIB</li>\r\n	<li>Voucher sudah termasuk pajak dan layanan</li>\r\n	<li>Voucher berlaku di hari libur nasional</li>\r\n	<li>Voucher hanya berlaku Dine in</li>\r\n	<li>Voucher tidak dapat digabungkan dengan promo lainnya</li>\r\n	<li>Tidak memerlukan reservasi</li>\r\n</ul>\r\n','<ul>\r\n	<li>\r\n	<p>Voucher Value Worth Rp 100.000,- for All F&amp;B</p>\r\n	</li>\r\n	<li>\r\n	<p>Tersedia berbagai macam pilihan hidangan Italia</p>\r\n	</li>\r\n	<li>\r\n	<p>Pizza dengan roti yang sangat tipis</p>\r\n	</li>\r\n	<li>\r\n	<p>Berlaku di 3 outlet</p>\r\n	</li>\r\n	<li>\r\n	<p>Spesial untuk pengguna XL! Tukarkan sisa kuotamu di MyXL Promo Page dan dapatkan tambahan diskon hingga 15%. Kamu bisa menikmati deal ini hanya dengan Rp 72.250 aja! Mau? Kunjungi http://lp.myfave.com/xl untuk cari tahu lebih lanjut. S&amp;K Berlaku.</p>\r\n	</li>\r\n</ul>\r\n','2017-11-02 11:45:07','2017-11-29 14:15:47','hilman'),(33,'VCR-0000250',NULL,15000,100,0,'Mangga','2018-11-29 14:20:04',1,1,50,0,0,4,7,'','',287,13,0,0,'20171127093720.jpg',0,20,'','','2017-11-27 21:37:20','2017-11-29 14:14:39','hilman');
/*!40000 ALTER TABLE `voucher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voucher_category`
--

DROP TABLE IF EXISTS `voucher_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voucher_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voucher_category`
--

LOCK TABLES `voucher_category` WRITE;
/*!40000 ALTER TABLE `voucher_category` DISABLE KEYS */;
INSERT INTO `voucher_category` VALUES (1,'Point','20171017104830.png','2017-07-21 11:18:25','2017-09-07 23:34:52','hilman'),(2,'Hotel','20170908054545.jpg','2017-07-21 11:18:32','2017-09-08 05:45:45','hilman'),(3,'Klinik','20171002090449.jpg','2017-09-08 05:45:58','2017-10-02 21:04:49','hilman'),(4,'Rumah Sakit','20171002090500.jpg','2017-09-08 05:47:28','2017-10-02 21:05:00','hilman'),(5,'Apotek','20171002090650.jpg','2017-09-08 05:48:23','2017-10-02 21:06:50','hilman'),(6,'Fitnes','20171002090410.jpg','2017-10-02 21:04:10','2017-10-02 21:04:10','hilman'),(7,'Food','20170907113452.jpg','2017-10-17 10:48:30','2017-10-17 10:48:30','hilman');
/*!40000 ALTER TABLE `voucher_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `web_settings`
--

DROP TABLE IF EXISTS `web_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `web_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `content` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `web_settings`
--

LOCK TABLES `web_settings` WRITE;
/*!40000 ALTER TABLE `web_settings` DISABLE KEYS */;
INSERT INTO `web_settings` VALUES (1,'faq','-','pertanyaan Faq',NULL,NULL,NULL),(2,'about_us','-','text about us',NULL,NULL,NULL),(3,'term_condition','-','text term condition',NULL,NULL,NULL);
/*!40000 ALTER TABLE `web_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `withdraw_history`
--

DROP TABLE IF EXISTS `withdraw_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `withdraw_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `id_invoice_history` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `request_date` datetime DEFAULT NULL,
  `process_date` datetime DEFAULT NULL,
  `processed_by` int(11) DEFAULT NULL,
  `bank_account_id` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `withdraw_history`
--

LOCK TABLES `withdraw_history` WRITE;
/*!40000 ALTER TABLE `withdraw_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `withdraw_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-12  6:52:38
